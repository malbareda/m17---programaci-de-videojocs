﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorProceduralSimple : MonoBehaviour
{
    public GameObject[] blocks;
    private GameObject blockActual;
    public GameObject aigua;
    public GameObject joan;
    public GameObject padre;
    public bool planicie;
    public float maxPlanicie;
    public bool rellenar;

    public bool SnapToGrid = true;
    public int seed = 98734387;
    public int comunisme = 324232;
    public int offsetX;
    public int offsetZ;

    //altura maxima
    public float amp = 50f;
    public float freq = 10f;
    public float freqJoan = 200f;



    // Start is called before the first frame update
    void Start()
    {
        gererateProceduralMap();
        
    }

    private void gererateProceduralMap()
    {
        Vector3 pos = this.transform.position;

        int fil = 60;
        int col = 60;

        //ens movem pel mapa com una matriu
        for(int x = 0; x<col; x++)
        {
            for(int z = 0; z < fil; z++)
            {
                float probJoan = Mathf.PerlinNoise((pos.x+x+comunisme)/ freqJoan, (pos.z + z + comunisme) / freqJoan);
                //print(probJoan);

                float y = Mathf.PerlinNoise((offsetX+x +seed)/freq, (offsetZ+z + seed)/freq);
                
               
                //y -= 5;
                    
                //y += Mathf.Sin(z)/10;
                //y += Mathf.Sin(x)/100;

                if(x > fil/2-fil*0.1 && x < fil / 2 + fil * 0.1 && z > col / 2 - col * 0.1 && z < col / 2 + col * 0.1)
                {
                    y--;
                }


                
                if (y > 0.7)
                {
                    blockActual = blocks[2];
                }else if(y > 0.4)
                {
                    blockActual = blocks[1];
                }
                else
                {
                    blockActual = blocks[0];
                }
                if (planicie)
                {
                    if (y < maxPlanicie) y = maxPlanicie;
                }
                
                y = y * amp;
                if (SnapToGrid)
                {
                    //fuerzas a que la y sea un entero (las diferencias seran a nivel bloque, como si fuera minecraft)
                    y = Mathf.Floor(y);
                }

                int valorRellenar = 0;
                if (!rellenar) valorRellenar = (int)(y - 3);
                for (float i=valorRellenar; i < y; i++)
                {
                GameObject newBlock = Instantiate(blockActual);
                    newBlock.transform.position = new Vector3(pos.x + x, i, pos.z + z);
                    newBlock.transform.parent = padre.transform;
                }
                for(float i = y; i < amp*10/100; i++)
                {
                    GameObject aguaPorFavor = Instantiate(aigua);
                    aguaPorFavor.transform.position= new Vector3(pos.x + x, i, pos.z + z);
                    aguaPorFavor.transform.parent = padre.transform;

                }

                if (probJoan>0.65f)
                {
                    float rand = UnityEngine.Random.Range(0f, 1f);
                    if (rand < probJoan)
                    {
                        GameObject newJoan = Instantiate(joan);
                        newJoan.transform.position = new Vector3(pos.x + x, y + 1, pos.z + z);
                        newJoan.transform.parent = padre.transform;
                    }
                    
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for(int i = 0; i < padre.transform.childCount; i++)
            {
                Destroy(padre.transform.GetChild(i).gameObject);
            }
            gererateProceduralMap();
        }
           
        

    }

}
