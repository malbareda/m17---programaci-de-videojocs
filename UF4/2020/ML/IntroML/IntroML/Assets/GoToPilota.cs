﻿using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using UnityEngine;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;

public class GoToPilota : Agent
{
    public float spd = 1;
    [SerializeField] private Transform target;

    public override void OnEpisodeBegin()
    {
        transform.localPosition = Vector3.zero;
    }
    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
        sensor.AddObservation(target.localPosition);

    }
    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        float moveZ = actions.ContinuousActions[1];

        transform.localPosition += new Vector3(moveX, 0, moveZ) * Time.deltaTime*spd;
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> acCont = actionsOut.ContinuousActions;
        acCont[0] = Input.GetAxisRaw("Horizontal");
        acCont[1] = Input.GetAxisRaw("Vertical");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "pelota")
        {
            SetReward(1f);
            EndEpisode();
        }
        if (other.transform.tag == "wall")
        {
            SetReward(-1f);
            EndEpisode();
        }
    }

   
}
