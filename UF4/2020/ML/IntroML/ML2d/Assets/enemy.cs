﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour
{
    public int spd=1;

    // Update is called once per frame
    void Update()
    {
        this.transform.localPosition += new Vector3(-1, 0,0) * Time.deltaTime * spd;
        if (this.transform.localPosition.x < -20)
        {
            Destroy(this.gameObject);
        }
    }
}
