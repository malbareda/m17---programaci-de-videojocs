﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using UnityEngine;

public class saltador : Agent
{
    bool canJump = true;
    private int score = 0;
    private int max = 0;
    private Vector3 posInicial;

    public delegate void ResetDelegate();
    public event ResetDelegate OnReset;
    public override void Initialize()
    {
        base.Initialize();
        posInicial = this.transform.position;
    }

    public override void OnActionReceived(ActionBuffers actions)
    {

        if (actions.DiscreteActions[0] == 1)
        {
            Jump();
        }
    }

    private void FixedUpdate()
    {
        if (canJump)
        {
            RequestDecision();
        }
    }




    public override void OnEpisodeBegin()
    {
        canJump = true;
        score = 0;
        this.transform.position = posInicial;
        this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        OnReset?.Invoke();
    }

    private void Jump()
    {
        if (canJump)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));
            canJump = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "floor")
        {
            canJump = true;
        }
        if (collision.transform.tag == "enemy")
        {
            AddReward(-1f);
            //Debug.Log(GetCumulativeReward());
            if (score > max)
            {
                max = score;
                this.transform.parent.GetChild(4).gameObject.GetComponent<TMPro.TextMeshPro>().text = score + "";
            }
            
            EndEpisode();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag=="passedEnemy")
        {
            AddReward(0.1f);
            score++;
            this.transform.parent.GetChild(3).gameObject.GetComponent<TMPro.TextMeshPro>().text = score + "";
            //Debug.Log(GetCumulativeReward());
        }
    }
}
