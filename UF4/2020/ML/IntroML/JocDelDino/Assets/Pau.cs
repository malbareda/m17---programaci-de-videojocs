using System;
using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class Pau : Agent
{
    public GameObject kane;
    private int score = 0;
    private int max = 0;
    public float vel = 5;
    public int vides = 10;
    private Vector3 posInicial;
    Rigidbody2D rb;

    public delegate void ResetDelegate();
    public event ResetDelegate OnReset;

    public override void Initialize()
    {
        vides = 10;
        rb = this.GetComponent<Rigidbody2D>();
        base.Initialize();
        posInicial = this.transform.position;
    }

    internal void plof()
    {
        this.AddReward(-1f);
        vides--;
        if (vides <= 0)
        {
            if (score > max)
            {
                max = score;
                this.transform.parent.GetChild(4).gameObject.GetComponent<TMPro.TextMeshPro>().text = score + "";
            }
            EndEpisode();
        }
    }

    //NOVA
    public override void CollectObservations(VectorSensor sensor)
    {
        float distX = kane.transform.position.x - this.transform.position.x;
        sensor.AddObservation(distX);
        sensor.AddObservation(kane.transform.position.y);
    }

    private void FixedUpdate()
    {
        RequestDecision();
    }


    //accion es continua
    public override void OnActionReceived(float[] vectorAction)
    {
        //una accion continua devuelve un valor entre -1 y 1
        float accioVector = vectorAction[0];
        //el multipliquem per velocitat, amb el que donara un valor entre -vel i vel
        float spd = (accioVector) * vel;
        //print(spd);
        //el posem al rigidbody
        rb.velocity = new Vector2(spd, 0);
    }

    public override void OnEpisodeBegin()
    {
        vides = 10;
        score = 0;
        this.transform.parent.GetChild(3).gameObject.GetComponent<TMPro.TextMeshPro>().text = score + "";
        this.transform.position = posInicial;
        this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        OnReset?.Invoke();
    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.transform.tag == "kane")
        {
            AddReward(0.1f);
            //Debug.Log(GetCumulativeReward());
            score++;
            this.transform.parent.GetChild(3).gameObject.GetComponent<TMPro.TextMeshPro>().text = score + "";

            //EndEpisode();

        }
    }



}
