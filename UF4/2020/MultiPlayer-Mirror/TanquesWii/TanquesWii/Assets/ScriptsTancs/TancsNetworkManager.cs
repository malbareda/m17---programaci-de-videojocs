using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

[AddComponentMenu("")]
public class TancsNetworkManager : NetworkManager
{

    public Transform P1;
    public Transform P2;
    public GameObject obst;

    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        // add player at correct spawn position
        Transform start = P1;
        if (numPlayers == 0)
        {
            start = P1;
        }
        else if (numPlayers == 1)
        {
            start = P2;
            start.Rotate(new Vector3(0, 0, 180));
        }

        GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
        NetworkServer.AddPlayerForConnection(conn, player);//esto llama a la funcion onStartLocalPlayer del player




    }

    public override void OnServerDisconnect(NetworkConnectionToClient conn)
    {

        // call base functionality (actually destroys the player)
        base.OnServerDisconnect(conn);
    }

}
