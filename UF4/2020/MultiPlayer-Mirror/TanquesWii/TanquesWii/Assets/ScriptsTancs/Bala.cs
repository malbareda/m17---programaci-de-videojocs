﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : NetworkBehaviour
{
    public int playerId;

    [ServerCallback]
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "wall")
        {
            Destroy(this.gameObject);
        }else if(collision.transform.tag == "tank" && collision.GetInstanceID()!=playerId)
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }else if(collision.transform.tag == "obstacle")
        {
            collision.GetComponent<Obstacle>().danyar();
            Destroy(this.gameObject);
        }
    }
}
