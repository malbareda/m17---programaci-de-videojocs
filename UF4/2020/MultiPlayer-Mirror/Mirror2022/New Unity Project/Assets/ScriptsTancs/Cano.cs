﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Cano : NetworkBehaviour
{
    public float speed = 120;
    public float angularSpeed = 30;
    public float bulletSpeed = 200;
    public GameObject bullet;
    private bool cd = false;
    public TextMesh playerNameText;
    

    [SyncVar(hook = nameof(OnNameChanged))]
    public string playerName;

    [SyncVar(hook = nameof(OnColorChanged))]
    public Color playerColor = Color.white;
    private GeneralManager sceneScript;
    private InputField input;

    void Awake()
    {
        //allow all players to run this
        sceneScript = GameObject.FindObjectOfType<GeneralManager>();
        input = GameObject.FindObjectOfType<InputField>();
    }
    [Command]
    public void CmdSendPlayerMessage()
    {
        
        if (sceneScript)
        {
            sceneScript.statusText = playerName + " diu "+input.text;
        }
    }


    void OnNameChanged(string _Old, string _New)
    {
        playerNameText.text = _New;
    }

    void OnColorChanged(Color _Old, Color _New)
    {
        playerNameText.color = _New;
        GetComponent<SpriteRenderer>().color = _New;
    }


    public override void OnStartLocalPlayer()
    {
        sceneScript.cano = this;
        string name = "Player" + Random.Range(100, 999);
        Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        CmdSetupPlayer(name, color);
    }

    [Command]
    public void CmdSetupPlayer(string _name, Color _col)
    {
        //player info sent to server, then server updates sync vars which handles it on all clients
        playerName = _name;
        playerColor = _col;
        sceneScript.statusText = playerName + " s'ha unit.";
    }

    void FixedUpdate()
    {
        // nomes nosaltres podem controlar el canó
        if (isLocalPlayer)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(Input.GetAxisRaw("Horizontal"), 0) * speed * Time.fixedDeltaTime;
            this.transform.GetChild(0).Rotate(new Vector3(0, 0, Input.GetAxis("Vertical")) * angularSpeed * Time.fixedDeltaTime);
            
            if (Input.GetKey("space"))
            {
                if (!cd)
                {
                    Shoot();
                    StartCoroutine(cooldown());
                }

            }
        }


    }
    [ClientCallback]
    private void Shoot()
    {
        commandShoot();
    }

    [Command]
    private void commandShoot()
    {
        GameObject newBala = Instantiate(bullet);
        newBala.transform.position = this.transform.GetChild(0).GetChild(0).position;
        newBala.transform.rotation = this.transform.GetChild(0).GetChild(0).rotation;
        newBala.GetComponent<Rigidbody2D>().velocity = newBala.transform.up * bulletSpeed * Time.fixedDeltaTime;
        newBala.GetComponent<Bala>().playerId = this.GetInstanceID();
        NetworkServer.Spawn(newBala);
    }

    private IEnumerator cooldown()
    {
        cd = true;
        yield return new WaitForSeconds(0.5f);
        cd = false;

    }
}
