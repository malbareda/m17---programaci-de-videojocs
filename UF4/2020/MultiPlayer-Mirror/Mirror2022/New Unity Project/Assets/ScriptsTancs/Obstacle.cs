﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Obstacle : NetworkBehaviour
{
    //variable sincronitzada
    //la variable sincronitzada es queda igual per a tots els clients si i només si l'actualitza el servidor.
    [SyncVar]
    public int hp;

    private void Start()
    {
        hp = 5;
        this.GetComponent<SpriteRenderer>().color = new Color((1 - (0.2f * hp)),  (0.2f * hp), 0f);

    }

    //funcio de server
    [ServerCallback]
    internal void danyar()
    {
        //this.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = ""+hp;
        
        canviColor();
        hp--;
        if (hp == 0)
        {
            Destroy(this.gameObject);
        }
    }

    //Command al reves, el servidor ordena a TOTS els clients que executin una funcio
    [ClientRpc]
    private void canviColor()
    {
        this.GetComponent<SpriteRenderer>().color = new Color((1 - (0.2f * hp)), 0f, (0.2f * hp));
    }
}
