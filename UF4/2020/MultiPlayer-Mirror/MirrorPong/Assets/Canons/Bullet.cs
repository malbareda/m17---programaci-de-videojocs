﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : NetworkBehaviour
{
    public int playerid;

    // Update is called once per frame
    void Update()
    {
        
    }
    [ServerCallback]
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "wall")
        {
            Destroy(this.gameObject);
        }else if(other.transform.tag == "cannon")
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
        print("a");
    }
}
