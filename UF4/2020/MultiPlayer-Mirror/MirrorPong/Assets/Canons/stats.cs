﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class stats : NetworkBehaviour
{
    public Text textDispars;

    [SyncVar]
    public int dispars;


    [ServerCallback]
    private void Update()
    {
        textDispars.text = "DISPARS: " + dispars;
    }
}
