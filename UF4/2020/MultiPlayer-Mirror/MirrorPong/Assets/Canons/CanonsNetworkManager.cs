﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonsNetworkManager : NetworkManager
{
    public Transform P1;
    public Transform P2;
    public GameObject obst;


    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        // add player at correct spawn position
        Transform start = P1;
        if (numPlayers == 0)
        {
            start = P1;
        }else if (numPlayers == 1)
        {
            start = P2;
            start.Rotate(new Vector3(0, 0, 180));
        }
        
        GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
        NetworkServer.AddPlayerForConnection(conn, player);



        
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
       
        // call base functionality (actually destroys the player)
        base.OnServerDisconnect(conn);
    }
}
