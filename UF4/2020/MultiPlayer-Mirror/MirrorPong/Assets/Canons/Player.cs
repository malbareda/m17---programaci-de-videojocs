﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : NetworkBehaviour
{
    public float speed = 70;
    public float angularSpeed = 30;
    public float bulletSpeed = 200;
    public GameObject bullet;
    public GameObject stats;
    private bool cd = false;
    

    // need to use FixedUpdate for rigidbody
    void FixedUpdate()
    {
        // only let the local player control the cannon
        if (isLocalPlayer)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(Input.GetAxisRaw("Horizontal"), 0) * speed * Time.fixedDeltaTime;
            this.transform.GetChild(0).Rotate(new Vector3(0, 0, Input.GetAxis("Vertical")) * angularSpeed * Time.fixedDeltaTime);
            if (Input.GetKey("space"))
            {
                if (!cd)
                {
                    Shoot();
                    StartCoroutine(cooldown());
                }
                
            }
        }
            
            
    }

    private IEnumerator cooldown()
    {
        cd = true;
        yield return new WaitForSeconds(0.5f);
        cd = false;

    }

    [Client]
    private void Shoot()
    {
        commandShoot();
        
    }

    [Command]
    private void commandShoot()
    {
        GameObject newBullet = Instantiate(bullet);
        newBullet.transform.position = this.transform.GetChild(0).GetChild(0).position;
        newBullet.transform.rotation = this.transform.GetChild(0).GetChild(0).rotation;
        newBullet.GetComponent<Bullet>().playerid = this.GetInstanceID();
        newBullet.GetComponent<Rigidbody2D>().velocity = newBullet.transform.up * bulletSpeed * Time.fixedDeltaTime;
        NetworkServer.Spawn(newBullet);
    }
}
