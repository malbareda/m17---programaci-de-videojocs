﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManagerCanons : NetworkManager
{
    public Transform P1;
    public Transform P2;
    public GameObject obst;

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        Transform start = P1;
        //numplayers es una variable de networkmanager que et diu quants jugadors tens connectats
        if (numPlayers == 0)
        {
            start = P1;
        }else if (numPlayers == 1)
        {
            start = P2;
            start.Rotate(new Vector3(0, 0, 180));
        }

        GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
        //aixo instancia un player a nivell servidor (per a tots els clients i a mes li dona el control al player corresponent)
        NetworkServer.AddPlayerForConnection(conn, player);

        GameObject newObstacle = Instantiate(obst);
        newObstacle.transform.position = new Vector3(Random.Range(-20, 20), 0, 0);
        NetworkServer.Spawn(newObstacle);

    }
}
