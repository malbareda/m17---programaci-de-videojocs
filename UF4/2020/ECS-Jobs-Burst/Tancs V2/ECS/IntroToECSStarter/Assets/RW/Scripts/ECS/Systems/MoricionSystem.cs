﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class MoricionSystem : ComponentSystem
{

    //chunk
    //ChunkArchetype
    private float collisionDistance = 2f;

    protected override void OnUpdate()
    {
        if (GameManager.IsGameOver())
        {
            return;
        }

        float3 playerPos = (float3)GameManager.GetPlayerPosition();

        Entities.WithAll<EnemyTag>().ForEach((Entity enemy, ref Translation enemyPos, ref Vida vida) =>
        {
            playerPos.y = enemyPos.Value.y;
            //hacemos la colision a mano
            if(math.distance(playerPos, enemyPos.Value) < collisionDistance)
            {
                FXManager.Instance.CreateExplosion(enemyPos.Value);

                //FXManager.Instance.CreateExplosion(playerPos);
                //si l'enemic colisiona amb el player acabem la partida
                //GameManager.EndGame();

                //es un late update. Si no esborrariem coses d'una llista metnre recorrem la llista que means problem
                PostUpdateCommands.DestroyEntity(enemy);

            }

            int hp = vida.hp;
            float3 enemyPosition = enemyPos.Value;

            //per cada enemic
            Entities.WithAll<BulletTag>().ForEach((Entity bullet, ref Translation bulletPos) =>
            {
                if (math.distance(bulletPos.Value, enemyPosition) < collisionDistance)
                {
                    //es un late update. Si no esborrariem coses d'una llista metnre recorrem la llista que means problem
                    //PostUpdateCommands.DestroyEntity(enemy);
                    hp--;
                    if (hp <= 0)
                    {
                        PostUpdateCommands.DestroyEntity(enemy);
                        GameManager.AddScore(1);
                        FXManager.Instance.CreateExplosion(enemyPosition);
                    }

                    PostUpdateCommands.DestroyEntity(bullet);

                    

                    
                    //PostUpdateCommands.DestroyEntity(enemy);
                   


                }
            });

            vida.hp = hp;


        });
    }
}
