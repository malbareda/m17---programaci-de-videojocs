﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;


//nova classe
public class MovementSystem : SystemBase
{
    //barrera. Com el postUpdate. Et permet fer coses thread-safe com afegir o esborrar entitats paralelament.
    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

    protected override void OnUpdate()
    {
        //commandbuffer es com farem servir la barrera.
        //permet 4 metodes. AddComponent, RemoveComponent, CreateEntity i DestroyEntity
        var commandBuffer = barrier.CreateCommandBuffer().AsParallelWriter();

        //coses com el deltaTime han d'anar fora del thread
        float dt = Time.DeltaTime;

        //1r selecciones les entitats mitjançant el tag
        //2n fas un ForEach
        //input del foreach
        //sempre entity i index de entity
        //els components que vulguis, siguin de transform  Translation/Rotation/Scale, o creats per tu
        //in variable de entrada. Solo lectura.  ref es una variable de sortida, hi pots escriure 
        Entities.WithAny<EnemyTag, BulletTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, in Rotation rot, in MoveForward moveForward) =>
         {
             trans.Value += moveForward.speed * dt *  math.forward(rot.Value);
         }
        //automaticament ho fa en paralel
        ).ScheduleParallel();
    }

}