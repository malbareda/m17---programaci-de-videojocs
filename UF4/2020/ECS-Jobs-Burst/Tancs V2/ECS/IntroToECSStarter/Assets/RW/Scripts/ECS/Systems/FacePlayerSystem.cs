﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;


public class FacePlayerSystem : SystemBase
{
    protected override void OnUpdate()
    {
        if (GameManager.IsGameOver()){
            return;
        }

        float3 playerPos = (float3)GameManager.GetPlayerPosition();


        Entities.WithAll<EnemyTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, ref Rotation rot) =>
        {



            float3 direction = playerPos - trans.Value;
            direction.y = 0f;

            rot.Value = Quaternion.LookRotation(direction, math.up());

        }).ScheduleParallel();
    }
}
