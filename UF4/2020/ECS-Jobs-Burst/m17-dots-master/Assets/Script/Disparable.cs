﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Disparable : MonoBehaviour{

	public float rangoDeAccao = 1f;
	public void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, rangoDeAccao);
	}

}
