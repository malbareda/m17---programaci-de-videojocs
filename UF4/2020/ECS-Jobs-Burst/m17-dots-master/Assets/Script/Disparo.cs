﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu]
public class Disparo : ScriptableObject{
	public float speed;
	public float tts;
	public float ttl;
	public float damage;
	public ShootType shootType = ShootType.MULTISHOOT;
	public float quantity;
	public float size = 1; //ni idea de como hacerlo
	public float rotateAfterTime;

	public bool changeDirection;
	public int changeDirectionShootCount = 1;
	public bool changeAngleXShoot;
	public float angleXShoot = 0;
	public bool invertAngleXShoot;
	public float invertAngleXShootCount = 1;

	public Sprite bala;
	public Color color = Color.white;

	public bool shouldStop;
}

public enum ShootType {
	MULTISHOOT, ROUND
}
