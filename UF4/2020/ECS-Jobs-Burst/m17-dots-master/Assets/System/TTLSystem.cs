﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

public class TTLSystem : SystemBase {

	EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

	protected override void OnUpdate() {

		var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();
		
		float dt = Time.DeltaTime;
		Entities.WithAny<BulletTag>().ForEach((Entity entity, int entityInQueryIndex, ref TimeToLiveComponent ttl) => {
			ttl.timeToLive -= dt;
			if (ttl.timeToLive <= 0)
				commandBuffer.DestroyEntity(entityInQueryIndex, entity);


		}).WithName("TTL").ScheduleParallel();

		barrier.AddJobHandleForProducer(Dependency);
	}
}
