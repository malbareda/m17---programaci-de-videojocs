﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;


public class RotateSystem : SystemBase {

	EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

	[BurstDiscard]
	protected override void OnUpdate() {
		var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();

		float dt = Time.DeltaTime;
		Entities.WithAny<BulletTag>().ForEach((Entity entity, int entityInQueryIndex, ref Rotation rot, in RotationComponent rotaComp) => {
			//quaternion q = quaternion.RotateZ(rotaComp.angularSpeed);
			//rot.Value = Quaternion.Lerp(rot.Value, q, rotaComp.angularSpeed * dt);
			//Debug.Log(rot.Value);

			rot.Value = math.mul(math.normalize(rot.Value), quaternion.AxisAngle(math.mul(rot.Value, new float3(0, 0, 1)), rotaComp.angularSpeed * dt));

		}).ScheduleParallel();
	}
}
