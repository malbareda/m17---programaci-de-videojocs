﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

public class BulletToEnemySystem : SystemBase {

	EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

	protected override void OnUpdate() {

		var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();
		
		float dt = Time.DeltaTime;
		Entities.WithAny<BulletTag>().ForEach((Entity entity, int entityInQueryIndex, ref TimeToLiveComponent ttl) => {


			//commandBuffer.DestroyEntity(entityInQueryIndex, entity);


		}).ScheduleParallel();

		barrier.AddJobHandleForProducer(Dependency);
	}
}
