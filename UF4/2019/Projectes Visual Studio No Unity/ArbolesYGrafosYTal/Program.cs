﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbolesYGrafosYTal
{
    //representacio d'un graf
    class Grafo
    {
        //un graf es defineix com un diccionari, a on hi son tots els nodes. per cada node hi ha una llista dels seus veins, que es representa com una llista d'arestes que connecten els dos nodes)
        Dictionary<Nodo, List<Aresta>> grafo = new Dictionary<Nodo, List<Aresta>>();
        
        //afegir un cami a un graf
        public void addPath(Nodo n1, Nodo n2)
        {
            
            List<Aresta> list;
            //mira si el node ja existeix al graf
            if (grafo.TryGetValue(n1, out list))
            {
                //si ja existeix
                //afegeixes el cami com una nova aresta
                list.Add(new Aresta { nodo1 = n1, nodo2 = n2 });
            }
            else
            {
                //si no existeix
                list = new List<Aresta>();
                //afageixes el nou node
                grafo.Add(n1, list);
                //afegeixes al node que acabes de crear el cami com una nova aresta
                list.Add(new Aresta { nodo1 = n1, nodo2 = n2 });
            }
        }
    }

    class Nodo
    {
        string valor;
    }
    class Aresta
    {
        public Nodo nodo1;
        public Nodo nodo2;
    }
    class NodoArbolBinario
    {
        public string valor;
        public NodoArbolBinario hijoDer;
        public NodoArbolBinario hijoIzq;

        public NodoArbolBinario(string valor)
        {
            this.valor = valor;
        }

        public void addLeftChild(NodoArbolBinario h)
        {
            hijoIzq = h;
        }

        public void addRightChild(NodoArbolBinario h)
        {
            hijoDer = h;
        }
    }

    class NodoArbol
    {
        public string valor;
        public List<NodoArbol> hijos = new List<NodoArbol>();

        public void addChild(NodoArbol h)
        {
            hijos.Add(h);
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            NodoArbolBinario raiz = new NodoArbolBinario("España");
            Console.WriteLine(raiz.valor);
            raiz.addLeftChild(new NodoArbolBinario("ERREJON"));
            raiz.hijoIzq.addLeftChild(new NodoArbolBinario("PODEMOS"));
            raiz.hijoIzq.addRightChild(new NodoArbolBinario("PSOE"));
            raiz.addRightChild(new NodoArbolBinario("PP"));
            raiz.hijoDer.addRightChild(new NodoArbolBinario("VOX"));
            raiz.hijoDer.addLeftChild(new NodoArbolBinario("CIUDADANOS"));


            Console.WriteLine( contains("PSOE",raiz));


            Console.ReadLine();

            NodoArbol raiz2 = new NodoArbol { valor = "Fermín" };

            raiz2.addChild(new NodoArbol { valor = "Fermín" });
            raiz2.addChild(new NodoArbol { valor = "Hermana de Fermín" });

            raiz2.hijos[0].addChild(new NodoArbol { valor = "David" });
            raiz2.hijos[0].hijos[0].addChild(new NodoArbol { valor = "Susto" });
            raiz2.hijos[0].addChild(new NodoArbol { valor = "Judit" });

            Console.WriteLine(contains("Susto",raiz2));

            Console.ReadLine();










        }


        private static bool contains(string v, NodoArbol raiz)
        {
            if (raiz.valor == v)
            {
                return true;
            }
            else
            {
                bool acc = false;
                foreach(NodoArbol h in raiz.hijos)
                {
                    acc = (acc || contains(v, h));

                }
                return acc;
            }
        }

        private static bool contains(string v, NodoArbolBinario raiz)
        {
            if (raiz == null)
            {
                return false;
            }
            if (raiz.valor == v)
            {
                Console.WriteLine("encontrado");
                return true;
            }
            else
            {
                return contains(v, raiz.hijoIzq) || contains(v, raiz.hijoDer);
            }
        }
    }
}
