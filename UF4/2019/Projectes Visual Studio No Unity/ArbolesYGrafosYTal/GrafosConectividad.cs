﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingMientrasEstoSeBaja
{
    class GrafosConectividad
    {
        static List<string> memoria = new List<string>();
        static void Main(string[] args)
        {
            //Grafo Dirigido. El grafo dirigido significa que los caminos son unidireccionales
            ///  -----> 
            ///
            /// Grafos con peso   (Weighted Graph) significa que cada camino tiene un peso. Es el peso que tardas en recorrerlo. 

            Dictionary<string, Dictionary<string, int>> mapaEpidemiologico = new Dictionary<string, Dictionary<string, int>>();

            mapaEpidemiologico.Add("China", new Dictionary<string, int>());
            mapaEpidemiologico.Add("España", new Dictionary<string, int>());
            mapaEpidemiologico.Add("Italia", new Dictionary<string, int>());
            mapaEpidemiologico.Add("Iran", new Dictionary<string, int>());
            mapaEpidemiologico.Add("Alemania", new Dictionary<string, int>());
            mapaEpidemiologico.Add("Corea", new Dictionary<string, int>());
            mapaEpidemiologico.Add("Japon", new Dictionary<string, int>());
            mapaEpidemiologico.Add("Andorra", new Dictionary<string, int>());
            mapaEpidemiologico.Add("Narnia", new Dictionary<string, int>());
            mapaEpidemiologico.Add("Murcia", new Dictionary<string, int>());


            mapaEpidemiologico["China"].Add("Corea", 2);
            //mapaEpidemiologico["Corea"].Add("China", 1);
            mapaEpidemiologico["China"].Add("Japon", 3);
            mapaEpidemiologico["China"].Add("Iran", 5);
            mapaEpidemiologico["Iran"].Add("Alemania", 6);
            mapaEpidemiologico["Alemania"].Add("Italia", 4);
            mapaEpidemiologico["Italia"].Add("España", 8);



            Console.WriteLine(Connect(mapaEpidemiologico, "China", "España"));

            Console.ReadLine();

            memoria.Clear();
            Console.WriteLine(Distancia(mapaEpidemiologico, "China", "España"));

            Console.ReadLine();



            //Dictionary<Nodo, Dictionary<Nodo, Nodo>>
        }

        private static int Distancia(Dictionary<string, Dictionary<string, int>> mapaEpidemiologico, string actual, string objetivo)
        {
            if (actual == objetivo)
            {
                //hay distancia 0 
                return 0;
            }
            //si ya hemos pasado por este pais significa que estamos dando vueltas en circulo
            else if (memoria.Contains(actual))
            {
                return -1;
            }
            else
            {
                //añadimos el pais como pais visitado
                memoria.Add(actual);

                //miramos todos los vecinos de ese pais (que es un diccionario)
                Dictionary<string, int> vecinos = mapaEpidemiologico[actual];
                int dist = -1;
                foreach (var entry in vecinos)
                {
                    int aux = (entry.Value + Distancia(mapaEpidemiologico, entry.Key, objetivo));
                    dist = dist == -1 || aux < dist && aux != -1 ? aux : dist;
                }
                return dist;

            }
        }

        private static bool Connect(Dictionary<string, Dictionary<string, int>> mapaEpidemiologico, string actual, string objetivo)
        {
            //la conecitivdad se hace por recursividad

            //si el pais que estamos buscando es el pais actual
            if (actual == objetivo)
            {
                //hay conectividad
                return true;
            }
            //si ya hemos pasado por este pais significa que estamos dando vueltas en circulo
            else if (memoria.Contains(actual))
            {
                return false;
            }
            else
            {
                //añadimos el pais como pais visitado
                memoria.Add(actual);

                //miramos todos los vecinos de ese pais (que es un diccionario)
                Dictionary<string, int> vecinos = mapaEpidemiologico[actual];
                bool flag = false;
                foreach (var entry in vecinos)
                {
                    flag = flag || Connect(mapaEpidemiologico, entry.Key, objetivo);

                }
                return flag;

            }
        }
    }
}
