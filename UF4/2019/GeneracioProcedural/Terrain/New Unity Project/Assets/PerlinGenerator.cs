﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinGenerator : MonoBehaviour
{

    public int depth = 20;
    public int width = 50;
    public int height = 50;
    public float seed = 0;

    //altura maxima
    public float ampl = 15f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private TerrainData DataInitializer(TerrainData terrainData)
    {
        terrainData.heightmapResolution = 250;
        terrainData.size = new Vector3(width, depth, height);
        //generateheights, le pasas el voxel inicial y un matriz de todos los pixeles la altura que deberia tener
        //la matriz tiene que estar obligatoriamente entre 0 y 1
        terrainData.SetHeights(0, 0, GenerateHeights());

        return terrainData;

    }

    private float[,] GenerateHeights()
    {
        float[,] heights = new float[width,height];
        for(int x = 0; x < width; x++)
        {
            for(int y = 0; y < height; y++)
            {
                //para cada posiciond e la matriz de alturas genero la altura
                
                heights[x, y] = CreateHeight(x, y)-0.5f;
                heights[x, y] = heights[x,y]< 0 ? 0f : heights[x, y];

            }
        }

        return heights;

    }

    private float CreateHeight(int x, int y)
    {
        //divido por freq y mulitplico por ampl
        //si no divides pòr freq quedan cortes raros porque calcula cada punto independientemente
        //si no multiplicas por ampl se va a quedar todo entre 0 y 1 y por tanto apenas se va a ver nada.
        float coordX = (float)x / width * ampl;
        float coordY = (float)y / height * ampl;

        //la altura la genero mediante perlin
        return Mathf.PerlinNoise(coordX + seed, coordY);
    }


    // Update is called once per frame
    void Update()
    {
        Terrain terrain = this.GetComponent<Terrain>();

        //editas el terraindata
        terrain.terrainData = DataInitializer(terrain.terrainData);

    }
}
