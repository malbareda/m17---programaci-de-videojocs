﻿using UnityEngine;
using System.Collections;
using UnityEditor;

//esta etiqueta implica que es un editor del MapGenerator. Es decir, va a editar el inspector del componente MapGenerator
[CustomEditor (typeof (MapGenerator))]
//fijaos que hereda de Editor, no de mono
public class MapGeneratorEditor : Editor {

	//afecta a la GUI del Inspector
	public override void OnInspectorGUI() {
		MapGenerator mapGen = (MapGenerator)target;

		//si hay un cambio de las variables del inspector
		if (DrawDefaultInspector ()) {
			if (mapGen.autoUpdate) {
				mapGen.GenerateMap ();
			}
		}

		//pones un boton generate
		if (GUILayout.Button ("Generate")) {
			//llama a la funcion del mapgenerator
			mapGen.GenerateMap ();
		}
	}
}
