﻿using UnityEngine;
using System.Collections;

public class MapGenerator : MonoBehaviour {


	//enum para decidir el tipo de cosa que vas a dibujar
	public enum DrawMode {NoiseMap, ColourMap, Mesh};
	//un enum en el inspector se ve como un selector
	public DrawMode drawMode;

	const int mapChunkSize = 241;
	[Range(0,6)]
	public int levelOfDetail;
	public float noiseScale;

	public int octaves;
	[Range(0,1)]
	public float persistance;
	public float lacunarity;

	public int seed;
    public int secondseed;
    public Vector2 offset;

	public float meshHeightMultiplier;
	public AnimationCurve meshHeightCurve;

	public bool autoUpdate;

	public TerrainType[] regions;

	public void GenerateMap() {
		float[,] noiseMap = Noise.GenerateNoiseMap (mapChunkSize, mapChunkSize, seed, secondseed, noiseScale, octaves, persistance, lacunarity, offset);

		Color[] colourMap = new Color[mapChunkSize * mapChunkSize];
		for (int y = 0; y < mapChunkSize; y++) {
			for (int x = 0; x < mapChunkSize; x++) {
				//por cada puntod el mapa miras la altura
				float currentHeight = noiseMap [x, y];
				//y recorres el struct de regiones (colores)
				for (int i = 0; i < regions.Length; i++) {
					//si la region actual es mayor que la altura
					if (currentHeight <= regions [i].height) {
						//la pintas de este color
						colourMap [y * mapChunkSize + x] = regions [i].colour;
						//y haces break. obviamente esto solo funciona si las regiones estan ordenadas ascendientemente por altura
						break;
					}
				}
			}
		}

		MapDisplay display = FindObjectOfType<MapDisplay> ();
		if (drawMode == DrawMode.NoiseMap) {
			display.DrawTexture (TextureGenerator.TextureFromHeightMap (noiseMap));
		} else if (drawMode == DrawMode.ColourMap) {
			display.DrawTexture (TextureGenerator.TextureFromColourMap (colourMap, mapChunkSize, mapChunkSize));
		} else if (drawMode == DrawMode.Mesh) {
			display.DrawMesh (MeshGenerator.GenerateTerrainMesh (noiseMap, meshHeightMultiplier, meshHeightCurve, levelOfDetail), TextureGenerator.TextureFromColourMap (colourMap, mapChunkSize, mapChunkSize));
		}
	}

    //onValidate salta automaticamente cuando cambias los valores. Es de Unity. 
	void OnValidate() {
		if (lacunarity < 1) {
			lacunarity = 1;
		}
		if (octaves < 0) {
			octaves = 0;
		}
	}
}

[System.Serializable]
public struct TerrainType {
	public string name;
	public float height;
	public Color colour;
}