﻿using UnityEngine;
using System.Collections;

public static class Noise {

	public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, int seed, int otherseed, float scale, int octaves, float persistance, float lacunarity, Vector2 offset) {
		float[,] noiseMap = new float[mapWidth,mapHeight];

        //genera numeros aleatorios a partir de una semilla. Estos numeros van a ser para la primera octava (la que determina el contorno general del terreno)
		System.Random prng = new System.Random (seed);
        //genera numeros aleatorios a partir de una semilla. Estos numeros van a ser para el resto de octavas (las que determinan los detalles)
        System.Random prng2 = new System.Random(otherseed);
		
		//Octavas. Cada Perlin cuando haces un terreno complejo se conoce como una octava.
        //Cada octava tendra mas freq que la anterior y menos ampl

        Vector2[] octaveOffsets = new Vector2[octaves];
        float offsetX = prng.Next(-100000, 100000) + offset.x;
        float offsetY = prng.Next(-100000, 100000) + offset.y;
        octaveOffsets[0] = new Vector2(offsetX, offsetY);
		
        //cada octava va a tener su semilla aleatoria

        for (int i = 1; i < octaves; i++) {
			offsetX = prng2.Next (-100000, 100000) + offset.x;
			offsetY = prng2.Next (-100000, 100000) + offset.y;
			octaveOffsets [i] = new Vector2 (offsetX, offsetY);
		}

        //scale. En el Perlin no puedes pasarle numeros enteros. IDealmente le pasarias unmeros entre 0 y 1, pero tambien valen cualquier otro imentras sea entero.
        //Haremos una scale que sea la division. 
		if (scale <= 0) {
			scale = 0.0001f;
		}
        //max y min

		float maxNoiseHeight = float.MinValue;
		float minNoiseHeight = float.MaxValue;
        //mitad del tablero. Eso es para saber donde esta el centro

		float halfWidth = mapWidth / 2f;
		float halfHeight = mapHeight / 2f;

        //vamos a rellenar el mapa de alturas. Esto es un mapa que nos va a decir para cada x-z su altura correspondiente.

		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
		        //amplitud. "Altura maxima"
				float amplitude = 1;
				
				//frecuencia. "Cuan Escarpado esta el terreno"

				float frequency = 1;
				
				//Altura de cada punto del mapa
				float noiseHeight = 0;

				//vamos a tener multiples perlins.
				for (int i = 0; i < octaves; i++) {
                    //coges x la divides por lae scala para que no sea entero y lo multiplicas por la frecuencia y le sumas la seed.
					float sampleX = (x-halfWidth) / scale * frequency + octaveOffsets[i].x;
					float sampleY = (y-halfHeight) / scale * frequency + octaveOffsets[i].y;

                    //cogemos este perlin. Quiero que el Perlin me de un valor entre -1 y 1
					float perlinValue = Mathf.PerlinNoise (sampleX, sampleY) * 2 - 1;
                    //la altura es un acumulador. Le sumaras el valor de la priemra octava, segunda octava,etc
					noiseHeight += perlinValue * amplitude;
                    //multiplicaremos la amplitud por la persisencia. Cada octava tendra menos amplitud que la anterior
					amplitude *= persistance;
                    //multiplicamos la frecuencia por la lacunaridad. Cada octava tendra mas frecuencia que la anteiror
                    //multiplicamos la frecuencia por la lacunaridad. Cada octava tendra mas frecuencia que la anteiror
					frequency *= lacunarity;
				}

                //actualizmoas el minimo y maximo del mapa
                //actualizmoas el minimo y maximo del mapa
				if (noiseHeight > maxNoiseHeight) {
					maxNoiseHeight = noiseHeight;
				} else if (noiseHeight < minNoiseHeight) {
					minNoiseHeight = noiseHeight;
				}
                //ponemos en el mapa el valor calculado
				noiseMap [x, y] = noiseHeight;
			}
		}

		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
                //LERP. Interpolacion Lineal - le pasas maximo, minimo y valor. Y te devuelve un numero enre 0 y 1 que representa donde esta el valor entre maximo y minimo
                //InverseLerp es al reves. Te va a dar un numero entre minimo y maximo si le das el numero entre 0 y 1.
				noiseMap [x, y] = Mathf.InverseLerp (minNoiseHeight, maxNoiseHeight, noiseMap [x, y]);
			}
		}

		return noiseMap;
	}

}
