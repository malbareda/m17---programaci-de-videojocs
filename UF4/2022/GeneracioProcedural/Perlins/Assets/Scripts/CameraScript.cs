using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Plane;

    [SerializeField]
    private float m_MovementSpeed = 20f;
    [SerializeField]
    private float m_MouseSensitivity = 20f;
    [SerializeField]
    private bool m_InvertMouseY = false;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        //Show/Hide texture plane
        if (Input.GetKeyDown(KeyCode.T))
            m_Plane.SetActive(!m_Plane.activeSelf);


        //Move Camera
        Vector3 movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            movement += transform.forward;
        if (Input.GetKey(KeyCode.S))
            movement -= transform.forward;
        if (Input.GetKey(KeyCode.A))
            movement -= transform.right;
        if (Input.GetKey(KeyCode.D))
            movement += transform.right;

        transform.position += movement.normalized * m_MovementSpeed * Time.deltaTime;

        //Rotate Camera
        if(Input.GetMouseButton(1))
            transform.localEulerAngles = transform.localEulerAngles
                                        + Vector3.right * Input.GetAxis("Mouse Y") * Time.deltaTime * m_MouseSensitivity * (m_InvertMouseY ? 1 : -1)
                                        + Vector3.up * Input.GetAxis("Mouse X") * Time.deltaTime * m_MouseSensitivity;
    }


}
