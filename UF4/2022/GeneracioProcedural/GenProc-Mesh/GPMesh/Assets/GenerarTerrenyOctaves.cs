﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerarTerrenyOctaves : MonoBehaviour
{


    public int ampladax = 256;
    public int llargadaz = 256;
    public int amplitud = 20;
    public float noiseScale;

    public int octaves;
    public float lacunarity;
    [Range(0, 1)]
    public float persistence;

    public int seed;
    public Vector2 offset;

    public bool autoUpdate;

    // Start is called before the first frame update
    void Start()
    {

    }

    private TerrainData GenerarProceduralment(TerrainData td)
    {
        td.heightmapResolution = ampladax + 1;
        td.size = new Vector3(ampladax, amplitud, llargadaz);
        td.SetHeights(0, 0, GenerarAltures());
        return td;
    }

    private float[,] GenerarAltures()
    {
        float[,] noiseMap = Noise.GenerateNoiseMap(ampladax, llargadaz, seed, noiseScale, octaves, lacunarity, persistence, offset);
        return noiseMap;
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Terrain>().terrainData = GenerarProceduralment(this.GetComponent<Terrain>().terrainData);
    }
}
