﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerarTerrenyProcedural : MonoBehaviour
{

    public int amplitud;
    public float freq;

    public float seedx;
    public float seedz;

    public int ampladax=256;
    public int llargadaz=256;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private TerrainData GenerarProceduralment(TerrainData td)
    {
        td.heightmapResolution = ampladax + 1;
        td.size = new Vector3(ampladax, amplitud, llargadaz);
        td.SetHeights(0, 0, GenerarAltures());
        return td;
    }

    private float[,] GenerarAltures()
    {
        float[,] altures = new float[ampladax, llargadaz];
        for(int x = 0; x < ampladax; x++)
        {
            for(int z = 0; z < llargadaz; z++)
            {
                altures[x, z] = Mathf.PerlinNoise(x/freq+seedx, z/freq+seedz);
            }
        }
        return altures;
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Terrain>().terrainData = GenerarProceduralment(this.GetComponent<Terrain>().terrainData);
    }
}
