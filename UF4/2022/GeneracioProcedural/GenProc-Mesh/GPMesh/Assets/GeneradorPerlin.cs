﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorPerlin : MonoBehaviour
{

    public int amplitud = 20;

    public int ampladax = 256;
    public int llargadaz = 256;
    public float freq = 20;

    public float seed = 2313223f;

    private void Start()
    {
        Terrain terrain = GetComponent<Terrain>();
        terrain.terrainData = GenerarTerrain(terrain.terrainData);
    }

    private TerrainData GenerarTerrain(TerrainData terrainData)
    {
        terrainData.heightmapResolution = ampladax + 1;
        terrainData.size = new Vector3(ampladax, amplitud, llargadaz);

        terrainData.SetHeights(0, 0, GenerarAltura());

        return terrainData;

    }

    private float[,] GenerarAltura()
    {
        float[,] altures = new float[ampladax,llargadaz];
        for(int x = 0; x < ampladax; x++)
        {
            for(int z = 0; z < llargadaz; z++)
            {
                altures[x, z] = Mathf.PerlinNoise(x / freq +seed, z / freq + seed);
            }
        }
        return altures;
    }
}
