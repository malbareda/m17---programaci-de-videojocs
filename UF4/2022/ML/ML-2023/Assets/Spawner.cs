using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject cosa;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(algo());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator algo()
    {
        while (true)
        {
            yield return new WaitForSeconds(5f);
            GameObject a = Instantiate(cosa);

            a.transform.position = new Vector3(10, UnityEngine.Random.Range(-5f, 5f), 0);
        }
    }
}
