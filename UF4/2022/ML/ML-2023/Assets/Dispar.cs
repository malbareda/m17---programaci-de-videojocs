using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dispar : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = Vector3.right*3;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "enemic")
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
    }
}
