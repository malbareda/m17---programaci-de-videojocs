using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using UnityEditor.Presets;
using UnityEngine.SocialPlatforms.Impl;

public class Pj : Agent
{
    private int score = 0;
    private int max = 0;
    private Vector3 posInicial;
    private float spd = 2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void OnEpisodeBegin()
    {
        base.OnEpisodeBegin();
        StartCoroutine(segundito());
        score = 0;
        this.transform.position = posInicial;
        this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, actions.ContinuousActions[0]*spd, 0);
    }

    private void FixedUpdate()
    {
        
        RequestDecision();
        
    }

    public override void Initialize()
    {
        base.Initialize();
        posInicial = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator segundito()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            AddReward(0.1f);
            
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
       
        if (collision.transform.tag == "enemic")
        {
            Destroy(collision.gameObject);
            AddReward(-1f);
            Debug.Log(GetCumulativeReward());
            if (score > max)
            {
                max = score;
                this.transform.parent.GetChild(4).gameObject.GetComponent<TMPro.TextMeshPro>().text = score + "";
            }

            EndEpisode();
        }
    }
}
