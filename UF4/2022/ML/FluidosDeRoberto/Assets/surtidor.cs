using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class surtidor : MonoBehaviour
{
    public int caudal;
    public GameObject particula;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < caudal; i++)
        {
            GameObject particulaPrefab = Instantiate(particula);
            particulaPrefab.GetComponent<Rigidbody2D>().AddForce(new Vector2(10f, 0));
            particulaPrefab.transform.position = this.transform.position;
        }
    }
}
