using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class TargetPositionAuthoring : MonoBehaviour {


}

public class TargetPositionBaker : Baker<TargetPositionAuthoring> {

    public override void Bake(TargetPositionAuthoring authoring) {
        AddComponent(new TargetPositionComponent {
        });
    }

}