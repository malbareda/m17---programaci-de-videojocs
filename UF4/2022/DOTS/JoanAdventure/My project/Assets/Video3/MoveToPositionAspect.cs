using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public readonly partial struct MoveToPositionAspect : IAspect {

    private readonly Entity entity;

    //NOMES AGAFARA ROBERTOS
    private readonly TransformAspect transformAspect;
    private readonly RefRO<Joan1SpeedComponent> speed;
    private readonly RefRW<TargetPositionComponent> targetPosition;


    public void Move(float deltaTime, float3 target) {
        // Calculate dir
        float3 direction = math.normalize(target - transformAspect.LocalPosition);
        // Move
        Debug.Log(direction);
        transformAspect.LocalPosition += direction * deltaTime * speed.ValueRO.value;
    }

    //public void TestReachedTargetPosition(RefRW<RandomComponent> randomComponent) {
    //    float reachedTargetDistance = .5f;
    //    if (math.distance(transformAspect.Position, targetPosition.ValueRW.value) < reachedTargetDistance) {
    //        // Generate new random target position
    //        targetPosition.ValueRW.value = GetRandomPosition(randomComponent);
    //    }
    //}

    //private float3 GetRandomPosition(RefRW<RandomComponent> randomComponent) {
    //    return new float3(
    //        randomComponent.ValueRW.random.NextFloat(0f, 15f),
    //        0,
    //        randomComponent.ValueRW.random.NextFloat(0f, 15f)
    //    );
    //}

}