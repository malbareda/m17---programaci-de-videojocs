using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

[BurstCompile]
public partial struct RobertoISystem : ISystem {

    [BurstCompile]
    public void OnCreate(ref SystemState state) {
    }

    [BurstCompile]
    public void OnDestroy(ref SystemState state) {
    }

    [BurstCompile]
    public void OnUpdate(ref SystemState state) {
        //RefRW<RandomComponent> randomComponent = SystemAPI.GetSingletonRW<RandomComponent>();

        float deltaTime = SystemAPI.Time.DeltaTime;
        float3 target = StaticHelper.PinaPosition();
        Debug.Log(target);
        JobHandle jobHandle = new MoveJob {
            deltaTime = deltaTime,
            target = target
        }.ScheduleParallel(state.Dependency);

        jobHandle.Complete();

        //new TestReachedTargetPositionJob {
        //    randomComponent = randomComponent
        //}.Run();
    }

}

[BurstCompile]
public partial struct MoveJob : IJobEntity {

    public float deltaTime;
    public float3 target;

    [BurstCompile]
    public void Execute(MoveToPositionAspect moveToPositionAspect) {
        moveToPositionAspect.Move(deltaTime,target);
    }

}

//[BurstCompile]
//public partial struct TestReachedTargetPositionJob : IJobEntity {

//    [NativeDisableUnsafePtrRestriction] public RefRW<RandomComponent> randomComponent;

//    [BurstCompile]
//    public void Execute(MoveToPositionAspect moveToPositionAspect) {
//        moveToPositionAspect.TestReachedTargetPosition(randomComponent);
//    }

//}