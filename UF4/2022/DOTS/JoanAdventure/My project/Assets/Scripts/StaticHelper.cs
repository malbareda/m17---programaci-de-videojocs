using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

public static class StaticHelper
{
    public static float3 CajaPosition()
    {
        return new float3(GameObject.Find("CajaRosa").transform.position);
    }

    public static float3 PinaPosition()
    {
        return new float3(GameObject.Find("Pina").transform.position);
    }
}
