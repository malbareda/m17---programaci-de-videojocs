using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public readonly partial struct Joan1Aspect : IAspect
{

    private readonly Entity entity;

    //NOMES AGAFARA JOANS
    private readonly TransformAspect transformAspect;
    private readonly RefRW<Joan1SpeedComponent> speed;
    private readonly RefRO<Joan1JoanTag> tag;  
    


    //NO SE PUEDE LLAMAR AL SYSTEMAPI DESDE LOS ASPECT
    public void Move(float dt)
    {
        
        transformAspect.Position += new Unity.Mathematics.float3(dt * -speed.ValueRO.value, 0, 0);
    }

    public void CheckIfOut()
    {
        
        if (transformAspect.Position.x<-8 || transformAspect.Position.x>8)
        {
            speed.ValueRW.value *= -1;
        }
    }


}