using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class Joan1JoanTagAuthoring : MonoBehaviour
{

}

public class Joan1JoanTagBaker : Baker<Joan1JoanTagAuthoring>
{

    public override void Bake(Joan1JoanTagAuthoring authoring)
    {
        AddComponent(new Joan1JoanTag());
    }

}