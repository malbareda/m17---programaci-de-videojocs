using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

[BurstCompile]
public partial struct Joan1System : ISystem
{

    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
    }

    [BurstCompile]
    public void OnDestroy(ref SystemState state)
    {
    }

    [BurstCompile]
    public void OnUpdate(ref SystemState state)
    {

        float dt = SystemAPI.Time.DeltaTime;

        //foreach ((TransformAspect ta, RefRO<Joan1SpeedComponent> spd) in SystemAPI.Query<TransformAspect, RefRO<Joan1SpeedComponent>>())
        //{
        //    ta.Position += new Unity.Mathematics.float3(dt * -spd.ValueRO.value, 0, 0);
        //}

        //foreach (Joan1Aspect aspect in SystemAPI.Query<Joan1Aspect>())
        //{
        //    aspect.Move(SystemAPI.Time.DeltaTime);
        //    aspect.CheckIfOut();
        //}




        JobHandle jobHandle = new Joan1MoveJob
        {
            deltaTime = dt
        }.ScheduleParallel(state.Dependency);
        //TRES FORMAS DE LLAMAR A UNA JOB
        //1 RUN, EN EL THREAD PRINCIPAL
        //2 SCHEDULE, EN UN THREAD PARALELO PERO TODO EL PROCESO EN EL MISMO THREAD
        //3 SCHEDULEPARALLEL, EN UN THREAD PARALELO QUE SE ROMPE EN OTROS THREADS SI HAY VARIAS ENTIDADES

        jobHandle.Complete(); //Hasta que no acaben todos no pasara por aqui

        JobHandle jobHandle2 = new Joan1CheckJob
        {
        }.ScheduleParallel(state.Dependency);

        jobHandle2.Complete();
    }

}

[BurstCompile]
public partial struct Joan1MoveJob : IJobEntity
{
    //DENTRO DE UN JOB TAMPOCO PUEDES LLAMAR A LA SYSTEMAPI
    public float deltaTime;

    [BurstCompile]
    public void Execute(Joan1Aspect aspect)
    {
        aspect.Move(deltaTime);
    }

}

[BurstCompile]
public partial struct Joan1CheckJob : IJobEntity
{


    [BurstCompile]
    public void Execute(Joan1Aspect aspect)
    {
        aspect.CheckIfOut();
    }

}

