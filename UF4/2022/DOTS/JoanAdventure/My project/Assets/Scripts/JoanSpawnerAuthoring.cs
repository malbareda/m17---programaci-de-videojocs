using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class JoanSpawnerAuthoring : MonoBehaviour
{

    public GameObject joanPrefab;
    public GameObject robertoPrefab;
    public float delay;

}

public class JoanSpawnerBaker : Baker<JoanSpawnerAuthoring>
{

    public override void Bake(JoanSpawnerAuthoring authoring)
    {
        AddComponent(new JoanSpawnerComponent
        {
            joanPrefab = GetEntity(authoring.joanPrefab),
            robertoPrefab = GetEntity(authoring.robertoPrefab),
            delay = authoring.delay,
            time = authoring.delay,
        });
    }

}
