using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class PlayerVisual : MonoBehaviour {


    private Entity targetEntity;

    private void LateUpdate() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            targetEntity = GetRandomEntity();
            Debug.Log(targetEntity);
            World.DefaultGameObjectInjectionWorld.EntityManager.SetComponentData<Joan1SpeedComponent>(targetEntity, new Joan1SpeedComponent
            {
                value = 40
            });
        }

        if (targetEntity != Entity.Null) {

            Vector3 followPosition =
                World.DefaultGameObjectInjectionWorld.EntityManager.GetComponentData<WorldTransform>(targetEntity).Position;
            transform.position = followPosition;
        }
    }

    private Entity GetRandomEntity() {
        EntityQuery playerTagEntityQuery = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(Joan1JoanTag));
        NativeArray<Entity> entityNativeArray = playerTagEntityQuery.ToEntityArray(Unity.Collections.Allocator.Temp);
        if (entityNativeArray.Length > 0) {
            return entityNativeArray[Random.Range(0, entityNativeArray.Length)];
        } else {
            return Entity.Null;
        }
    }

}