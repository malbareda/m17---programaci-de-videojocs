using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public struct JoanSpawnerComponent : IComponentData
{

    public Entity joanPrefab;
    public Entity robertoPrefab;
    public float delay;
    public float time;

}