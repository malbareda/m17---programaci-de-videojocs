using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class Joan1SpeedAuthoring : MonoBehaviour
{

    public float valueDesdeInspector;

}

public class Joan1SpeedBaker : Baker<Joan1SpeedAuthoring>
{

    public override void Bake(Joan1SpeedAuthoring authoring)
    {
        AddComponent(new Joan1SpeedComponent
        {
            value = authoring.valueDesdeInspector
        });
    }

}