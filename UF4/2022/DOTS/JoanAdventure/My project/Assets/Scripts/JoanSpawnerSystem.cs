using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Entities;
using UnityEngine;
using Unity.Transforms;
using Unity.Mathematics;

[BurstCompile]
public partial struct JoanSpawnerSystem : ISystem
{
    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
    }

    [BurstCompile]
    public void OnDestroy(ref SystemState state)
    {
    }

    [BurstCompile]
    public void OnUpdate(ref SystemState state)
    {
        //Debug.Log("la rosa " + StaticHelper.CajaPosition());  
        //Debug.Log("hey");
        //SystemAPI.get
        RefRW<RandomComponent> randomComponent = SystemAPI.GetSingletonRW<RandomComponent>();
        //EL RANDOM COMPOENENT SIEMPRE EN ESCIRTURA O SI NO DARA SIEMPRE EL MISMO NUMERO

        //ECB SIRVE PARA HACER CAMBIOS ESTRUCTURALES
        //CREAR O DESTRUIR ENTITIES
        //CREAR O DESTRUIR COMPONENTES DENTRO DE UNA ENTITY
        EntityCommandBuffer entityCommandBuffer =
            SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(state.WorldUnmanaged);

        foreach (RefRW<JoanSpawnerComponent> joanSpawnerComponent in SystemAPI.Query < RefRW<JoanSpawnerComponent>>())
        {
            joanSpawnerComponent.ValueRW.time -= SystemAPI.Time.DeltaTime;

            if (joanSpawnerComponent.ValueRW.time <= 0)
            {
                //Debug.Log("hey2");
                joanSpawnerComponent.ValueRW.time = joanSpawnerComponent.ValueRO.delay;
                bool b = randomComponent.ValueRW.random.NextBool();
                Entity spawnedEntity;
                if (b)
                {
                    spawnedEntity = entityCommandBuffer.Instantiate(joanSpawnerComponent.ValueRO.joanPrefab);
                }
                else
                {
                    spawnedEntity = entityCommandBuffer.Instantiate(joanSpawnerComponent.ValueRO.robertoPrefab);
                }
                
                entityCommandBuffer.SetComponent(spawnedEntity, new Joan1SpeedComponent
                {
                    value = randomComponent.ValueRW.random.NextFloat(1f, 10f)
                });
                entityCommandBuffer.SetComponent(spawnedEntity, WorldTransform.FromPosition(new Unity.Mathematics.float3(randomComponent.ValueRW.random.NextFloat(-8f, 8f), 0, randomComponent.ValueRW.random.NextFloat(-8f, 8f))));
            }
        }
    }

}