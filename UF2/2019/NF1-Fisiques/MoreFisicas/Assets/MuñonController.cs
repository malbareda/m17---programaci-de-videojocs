﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuñonController : MonoBehaviour
{
    public int status;
    private bool cBreak;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("d"))
        {
            GameObject ataque = this.gameObject.transform.GetChild(2).gameObject;
            ataque.transform.localPosition = new Vector2(2, 2);
            //llamamos a la corrutina
            cBreak = false;
            StartCoroutine(MinCombo(1, 1));
            StartCoroutine(MaxCombo(2,status));

        }
        else if (Input.GetKeyDown("a"))
        {
            if (status == 1)
            {
                GameObject ataque = this.gameObject.transform.GetChild(2).gameObject;
                ataque.transform.localPosition = new Vector2(2, 1);
            }
            else
            {
                cBreak = true;
            }
        }

    }

    //llama a la ecorrutina
    IEnumerator MaxCombo(float f, int st)
    {
        //Debug.Log(Time.time);
        // ESPERA durante un segundo. el resto del codigo sigue ejecutandose.
        yield return new WaitForSeconds(f);
        Debug.Log("ahora no");
        //Debug.Log(Time.time);
        if (status == st)
        {
            status = 0;
        }
    }
    IEnumerator MinCombo(float f, int st)
    {
        // ESPERA durante un segundo. el resto del codigo sigue ejecutandose.
        yield return new WaitForSeconds(f);
        Debug.Log("ahora");
        if (!cBreak)
        {
            status = st;
        }
        
    }
}
