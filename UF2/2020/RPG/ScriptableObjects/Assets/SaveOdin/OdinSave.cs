using OdinSerializer;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class OdinSave : MonoBehaviour
{
    public A data;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            //A data;
            byte[] serializedData = SerializationUtility.SerializeValue<A>(data, DataFormat.JSON);

            string base64 = System.Convert.ToBase64String(serializedData);
            //File.WriteAllBytes("testOdin.json", serializedData);
            File.WriteAllText("testOdin.json", base64);

            string newBase64=File.ReadAllText("testOdin.json");
            byte[] postFile= System.Convert.FromBase64String(newBase64);
            //byte[] postFile = File.ReadAllBytes("testOdin.json");
            

            print(postFile);


            A newData = SerializationUtility.DeserializeValue<A>(postFile, DataFormat.JSON);

            print(newData.b.c.c);
        }
    }
}
