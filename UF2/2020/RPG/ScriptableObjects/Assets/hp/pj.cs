﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pj : MonoBehaviour
{
    public HPPlayer hp;
    public GameEvent dead;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "bullet")
        {
            hp.hp--;
            if (hp.hp <= 0)
            {
                dead.Raise();
            }
        }
    }
}
