﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    private int indexArma;
    public Arma[] armes;
    private Arma armaActiva;
    public GameObject bullet;
    private int balesActuals;

    private bool recharging = false;
    // Start is called before the first frame update
    void Start()
    {
        indexArma = 0;
        armaActiva = armes[indexArma];
        balesActuals = armaActiva.bales;
        this.GetComponent<SpriteRenderer>().color = armaActiva.color;
       
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetKeyDown(KeyCode.Space))
        {
            shoot();
        }else if (Input.GetKeyDown(KeyCode.Q))
        {
            indexArma++;
            if (indexArma >= armes.Length) indexArma = 0;
            armaActiva = armes[indexArma];
            balesActuals = armaActiva.bales;
            this.GetComponent<SpriteRenderer>().color = armaActiva.color;
        }else if (Input.GetKeyDown("r"))
        {
            StartCoroutine(recharge());
        }
    }

    private void shoot()
    {
        if (!recharging) {
            for (int i = 0; i < armaActiva.balesEnUnSoloDisparo; i++) {

                GameObject newBullet = Instantiate(bullet, this.transform.position, this.transform.rotation);
                newBullet.transform.localScale *= armaActiva.scale;
                newBullet.GetComponent<SpriteRenderer>().color = armaActiva.color;
                float randomRange = UnityEngine.Random.Range(-armaActiva.dispersionAngle / 2, armaActiva.dispersionAngle / 2);
                newBullet.transform.Rotate(new Vector3(0, 0, randomRange));
                newBullet.GetComponent<Rigidbody2D>().gravityScale = armaActiva.gravity;
                newBullet.GetComponent<Rigidbody2D>().velocity = newBullet.transform.right * armaActiva.spd;
            }
            balesActuals--;
            if (balesActuals <= 0)
            {
                StartCoroutine(recharge());
            }
        }
    }


    IEnumerator recharge()
    {
        this.recharging = true;
        yield return new WaitForSeconds(armaActiva.rechargeTime);
        this.recharging = false;
        balesActuals = armaActiva.bales;


    }
}
