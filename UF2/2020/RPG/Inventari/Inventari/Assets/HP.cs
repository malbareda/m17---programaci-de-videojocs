﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class HP : ScriptableObject
{
    public int hpmax;
    public int hp;

    public void heal(HealingItem item)
    {
        hp += item.hp;
        if (hp > hpmax) hp = hpmax;
    }

}
