using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonatgeManager : MonoBehaviour
{
    double hp;
    public Personatges data;
    double mana;
    // Start is called before the first frame update
    void Start()
    {
        this.hp = data.max_hp;
        mana = data.max_mana;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void danyar(double dany)
    {
        hp -= dany;
        if (hp < 0)
        {
            print("Personatge Debilitat");
        }
    }

    public void atacar()
    {
        int r = Random.Range(0, data.atacs.Length);
        Magia atacRandom = data.atacs[r];
        if (this.mana > atacRandom.costMana)
        {
            this.mana -= atacRandom.costMana;
            //atacRandom.atacar(altrePersonatge);
        }

    }
}
