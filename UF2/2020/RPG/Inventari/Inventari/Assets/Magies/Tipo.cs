using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Tipo : ScriptableObject
{
    public string nom;
    public List<string> resistencies;
    public List<string> debilitats;
    public List<string> inmunitats;
}
