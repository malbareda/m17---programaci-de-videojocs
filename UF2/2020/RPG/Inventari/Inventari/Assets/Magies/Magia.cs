using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class Magia : ScriptableObject
{
    public int damage;
    public float acc;
    public int costMana;
    public Tipo tipus;



    public void atacar(PersonatgeManager p)
    {
        if (p.data.tipo.inmunitats.Contains(tipus.name))
        {
            Debug.Log("es inmune");
            p.danyar(acc * 0);
        }
        else if (p.data.tipo.resistencies.Contains(tipus.name))
        {
            Debug.Log("es poco efectivo");
            p.danyar(acc * 0.5);
        }
        else if (p.data.tipo.debilitats.Contains(tipus.name))
        {
            Debug.Log("es superefectivo");
            p.danyar(   acc * 2);
        }
    }

}
