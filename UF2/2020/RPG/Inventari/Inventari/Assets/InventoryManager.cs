﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public Image cursor;
    public Text[] text = new Text[18];
    public List<Item> llista = new List<Item>();
    public HealingItemUsedEvent healingItemEvent;
    int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        actualizarUI();
    }

    public void actualizarUI()
    {
        for (int i = 0; i < llista.Count; i++)
        {
            if (llista[i] == null)
            {
                text[i].text = "Empty";
            }
            else
            {
                text[i].text = llista[i].name + " : " + llista[i].quantity;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("s"))
        {
            if(index!=8 && index != 17)
            {
                index++;
                cursor.rectTransform.anchoredPosition = new Vector2(cursor.rectTransform.anchoredPosition.x, cursor.rectTransform.anchoredPosition.y - 50);
            }
        }else if (Input.GetKeyDown("w"))
        {
            if (index != 0 && index != 9)
            {
                index--;
                cursor.rectTransform.anchoredPosition = new Vector3(cursor.rectTransform.anchoredPosition.x, cursor.rectTransform.anchoredPosition.y + 50);
            }
        }
        else if (Input.GetKeyDown("d"))
        {
            if (index < 9)
            {
                index+=9;
                cursor.rectTransform.anchoredPosition = new Vector3(cursor.rectTransform.anchoredPosition.x+450, cursor.rectTransform.anchoredPosition.y);
            }
        }
        else if (Input.GetKeyDown("a"))
        {
            if (index > 8)
            {
                index-=9;
                cursor.rectTransform.anchoredPosition = new Vector3(cursor.rectTransform.anchoredPosition.x-450, cursor.rectTransform.anchoredPosition.y);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            if (((HealingItem)llista[index]).quantity > 0)
            {
                ((HealingItem)llista[index]).quantity--;
                healingItemEvent.RaiseItem((HealingItem)llista[index]);
                actualizarUI();
            }
            
        }
    }
}
