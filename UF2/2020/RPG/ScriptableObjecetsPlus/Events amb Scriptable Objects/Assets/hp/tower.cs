﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tower : MonoBehaviour
{
    public Transform pj;
    public GameObject bullet;
    public HPPlayer hp;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("a", 0, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void a()
    {
        if (hp.hp > 0)
        {


            GameObject newBullet = Instantiate(bullet);
            newBullet.transform.position = this.transform.position;
            newBullet.transform.right = pj.position - newBullet.transform.position;
        }
    }
}
