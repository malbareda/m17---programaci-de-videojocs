﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class HPPlayer : ScriptableObject
{
    public float maxhp;
    public float hp;

}
