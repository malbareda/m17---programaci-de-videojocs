﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class curling : MonoBehaviour
{
    float min = 0;
    float max = 1000;
    float value = 1000;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (Input.GetKey("space"))
        {
            if(value>min)
            value--;
        }
        else
        {
            if (value < max)
                value++;
        }
        this.GetComponent<BoxCollider2D>().sharedMaterial.friction = value/1000000;
        print(this.GetComponent<BoxCollider2D>().sharedMaterial.friction);
    }
}
