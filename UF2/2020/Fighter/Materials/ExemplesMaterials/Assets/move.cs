﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(70f, 0));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("space"))
        {
            print("a");
            this.GetComponent<Rigidbody2D>().sharedMaterial.bounciness += 0.01f;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().sharedMaterial.bounciness -= 0.01f;
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "goal")
        {
            print("WIN");
        }
    }
}
