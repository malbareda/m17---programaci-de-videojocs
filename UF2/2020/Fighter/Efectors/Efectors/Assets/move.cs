﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("d"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(3, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        if (Input.GetKeyDown("w"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));
        }
    }
}
