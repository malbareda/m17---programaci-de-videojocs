﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    private Joan_nomono[] joans = new Joan_nomono[50000];
    public GameObject joan;

    void Start()
    {
        for (int i = 0; i < joans.Length; i++)
        {
            Joan_nomono newJoan = new Joan_nomono();
            float y = Random.Range(-5f, 5f);
            GameObject newGO = Instantiate(joan,new Vector2(-10, y), Quaternion.identity);
            newJoan.trans = newGO.transform;
            joans[i] = newJoan;
        }
    }

    void Update()
    {
        for (int i = 0; i < joans.Length; i++)
        {
            joans[i].Moute();
        }
    }
    
}
