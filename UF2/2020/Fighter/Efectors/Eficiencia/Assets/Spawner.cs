﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject moverPrefab;

    void Awake()
    {
        for (int i = 0; i < 50000; i++)
        {
            float y = Random.Range(-5f, 5f);
            Instantiate(moverPrefab,new Vector2(-10,y),Quaternion.identity);
        }
    }
}
