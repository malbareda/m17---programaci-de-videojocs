﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//NO ES MONOBEHAVIOUR 
public class Joan_nomono
{
    //la transform es publica perque no la pots assignar diretament
    public Transform trans;
 
    public void Moute()
    {
        trans.position += Vector3.right * Time.deltaTime;
    }

}
