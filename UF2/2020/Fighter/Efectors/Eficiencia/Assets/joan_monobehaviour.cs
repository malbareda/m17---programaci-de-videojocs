﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class joan_monobehaviour : MonoBehaviour
{
    Transform trans;

    void Start()
    {
        trans = this.transform;
    }

    void Update()
    {
        //vector3 right: La teva dreta (depenent de la rotació)
        //Time.deltatime = el temps entre frame i frame
        trans.position += Vector3.right * Time.deltaTime;
    }
}
