﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goToPedro : MonoBehaviour
{
    public GameObject pedro;
    public float vel = 3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Time.deltaTime. Temps desde l'ultim frame. per tant es inversament proporcional als fps
        //fent aquesta multiplicacio t'assegures que a una maquina a molts fps i a pocs fps li vagi a la mateixa velocitat.
        float step = vel * Time.deltaTime;

        this.transform.position = Vector2.MoveTowards(this.transform.position, pedro.transform.position, step);
        
    }
}
