﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combo : MonoBehaviour
{
    int comboStatus = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        //combo q y despues en menos de 0.5s w
        if (Input.GetKeyDown("q"))
        {
            StartCoroutine(combo("q"));
        }else if (Input.GetKeyDown("w",0.5f))
        {
            StartCoroutine(combo("w",0.7f));
        }else if (Input.GetKeyDown("e")){
            StartCoroutine(combo("e",0f));
        }
    }

    IEnumerator combo(string s, float wait)
    {
        if (s == "q")
        {
            if (comboStatus == 0) {
                print("combo1");

                comboStatus = 1;
                yield return new WaitForSeconds(wait);
                if (comboStatus == 1)
                {
                    print("combo fallado por tiempo");
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                print("tecla pulsada a destiempo");
            }

        }else if(s == "w")
        {
            if (comboStatus == 1)
            {
                print("combo2");
                comboStatus = 2;
                yield return new WaitForSeconds(wait);
                if (comboStatus == 2)
                {
                    print("combo fallado por tiempo");
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                print("tecla pulsada a destiempo");
            }
        }
        else if (s == "e")
        {
            if (comboStatus == 2)
            {
                print("combo3");
                comboStatus = 3;
                print("Combo completo");
                comboStatus = 0;
            }
            else
            {
                comboStatus = 0;
                print("tecla pulsada a destiempo");
            }
        }
        else
        {
            comboStatus = 0;
            print("tecla incorrecta");
        }
    }
}
