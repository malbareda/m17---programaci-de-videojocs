﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aCasitaQueHaceFrio : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(new Vector2(0, 0), Vector3.forward, 30 * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.name == "covid")
        {
            //lanzamos una corrutina
            print("covid");
            //una corrutina puede tener argumentos
            StartCoroutine(rojo(0.1f));
        }
    }

    //UNA CORRUTINA ES UNA FUNCION QUE DEVOLVERA SIEMPRE UN IENUMERATOR CON UN YIELD
    IEnumerator rojo(float rate)
    {
        float i = 0;
        

        while (i < 1)
        {
            this.GetComponent<SpriteRenderer>().color = new Color(1f, 1f-i, 1f-i);
            i += Time.deltaTime*rate;
            //yield return, devuelve el control al hilo principal;
            yield return 0;
        }
    }
}
