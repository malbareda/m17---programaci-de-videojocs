using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class attack : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "enano")
        {
            print("attack");

            this.GetComponentInParent<enemigo>().attack(true);

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "enano")
        {
            print("attack");

            this.GetComponentInParent<enemigo>().attack(false);

        }
    }
}
