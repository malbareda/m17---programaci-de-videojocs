using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    Rigidbody2D m_RigidBody;
    [SerializeField]
    float m_Speed = 4;
    public int comboStatus = 0;
    bool windowCombo1 = false;
    bool windowCombo2 = false;
    void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
    }

    
    void Update()
    {
        Vector3 movement = Vector3.zero;

        if(Input.GetKey(KeyCode.W))
        {
            movement += transform.up;
        }
        if (Input.GetKey(KeyCode.S))
        {
            movement -= transform.up;
        }
        if (Input.GetKey(KeyCode.A))
        {
            movement -= Vector3.right;
        }
        if (Input.GetKey(KeyCode.D))
        {
            movement += Vector3.right;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(comboStatus==1 && windowCombo1)
            {
                comboStatus=2;
                StartCoroutine(combo2());
                //animator tal
            }
            else if(comboStatus==2 && windowCombo2)
            {
                comboStatus = 3;
                //animator pascual
            }
            else
            {
                comboStatus = 1;
                StartCoroutine(combo());
                //animator rajoy
            }
        }
        m_RigidBody.velocity = movement.normalized * m_Speed;

        //rotem segons on estem anant
        if(movement.x < 0)
        {
            transform.rotation = Quaternion.Euler(Vector3.up*180);
        }
        else if(movement.x > 0)
        {
            transform.rotation = Quaternion.identity;
        }
    }

    public IEnumerator combo()
    {
        yield return new WaitForSeconds(0.5f);
        windowCombo1 = true;
        yield return new WaitForSeconds(1.5f);
        windowCombo1 = false;
    }

    public IEnumerator combo2()
    {
        yield return new WaitForSeconds(1f);
        windowCombo2 = true;
        yield return new WaitForSeconds(1.5f);
        windowCombo2 = false;
    }
}
