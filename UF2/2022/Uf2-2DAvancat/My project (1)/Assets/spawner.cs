using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    public Oleada ol;
    public GameObject en;
    private int muertos = 0;

    public IEnumerator spawn()
    {
        print("nueva oleada " + ol.oleada);
        for(int i = 0; i < ol.oleada; i++)
        {
            yield return new WaitForSeconds(1f);
            GameObject newEnemigo = Instantiate(en);

        }
    }

    public void enemigoMuertoAbonoPaMiHuerto()
    {
        muertos++;
        if (muertos == ol.oleada)
        {
            ol.oleada++;
            StartCoroutine(spawn());
            muertos = 0;
        }

    }
}
