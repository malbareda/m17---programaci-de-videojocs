using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemigo : MonoBehaviour
{
    private GameObject target;
    private int status = 0;
    private Rigidbody2D m_RigidBody;
    private int m_Speed=4;
    private int hp = 5;
    public GameEvent muero;
    // Start is called before the first frame update
    void Start()
    {
        this.m_RigidBody = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = Vector3.zero;
        if (status == 0)
        {
            movement = this.transform.right;
            m_RigidBody.velocity = movement.normalized * m_Speed;

            if (movement.x < 0)
            {
                transform.rotation = Quaternion.Euler(Vector3.up * 180);
            }
            else if (movement.x > 0)
            {
                transform.rotation = Quaternion.identity;
            }
            m_RigidBody.velocity = movement.normalized * m_Speed;

        }
        if (status == 1)
        {
            if (this.transform.position.y < target.transform.position.y)
            {
                movement += transform.up;
            }
            if (this.transform.position.y > target.transform.position.y)
            {
                movement -= transform.up;
            }
            if (this.transform.position.x > target.transform.position.x)
            {
                movement -= Vector3.right;
            }
            if (this.transform.position.x < target.transform.position.x)
            {
                movement += Vector3.right;
            }
            m_RigidBody.velocity = movement.normalized * m_Speed;

            //rotem segons on estem anant
            if (movement.x < 0)
            {
                transform.rotation = Quaternion.Euler(Vector3.up * 180);
            }
            else if (movement.x > 0)
            {
                transform.rotation = Quaternion.identity;
            }
        }else if (status == 2)
        {
            m_RigidBody.velocity = Vector2.zero;

            if (movement.x < 0)
            {
                transform.rotation = Quaternion.Euler(Vector3.up * 180);
            }
            else if (movement.x > 0)
            {
                transform.rotation = Quaternion.identity;
            }
            //animacion de atacar
            print("atacar");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="enano")
        print("enemigo");
        if (collision.name == "hachacollider")
        {
            print("hacha");
            int a = collision.gameObject.transform.parent.GetComponent<CharacterMovement>().comboStatus;
            hp-=a;
            print(a);
            if (hp <= 0)
            {
                muero.Raise();
                Destroy(gameObject);
            }
        }
    }

    public void aggro(GameObject target)
    {
        print("masaggro");
        this.status = 1;
        this.target = target;
    }

    internal void attack(bool b)
    {
        
        print("masattack");
        if (b)
            this.status = 2;
        else this.status = 1;
    }
}
