using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CharData : ScriptableObject
{
    public int hp;
    public int mp;
    public int vidas;
}
