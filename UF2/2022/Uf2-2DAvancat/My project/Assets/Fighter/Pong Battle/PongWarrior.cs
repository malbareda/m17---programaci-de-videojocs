using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class PongWarrior : MonoBehaviour
{
    public delegate void OnHit(int hp);
    public event OnHit OnHitEvent;

    public InputAction upAct;
    public InputAction downAct;
    public InputAction crossAct;
    public InputAction northAct;
    public InputAction southAct;
    public InputAction westAct;
    public InputAction eastAct;




    public float delay;
    public float uptime;
    public float cooldown;

    private bool up;
    private bool down;
    private bool cross;
    private bool cd=false;
    // Start is called before the first frame update
    void Start()
    {
        upAct.started += upStart;
        downAct.started += downStart;
        crossAct.started += crossStart;
        crossAct.canceled += crossEnd;
        southAct.performed += south;
        westAct.performed += west;
        eastAct.performed += east;
        southAct.performed += south;
        up = true;
        down = false;
    }

    void OnEnable()
    {
        upAct.Enable();
        downAct.Enable();
        crossAct.Enable();
        northAct.Enable();
        southAct.Enable();
        westAct.Enable();
        eastAct.Enable();

    }

    void OnDisable()
    {
        upAct.Disable();
        downAct.Disable();
        crossAct.Disable();
        northAct.Disable();
        southAct.Disable();
        westAct.Disable();
        eastAct.Disable();

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print(":(");
        collision.gameObject.GetComponent<Ball>().ResetBall();
        int dmg = 10;
        OnHitEvent.Invoke(dmg);
    }

    public void upStart(InputAction.CallbackContext ctx)
    {
        up = true;
        down = false;
        print("upstart");
        this.transform.GetChild(0).GetComponent<Paddle>().setPaddleUp(true);
    }
    
    public void downStart(InputAction.CallbackContext ctx)
    {
        down = true;
        up = false;
        this.transform.GetChild(0).GetComponent<Paddle>().setPaddleUp(false);
    }
    
    public void crossStart(InputAction.CallbackContext ctx)
    {
        cross = true;
    }
    public void crossEnd(InputAction.CallbackContext ctx)
    {
        cross = false;
    }
    public void south(InputAction.CallbackContext ctx)
    {
        StartCoroutine(execute(1, Color.white));
    }

    public void west(InputAction.CallbackContext ctx)
    {
        StartCoroutine(execute(3, Color.red));
    }


    public void east(InputAction.CallbackContext ctx)
    {
        StartCoroutine(execute(4, Color.blue));
    }

    public void BallHit()
    {
        cd = false;
    }


    public IEnumerator execute(int status, Color c)
    {
        print("�");
        if (!cd)
        {
            print("?");
            cd = true;
            this.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = c;
            yield return new WaitForSeconds(delay);
            this.transform.GetChild(0).gameObject.GetComponent<Paddle>().active(true);
            this.transform.GetChild(0).GetComponent<Paddle>().setPaddle(up, down, cross, status);
            yield return new WaitForSeconds(uptime);
            this.transform.GetChild(0).gameObject.GetComponent<Paddle>().active(false);
            yield return new WaitForSeconds(cooldown);
            cd = false;
        }
    }
}
