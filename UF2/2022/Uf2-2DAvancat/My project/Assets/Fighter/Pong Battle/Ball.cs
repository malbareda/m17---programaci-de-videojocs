using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float spd;
    public int status;
    public Vector3 target;
    public bool comesFromLeft;
    // Start is called before the first frame update
    void Start()
    {
        ResetBall();
    }

    // Update is called once per frame
    void Update()
    {
        if (status == 3 )
        {
            if(comesFromLeft && this.transform.position.x > 0 || !comesFromLeft && this.transform.position.x < 0)
            {
                Vector3 dir = target - this.transform.position;
                dir.Normalize();
                this.GetComponent<Rigidbody2D>().velocity = dir * spd;
                status = 1;
            }
            
        }
        if(status==4 && comesFromLeft && this.transform.position.x>4.25f || status == 4 && !comesFromLeft && this.transform.position.x < -4.25f)
        {
            StartCoroutine(paradinha());
            status = 1;
        }
        }
    IEnumerator paradinha()
    {
        Vector3 vel = this.GetComponent<Rigidbody2D>().velocity;
        this.GetComponent<Rigidbody2D>().velocity=Vector3.zero;
        yield return new WaitForSeconds(1);
        print(vel);
        this.GetComponent<Rigidbody2D>().velocity = vel;
    }
    
    public void ResetBall()
    {
        spd = 1;
        this.GetComponent<Rigidbody2D>().velocity = Vector3.left;
        this.transform.position = new Vector3(0,spd,0);
    }
}
