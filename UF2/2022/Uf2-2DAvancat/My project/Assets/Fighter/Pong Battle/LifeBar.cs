using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour
{
    public GameObject warrior;
    // Start is called before the first frame update
    void Start()
    {
        warrior.GetComponent<PongWarrior>().OnHitEvent += dmg;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void dmg(int hp)
    {
        print("ouch");
        this.transform.localScale = new Vector2(this.transform.localScale.x - (hp / 100f), this.transform.localScale.y);
        this.GetComponent<Image>().color = new Color(this.GetComponent<Image>().color.r + hp / 100f, this.GetComponent<Image>().color.g - hp / 100f, 0);
    }
}
