using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    public Transform upPos;
    public Transform downPos;
    public Transform oupPos;
    public Transform odownPos;
    private bool up;
    private bool cross;
    private int status;
    public float spd;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setPaddle(bool up, bool down, bool cross, int status)
    {
        if (up)
        {
            this.transform.position = upPos.position;
            this.up = true;
        }else if (down)
        {
            this.transform.position = downPos.position;
            this.up = false;
        }
        this.cross = cross;
        this.status = status;

    }

    public void active (bool a)
    {
        if (a)
        {
            this.GetComponent<BoxCollider2D>().enabled = true;
            Color c=this.GetComponent<SpriteRenderer>().color;
            c.a = 1f;
            this.GetComponent<SpriteRenderer>().color=c;
        }
        else
        {
            this.GetComponent<BoxCollider2D>().enabled = false;
            
            this.GetComponent<SpriteRenderer>().color = new Color(1,1,1,0.2f);
            this.status = 1;
        }
    }

    public void setPaddleUp(bool u)
    {
        if (u)
        {
            this.transform.position = upPos.position;
            this.up = true;
        }
        else
        {
            this.transform.position = downPos.position;
            this.up = false;
        }
    }

    public void setPaddleStatus(int status, Color c)
    {
        this.GetComponent<SpriteRenderer>().color = c;
        this.status = status;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "ball")
        {
            this.transform.parent.GetComponent<PongWarrior>().BallHit();
            float bspd = collision.GetComponent<Ball>().spd;
            bspd+=spd;
            collision.GetComponent<Ball>().spd = bspd;
            collision.GetComponent<Ball>().comesFromLeft = (this.transform.position.x < 0);
            print("ok "+bspd+" "+cross);
            
            if (status == 3)
            {
                //efecto
                collision.GetComponent<Ball>().status = 3;
                if (up && cross || !up && !cross)
                {
                    collision.GetComponent<Ball>().target = oupPos.position;
                }
                else
                {
                    collision.GetComponent<Ball>().target = odownPos.position;
                }
                

            }
            
            collision.GetComponent<Ball>().status = status;

            
            if (cross)
                {
                    if (up)
                    {
                        Vector3 dir = this.odownPos.position - this.upPos.position;
                        dir.Normalize();
                        collision.gameObject.GetComponent<Rigidbody2D>().velocity = dir * bspd;
                    }
                    else
                    {
                        Vector3 dir = this.oupPos.position - this.downPos.position;
                        dir.Normalize();
                        collision.gameObject.GetComponent<Rigidbody2D>().velocity = dir * bspd;
                    }
                }
                else
                {
                    Vector3 dir = this.oupPos.position - this.upPos.position;
                    dir.Normalize();

                    collision.gameObject.GetComponent<Rigidbody2D>().velocity = dir * bspd;
                    
                }
            
        }
    }
}
