using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMCombo : MonoBehaviour
{
    /*
     * A - cop de puny
     * AA - cop de codic
     * AAA - wancho por ejemplo
     * B - putiaso
     * BB - gamepad anal
     * AB - agarre
     * AAB - Bongos Rusos
     * ABA - Teoria
     * AAABB - Luz Eterna (Discord en blanco)
     * 
     */
    private Status status;

    //just Unity things
    Coroutine corrutinaGuardada;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //print(status);
        if (Input.GetKeyDown(KeyCode.A))
        {
            if(status == Status.A)
            {
                print("cop de codic (AA)");
                status = Status.AA;
                StopCoroutine(corrutinaGuardada);
                corrutinaGuardada = StartCoroutine(ComboCooldown(0.5f));
            }else if(status == Status.AA)
            {
                print("wancho por ejemplo (AAA)");
                status = Status.AAA;
                StopCoroutine(corrutinaGuardada);
                corrutinaGuardada = StartCoroutine(ComboCooldown(0.5f));
            }
            else if(status == Status.AB)
            {
                print("Teoria (ABA)");
                status = Status.NONE; //perque ja ha acabat, ABA es un cop final de combo
                StopCoroutine(corrutinaGuardada);
                //corrutinaGuardada = StartCoroutine(ComboCooldown(0.5f));
            }
            else
            {
                print("cop de puny (A)");
                status = Status.A;
                //StopCoroutine(corrutinaGuardada);
                corrutinaGuardada = StartCoroutine(ComboCooldown(0.5f));
            }
        }else if (Input.GetKeyDown(KeyCode.B))
        {
            switch (status)
            {
                case Status.AA:
                    print("Bongos Rusos (AAB)");
                    status = Status.NONE;
                    StopCoroutine(corrutinaGuardada);
                    //corrutinaGuardada = StartCoroutine(ComboCooldown(0.5f));
                    break;
                case Status.AAA:
                    status = Status.AAAB;
                    StopCoroutine(corrutinaGuardada);
                    corrutinaGuardada = StartCoroutine(ComboCooldown(0.5f));
                    break;
                case Status.AAAB:
                    print("Disculpa! (AAABB)");
                    status = Status.NONE;
                    StopCoroutine(corrutinaGuardada);
                    //corrutinaGuardada = StartCoroutine(ComboCooldown(0.5f));
                    break;
                case Status.A:
                    print("agarre (AB)");
                    status = Status.AB;
                    StopCoroutine(corrutinaGuardada);
                    corrutinaGuardada = StartCoroutine(ComboCooldown(0.5f));
                    break;
                case Status.B:
                    print("Gamepad Anal (BB)");
                    status = Status.NONE;
                    StopCoroutine(corrutinaGuardada);
                    //corrutinaGuardada = StartCoroutine(ComboCooldown(0.5f));
                    break;
                default:
                    print("putiaso (B)");
                    status = Status.B;
                    //StopCoroutine(corrutinaGuardada);
                    corrutinaGuardada = StartCoroutine(ComboCooldown(0.5f));
                    break;
            }
        }
   }


    IEnumerator ComboCooldown(float f)
    {
        yield return new WaitForSeconds(f);
        status = Status.NONE;
        print("he acabat la corrutina");
    }
}

public enum Status
{
    NONE,A,AA,AAA,B,AB,AAAB
}
