﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour {

	Rigidbody2D rb;
	Animator anim;
	float vel = 5f;
    float dirX = 0f;
	int  healthPoints = 3;
	bool isHurting, isDead;
	bool facingRight = true;
    bool onGround = true;
	Vector3 localScale;

	// Use this for initialization
	void Start () {
        rb = this.GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator> ();
		localScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {		

		if (Input.GetKeyDown("w") && !isDead && onGround)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,200f));
            onGround = false;
        }
        if (Input.GetKey("d") && !isDead)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                dirX = vel * 2;
            }
            else
            {
                dirX = vel;
            }
        }
        else if (Input.GetKey("a") && !isDead)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                dirX = -vel*2;
            }
            else
            {
                dirX = -vel;
            }
            
        }
        else
        {
            dirX = 0;
        }



        SetAnimationState ();

		if (!isDead)
        {

        }
			
	}

	void FixedUpdate()
	{
		if (!isHurting)
            this.GetComponent<Rigidbody2D>().velocity = new Vector2 (dirX, rb.velocity.y);
	}

	void LateUpdate()
	{
		CheckWhereToFace();
	}

	void SetAnimationState()
	{
		if (dirX == 0) {
			anim.SetBool ("isWalking", false);
			anim.SetBool ("isRunning", false);
		}

		if (rb.velocity.y == 0) {
			anim.SetBool ("isJumping", false);
			anim.SetBool ("isFalling", false);
		}

		if ((dirX == 5 || dirX == -5) && rb.velocity.y == 0)
			anim.SetBool ("isWalking", true);
		
		if ((dirX == 10 || dirX == -10) && rb.velocity.y == 0)
        {
            anim.SetBool("isWalking", false);
            anim.SetBool("isRunning", true);
        }
			
		else
			anim.SetBool ("isRunning", false);

		if (rb.velocity.y > 0) 
            anim.SetBool ("isJumping", true);	
		if (rb.velocity.y < 0) {
			anim.SetBool ("isJumping", false);
			anim.SetBool ("isFalling", true);
		}
	}

	void CheckWhereToFace()
	{
		if (dirX > 0)
			facingRight = true;
		else if (dirX < 0)
			facingRight = false;

		if (((facingRight) && (localScale.x < 0)) || ((!facingRight) && (localScale.x > 0)))
			localScale.x *= -1;

		transform.localScale = localScale;

	}

	void OnTriggerEnter2D (Collider2D col)
	{
        if(col.gameObject.name == "Niebla Chunga" && !isDead)
        {
            dirX = 0;
            isDead = true;
            anim.SetBool("isF",true);
        }
		//anim.Play();
        /*
		if (col.gameObject.name.Equals ("Fire")) {
			healthPoints -= 1;
		}

		if (col.gameObject.name.Equals ("Fire") && healthPoints > 0) {
			anim.SetTrigger ("isHurting");
			StartCoroutine ("Hurt");
		} else {
			dirX = 0;
			isDead = true;
			anim.SetTrigger ("isDead");
		}
        */
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.name == "Suelo")
        {
            onGround = true;
        }
    }

    IEnumerator Hurt()
	{
		isHurting = true;
		rb.velocity = Vector2.zero;

		if (facingRight)
			rb.AddForce (new Vector2(-200f, 200f));
		else
			rb.AddForce (new Vector2(200f, 200f));
		
		yield return new WaitForSeconds (0.5f);

		isHurting = false;
	}

}
