using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowLerp : MonoBehaviour
{
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LerpPositionAmbSlowDown(2));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator LerpPositionAmbSlowDown(float duration)
    {
        float time = 0;
        Vector2 startPosition = transform.position;

        while (true)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime);
            time += Time.deltaTime;
            yield return null;
        }
        transform.position = target.position;
    }
}
