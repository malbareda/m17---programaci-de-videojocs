using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MutliplayerData : ScriptableObject
{
    public string[] nombres;
    public Sprite[] sprites;
    public Transform[] transforms;

}
