using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class CreadorDeRobertos : MonoBehaviour
{
    public MutliplayerData data;
    public int idCounter = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("MP2");
        }
    }

    public void onJoin(PlayerInput pi)
    {
        GameObject player = pi.gameObject;
        player.GetComponent<SpriteRenderer>().sprite = data.sprites[idCounter];
        player.name = data.nombres[idCounter];
        player.transform.position = data.transforms[idCounter].position;
        idCounter++;

        DontDestroyOnLoad(player);
    }
}
