using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputSystemPerObserver : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void Move(InputAction.CallbackContext context)
    {
        print(context.ReadValue<Vector2>());
        this.GetComponent<Rigidbody2D>().velocity = context.ReadValue<Vector2>();
    }

    // Update is called once per frame
    public void Fire(InputAction.CallbackContext context)
    {
        Debug.Log("Fire!");
        print("actioN"+context.action);
        print("control"+context.control);
        print("canceled"+context.canceled);
        print("performed"+context.performed);
    }

    public void Charged(InputAction.CallbackContext context)
    {
        if (context.started) print("charge started");
        if(context.performed)Debug.Log("Charged!");
        if (context.canceled) print("Charge cancelled");
    }

    public void ComboX(InputAction.CallbackContext context)
    {
        if (context.performed) Debug.Log("ComboX!");
    }

    public void TapA(InputAction.CallbackContext context)
    {
        if (context.performed) Debug.Log("TapA!");
    }

    public void Norte(InputAction.CallbackContext context)
    {
        if (context.performed) Debug.Log("Norte");
    }
}
