using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ControllerIS : MonoBehaviour
{
    public float spd;
    private float myTime;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        Gamepad gamepad = Gamepad.current;
        if (gamepad == null)
        {
            print("no gamepad");
            return;
        }
        if (gamepad.buttonNorth.wasPressedThisFrame)
        {
            print("Triangle");
        }
        if (gamepad.buttonSouth.wasPressedThisFrame)
        {
            myTime = Time.time;
        }
        if (gamepad.buttonSouth.wasReleasedThisFrame)
        {
            print("He apretat el boto d'abaix " + (Time.time - myTime) + " ms");
        }
        if (gamepad.buttonWest.isPressed)
        {
            print("estic apretant el boto oest");
        }
        Vector2 move = gamepad.leftStick.ReadValue();
        this.GetComponent<Rigidbody2D>().velocity = move * spd;

        //valor del 0 al 1 dependiendo de lo muy tirado qeu este el joystick en esa direccion
        print(gamepad.rightStick.left.ReadValue());
    }
}
