using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RobertoAction : MonoBehaviour
{
    public InputAction jumpaction;

    public InputActionMap gameplayActions;

    void Awake()
    {
        jumpaction.performed += OnJump;

        gameplayActions["jump"].performed += OnJump;
    }

    void OnEnable()
    {
        jumpaction.Enable();

        gameplayActions.Enable();
    }

    void OnDisable()
    {
        jumpaction.Disable();

        gameplayActions.Disable();
    }


    public void OnJump(InputAction.CallbackContext ctx)
    {
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 100));
    }
}
