using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RPGItem : ScriptableObject
{
    public Sprite sprite;
    public string name;
    public int precioCompra;

}
public enum elemento
{
    FIRE, WATER, THUNDER
}