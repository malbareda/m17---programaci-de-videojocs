using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JoanSingleton : MonoBehaviour
{
    static GameObject joanInstance=null;
    void Awake()
    {
        if(joanInstance == null)
        {
            joanInstance = this.gameObject;
            SceneManager.sceneLoaded += onSceneLoaded;
            DontDestroyOnLoad(joanInstance);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    
    void onSceneLoaded(Scene s, LoadSceneMode lsm)
    {
        if (s.name == "Combat")
        {
            this.GetComponent<JoanOverWorld>().enabled = false;
            this.GetComponent<JoanCombat>().enabled = true;
            this.GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            this.GetComponent<JoanOverWorld>().enabled = true;
            this.GetComponent<JoanCombat>().enabled = false;
            this.GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    // Update is called once per frame
    
}
