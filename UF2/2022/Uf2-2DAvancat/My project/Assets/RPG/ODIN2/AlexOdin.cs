using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AlexOdin : MonoBehaviour
{
    int spd = 3;
    static GameObject instance = null;

    // Start is called before the first frame update
    void Awake()
    {
        if(instance == null)
        {
            instance = this.gameObject;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 move = new Vector2();
        if (Input.GetKey(KeyCode.W))
        {
            move += Vector2.up;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            move -= Vector2.up;
        }
        if (Input.GetKey(KeyCode.A))
        {
            move -= Vector2.right;

        }
        

        else if (Input.GetKey(KeyCode.D))
        {
            move += Vector2.right;
        }


        else if (Input.GetKey(KeyCode.P))
        {
            if (SceneManager.GetActiveScene().name == "SaveOdin") SceneManager.LoadScene("SaveOdin2");
            else SceneManager.LoadScene("SaveOdin");
        }

        this.GetComponent<Rigidbody2D>().velocity = move.normalized * spd;
    }
}
