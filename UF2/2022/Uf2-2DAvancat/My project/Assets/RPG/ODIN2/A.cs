using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class A : ScriptableObject
{
    public int a;
    public B b;
    public Color c;
    public Sprite sprite;
    public Vector2 pos;

}
