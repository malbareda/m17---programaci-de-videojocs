using OdinSerializer;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OdinSave : MonoBehaviour
{
    public A data;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            data.b.c.c = SceneManager.GetActiveScene().name;
            data.pos = GameObject.Find("Alex").transform.position;
            //A data;
            print("aaa" + data.b.c.c);

            byte[] serializedData = SerializationUtility.SerializeValue<A>(data, DataFormat.JSON);

            string base64 = System.Convert.ToBase64String(serializedData);
            //File.WriteAllBytes("testOdin.json", serializedData);
            File.WriteAllText("testOdin.json", base64);



            //A clone = Instantiate(newData);
        }

        else if (Input.GetKeyDown(KeyCode.L)){
            string newBase64 = File.ReadAllText("testOdin.json");
            byte[] postFile = System.Convert.FromBase64String(newBase64);
            //byte[] postFile = File.ReadAllBytes("testOdin.json");


            print(postFile);


            A newData = SerializationUtility.DeserializeValue<A>(postFile, DataFormat.JSON);


            SceneManager.LoadScene(newData.b.c.c);
            GameObject.Find("Alex").transform.position = newData.pos;



            print(newData.b.c.c);
        }
    }
}
