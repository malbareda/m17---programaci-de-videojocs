using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{

    public GameEventItem drop;

    [Serializable]
    public struct Loot
    {
        public RPGItem item;
        public float prob;
    }

    public Loot[] lootTable;

    // Start is called before the first frame update
    private void OnMouseDown()
    {
        matar();
    }

    public void matar()
    {
        foreach(Loot l in lootTable)
        {
            float r = UnityEngine.Random.Range(0, 1f);
            if (r < l.prob)
            {
                print("obtenido " + l.item.name);
                drop.Raise(l.item);
            }
        }
    }
}
