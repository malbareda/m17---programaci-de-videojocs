using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Progress;

public class StackInventaryController : MonoBehaviour
{
    public const int MAX_AMMOUNT = 64;
    public const int MAX_INV = 10;
    public struct stack
    {
        public RPGItem tipo;
        public int ammount;

        public override String ToString()
        {
            return tipo.name+", "+ammount;
        }
    }

    public List<stack> stackInventory = new List<stack>();


    public void addItem(RPGItem i)
    {
        bool flag = false;
        for(int j = 0; j < stackInventory.Count; j++)
        {
            if (stackInventory[j].tipo.name == i.name)
            {
                if (stackInventory[j].ammount < MAX_AMMOUNT)
                {
                    stack a;
                    a.tipo = i;
                    a.ammount = stackInventory[j].ammount+1;
                    stackInventory[j] = a;
                    flag = true;
                }
            }
        }
        if (!flag)
        {
            if (stackInventory.Count < MAX_INV)
            {
                stack a;
                a.tipo = i;
                a.ammount = 1;
                stackInventory.Add(a);
                flag = true;
            }
        }

        print(string.Join(",", stackInventory));
    }
}
