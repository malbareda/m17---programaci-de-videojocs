using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class QuestGiverData : ScriptableObject
{
    public int questid;
    public string textAccepted;
    public string textCompleted;
    public string textDelivered;
}
