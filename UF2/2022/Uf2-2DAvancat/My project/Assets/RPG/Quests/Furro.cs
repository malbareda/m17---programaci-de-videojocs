using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Furro : MonoBehaviour
{
    public GameEventItem itemAcq;
    public RPGItem item;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        itemAcq.Raise(this.item);
        Destroy(this.gameObject);
    }
}
