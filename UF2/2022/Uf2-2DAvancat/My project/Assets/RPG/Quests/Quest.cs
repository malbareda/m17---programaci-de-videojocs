using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest: ScriptableObject 
{
    public int idQuest;
    public statusQuest statusQuest;
    public string Questname;
    public string desc;
}

public enum statusQuest { NONE, ACCEPTED, COMPLETED, DELIVERED }
