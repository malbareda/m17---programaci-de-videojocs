using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class QuestText : MonoBehaviour
{
    public void display(string v)
    {
        StartCoroutine(showtext(v));
    }

    public IEnumerator showtext(string v)
    {
        TextMeshProUGUI text = this.GetComponent<TMPro.TextMeshProUGUI>();
        text.color = Color.yellow;
        text.text = v;
        while (text.color.a>0)
        {
            yield return new WaitForSeconds(0.2f);
            text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a-0.1f);   
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
