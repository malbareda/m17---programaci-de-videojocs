using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    private QuestLog quests;
    public QuestText QuestText;
    private Dictionary<RPGItem,CollectQuest> trackedItems = new Dictionary<RPGItem, CollectQuest>();
    public void tryGiveQuest(int questId)
    {
        Quest q;
        quests.questLog.TryGetValue(questId, out q);
        if (q.statusQuest == statusQuest.NONE)
        {
            print("Quest acceptada: " + q.name);
            q.statusQuest = statusQuest.ACCEPTED;
            QuestText.display("Quest acceptada: " + q.name);

            if(q is CollectQuest cq)
            {
                //track item
                trackedItems.Add(cq.item,cq);
            }
        }
    }


    public void itemAcquired(RPGItem item)
    {
        CollectQuest qid;
        if (trackedItems.TryGetValue(item, out qid))
        {
            qid.numberCurrent++;
            QuestText.display(qid.desc+" " + qid.numberCurrent+"/"+qid.numberNeeded);
            if (qid.numberCurrent == qid.numberNeeded)
            {
                qid.statusQuest = statusQuest.COMPLETED;
                trackedItems.Remove(item);
            }

        }
    }
    // Start is called before the first frame update
    void Awake()
    {
        quests = Resources.Load<QuestLog>("quests/QuestLog");
        quests.createDictionary();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
