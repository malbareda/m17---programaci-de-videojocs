using System.Collections;
using System.Collections.Generic;
using System.Drawing.Printing;
using UnityEngine;

[CreateAssetMenu]
public class QuestLog : ScriptableObject
{
   public Dictionary<int, Quest> questLog = new Dictionary<int, Quest>();
   public List<Quest> quests = new List<Quest>();

    public void createDictionary()
    {
        foreach(Quest quest in quests)
        {
            questLog.Add(quest.idQuest, quest);
        }
        Debug.Log("quest dictionary " + questLog);
    }
}
