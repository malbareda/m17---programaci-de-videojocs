using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGiverSergi : MonoBehaviour
{
    public QuestGiverData data;
    private Quest myQuest;
    // Start is called before the first frame update
    void Start()
    {
        myQuest = Resources.Load<QuestLog>("quests/QuestLog").questLog.GetValueOrDefault(data.questid);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if(myQuest.statusQuest == statusQuest.NONE)
        {
            GameObject.Find("QuestManager").GetComponent<QuestManager>().tryGiveQuest(data.questid);
        }
        else if (myQuest.statusQuest == statusQuest.ACCEPTED)
        {
            print(data.textAccepted);
        }
        else if (myQuest.statusQuest == statusQuest.COMPLETED)
        {
            print(data.textCompleted);
            myQuest.statusQuest = statusQuest.DELIVERED;
            //aqui dariamos las recompensas
        }else if(myQuest.statusQuest == statusQuest.DELIVERED)
        {
            print(data.textDelivered);
        }



    }
}
