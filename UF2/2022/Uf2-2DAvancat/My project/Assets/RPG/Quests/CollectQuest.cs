using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CollectQuest : Quest
{
    public RPGItem item;
    public int numberCurrent;
    public int numberNeeded;

}
