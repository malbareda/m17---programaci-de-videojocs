using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cosesDeLerp : MonoBehaviour
{
    public Transform positionToMoveTo;

    void Start()
    {
        //StartCoroutine(LerpPosition(positionToMoveTo, 2));
        //StartCoroutine(LerpPositionAmbSlowDown(positionToMoveTo, 5));
        StartCoroutine(LerpRotation(Quaternion.Euler(new Vector3(0,0,180)), 5));
        StartCoroutine(LerpColor(Color.blue, 5));

    }

    IEnumerator LerpPosition(Transform target, float duration)
    {
        float time = 0;
        Vector2 startPosition = transform.position;

        while (time < duration)
        {
            transform.position = Vector2.Lerp(startPosition, target.position, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        transform.position = target.position;
    }



    IEnumerator LerpPositionAmbSlowDown(Transform target, float duration)
    {
        float time = 0;
        Vector2 startPosition = transform.position;

        while (time < duration)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime);
            time += Time.deltaTime;
            yield return null;
        }
        transform.position = target.position;
    }

    IEnumerator LerpRotation(Quaternion endValue, float duration)
    {
        float time = 0;
        Quaternion startValue = transform.rotation;

        while (time < duration)
        {
            transform.rotation = Quaternion.Lerp(startValue, endValue, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        transform.rotation = endValue;
    }


    IEnumerator LerpColor(Color endValue, float duration)
    {
        
        float time = 0;
        Color startValue = this.GetComponent<SpriteRenderer>().color;

        while (time < duration)
        {
            this.GetComponent<SpriteRenderer>().color = Color.Lerp(startValue, endValue, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        this.GetComponent<SpriteRenderer>().color = endValue;
    }
}
