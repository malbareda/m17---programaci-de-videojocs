using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotLinkBehaviour : MonoBehaviour
{
    [SerializeField]
    private GameObject fletxa;

    public void Shoot()
    {
        GameObject instanciacio = Instantiate(fletxa);
        instanciacio.transform.position = transform.position;

        instanciacio.GetComponent<Rigidbody2D>().velocity = transform.right * -1*10;
        instanciacio.GetComponent<SpriteRenderer>().flipX = true;
        Destroy(instanciacio, 5);
    }
}
