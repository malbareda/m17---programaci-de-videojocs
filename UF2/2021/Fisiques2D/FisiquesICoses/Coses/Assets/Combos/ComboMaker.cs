using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboMaker : MonoBehaviour
{

    public float tempsMin=0.1f;
    public float tempsMax=1f;
    private int maxCharge = 10;
    private string state="-";
    bool tooSoon = false;

    /// <summary>
    /// -
    /// A
    /// A->
    /// AB
    /// AA
    /// </summary>


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            StartCoroutine(comboNewInput("A"));
        }else if (Input.GetKeyDown(KeyCode.K))
        {
            StartCoroutine(comboNewInput("B"));
        }
    }


    IEnumerator comboNewInput(string input)
    {
        StopCoroutine("cargaAtaque");
        if (!tooSoon)
        {
            switch (state)
            {
                case "-":
                    if (input == "A")
                    {
                        //haria cosas
                        print("hace A");

                        state = "A";
                    }
                    else if (input == "B")
                    {
                        //haria cosas
                        print("hace B");
                        state = "-";
                    }
                    break;
                case "A":
                    if (input == "A")
                    {
                        //haria cosas
                        print("hace AA");
                        state = "AA";
                    }
                    else if (input == "B")
                    {
                        //haria cosas
                        print("hace AB");
                        state = "AB";
                    }
                    break;
                case "AA":
                    if (input == "A")
                    {
                        //haria cosas
                        print("hace AAA");
                        state = "-";
                    }
                    else if (input == "B")
                    {
                        //haria cosas
                        print("hace AAB");
                        state = "-";
                    }
                    break;
                case "AB":
                    if (input == "A")
                    {
                        //haria cosas
                        print("hace A");
                        state = "A";
                    }
                    else if (input == "B")
                    {
                        //haria cosas
                        print("hace ABB");
                        state = "-";
                    }
                    break;
            }
        }
        else
        {
            //no fa res, podriem penalitzar posant el state a "-"
            print("too Soon");
        }
        


        string copystate = state;
        tooSoon = true;
        yield return new WaitForSeconds(tempsMin);
        tooSoon = false;
        if (state == "A" && Input.GetKey(KeyCode.J))
        {
            StartCoroutine(cargaAtaque(state, KeyCode.J));
        }
        yield return new WaitForSeconds(tempsMax-tempsMin);
        if (state == copystate && state!="-")
        {
            print("cooldown exhaurit a "+state);
            state = "-";
        }
        
    }


    IEnumerator cargaAtaque(string chargingState, KeyCode kc)
    {
        StopCoroutine("comboNewInput");
        state = chargingState+"->";
        print("cargant atac " + chargingState + "...");
        int cargaAtac = 0;
        while (Input.GetKey(kc))
        {
            if(cargaAtac<maxCharge)cargaAtac++;
            yield return new WaitForSeconds(0.1f);
        }

        //fer coses
        print("he disparat un atac " + chargingState + " amb potencia " + cargaAtac);
        state = "-";
        
    }
}
