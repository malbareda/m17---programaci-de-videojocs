using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimentJoan : MonoBehaviour
{
    public int spd;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-1 * spd, rb.velocity.y);
        }else if (Input.GetKey("d"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(1 * spd, rb.velocity.y);
        }

        if (Input.GetKeyDown("w"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));
        }
    }
}
