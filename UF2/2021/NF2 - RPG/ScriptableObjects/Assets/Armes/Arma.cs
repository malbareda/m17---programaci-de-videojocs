﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//aixo fa que puguis crear nous scriptable objects desde el menu contextual de Create
[CreateAssetMenu]
public class Arma : ScriptableObject
{
    public int spd;
    public int bales;
    public float rechargeTime;
    public float scale;
    public Color color;
    public int balesEnUnSoloDisparo;
    public float dispersionAngle;
    public float gravity;

}
