using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApretarF : MonoBehaviour
{
    public GameEvent f;
    public GameEvent g;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            f.Raise();
        }else if (Input.GetKeyDown(KeyCode.G))
        {
            g.Raise();
        }
    }
}
