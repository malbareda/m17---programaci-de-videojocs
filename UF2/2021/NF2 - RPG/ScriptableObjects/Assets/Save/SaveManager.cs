﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public GameObject[] coses;
    public Layout layout;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("s"))
        {
            layout.posicions = new Vector2[coses.Length];
            layout.colors = new Color[coses.Length];
            layout.tamany = new Vector2[coses.Length];
            for (int i =0;i<coses.Length;i++)
            {
                layout.posicions[i] = coses[i].transform.position;
                layout.colors[i] = coses[i].GetComponent<SpriteRenderer>().color;
                layout.tamany[i] = coses[i].transform.localScale;
            }
            string jsonStr = JsonUtility.ToJson(layout);
            //si vols guardarho en base64. base64 es un format de compressió per a que els jugadors no puguin canviar el savegame a ma, i per a que ocupi menys el fitxer de save
            string base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(jsonStr));
            File.WriteAllText("testlayout.json", base64);
            print("Todo ha salido a pedir de Milhouse");
        }
        if (Input.GetKeyDown("l"))
        {
            string base64 = File.ReadAllText("testlayout.json");
            //si vols guardarho en base64. base64 es un format de compressió per a que els jugadors no puguin canviar el savegame a ma, i per a que ocupi menys el fitxer de save
            string jsonStr= Encoding.UTF8.GetString(Convert.FromBase64String(base64));
            JsonUtility.FromJsonOverwrite(jsonStr, layout);
            for (int i = 0; i < coses.Length; i++)
            {
                coses[i].transform.position = layout.posicions[i];
                coses[i].GetComponent<SpriteRenderer>().color = layout.colors[i];
                coses[i].transform.localScale = layout.tamany[i];
            }
            print("He cargado de puta madre");
        }
        
    }
}
