﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Layout : ScriptableObject
{
    public Vector2[] posicions;
    public Color[] colors;
    public Vector2[] tamany;
}
