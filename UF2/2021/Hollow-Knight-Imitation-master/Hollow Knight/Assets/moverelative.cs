using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moverelative : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = this.transform.right * 10;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
