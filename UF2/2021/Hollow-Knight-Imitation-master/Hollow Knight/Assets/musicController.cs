using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicController : MonoBehaviour
{
    float maxDistance = 200f;
    AudioSource audio;
    float pitch = 1;
    bool dir = true;
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(ChangePitch());
    }

    // Update is called once per frame
    void Update()
    {
        //float magnitud = Mathf.Sqrt(Mathf.Pow(this.transform.position.x, 2) + Mathf.Pow(this.transform.position.y, 2) + Mathf.Pow(this.transform.position.z, 2));

        //float magnituden0y1 = Mathf.Clamp(magnitud, 0, maxDistance) / maxDistance;
        //this.GetComponent<AudioSource>().pitch = 1 - magnituden0y1;



        if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(-5, 0, 0);
        }
        else if (Input.GetKey("d")) {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(5, 0, 0);
        }
        if (Input.GetKeyDown("j"))
        {
            this.GetComponent<AudioSource>().Play();
        }else if (Input.GetKeyDown("k"))
        {
            this.GetComponent<AudioSource>().Stop();
        }

        if (Input.GetKeyDown("w"))
        {
            this.GetComponent<AudioSource>().pitch += 0.1f;
        }else if (Input.GetKeyDown("s"))
        {
            this.GetComponent<AudioSource>().pitch -= 0.1f;
        }

    }

    IEnumerator ChangePitch()
    {
        while (true)
        {


            if (dir) pitch += 0.01f;
            else pitch -= 0.01f;
            yield return new WaitForSeconds(0.1f);
            this.GetComponent<AudioSource>().pitch = pitch;
            if (pitch > 1.5) dir = false;
            if (pitch < 0) dir = true;
        }
    }
}
