﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class game1 : MonoBehaviour
{

    public GameObject infectat;
    // Start is called before the first frame update
    void Start()
    {
        GameObject newInf = Instantiate(infectat);
        //la forma de subscriure't a un event es sumant. Es suma una funció del mateix argument i retorn que el delegat.
        newInf.GetComponent<infectat1>().InfectarEvent += contagi;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void contagi()
    {
        GameObject newInf = Instantiate(infectat);
        int x = Random.Range(-10, 10);
        int y = Random.Range(-5, 5);
        newInf.transform.position = new Vector2(x, y);
        newInf.GetComponent<infectat1>().InfectarEvent += contagi;
    }
}
