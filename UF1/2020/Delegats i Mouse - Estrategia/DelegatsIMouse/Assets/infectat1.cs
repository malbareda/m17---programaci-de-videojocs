﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class infectat1 : MonoBehaviour
{
    //un delegat es un punter a una funció. És una variable que està representant una funció
    public delegate void Infectar();
    //l'event fa referència a una funció, indicada per el delegat.
    public event Infectar InfectarEvent;

    /*
     * Events.
     * 
     * Els events funcionen com un sistema de notificacions.
     * Tu tens subscripcions a l'event. Et subscrius al seu "sistema de notificacions intern"
     * Quan l'event salta mitjançant Invoke, aleshores tot alló subscrit a l'event rep una notificació
     * 
     * */

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("inf", 3f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void inf()
    {
        //voldriem que saltes un event que notifiques al gameController que ha de crear un nou infectat.

        //si l'event no es buit
        if (InfectarEvent != null)
        {
            //invocar l'event. Per tant estem invocant la funció delegada infectar.
            InfectarEvent.Invoke();
        }

    }
}
