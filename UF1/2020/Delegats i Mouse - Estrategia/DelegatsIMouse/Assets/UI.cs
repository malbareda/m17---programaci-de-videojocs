﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    //un delegat es un punter a una funció. És una variable que està representant una funció
    public delegate void GameOver();
    //l'event fa referència a una funció, indicada per el delegat.
    public event GameOver GameOverEvent;

    int nInfectats = 0;


    GameObject gameCont;
    // Start is called before the first frame update
    void Start()
    {
        gameCont = GameObject.Find("GameController");
        gameCont.GetComponent<game2>().ActualitzarUIEvent += actualitzarUI;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void actualitzarUI()
    {
        nInfectats++;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Infectats: " + nInfectats;
        if (nInfectats > 15)
        {
            if (GameOverEvent != null)
            {
                GameOverEvent.Invoke();
            }
        }
    }

    public int restarInfectado()
    {
        nInfectats--;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Infectats: " + nInfectats;
        return nInfectats;
    }


}
