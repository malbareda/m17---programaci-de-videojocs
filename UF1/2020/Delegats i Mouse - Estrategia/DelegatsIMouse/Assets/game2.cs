﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class game2 : MonoBehaviour
{

    public GameObject casa;
    //un delegat es un punter a una funció. És una variable que està representant una funció
    public delegate void ActualitzarUI();
    //l'event fa referència a una funció, indicada per el delegat.
    public event ActualitzarUI ActualitzarUIEvent;
    

    public GameObject infectat;
    GameObject textUI;
    // Start is called before the first frame update
    void Start()
    {
        textUI = GameObject.Find("TextUI");
        textUI.GetComponent<UI>().GameOverEvent += gameOver;
        contagi(this.transform);
        InvokeRepeating("spawnCasa",0f,5f);
    }

    private void gameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void contagi(Transform t)
    {
        GameObject newInf = Instantiate(infectat);
        float x = UnityEngine.Random.Range(-2f, 2f);
        float y = UnityEngine.Random.Range(-2f, 2f);
        //la forma de subscriure't a un event es sumant. Es suma una funció del mateix argument i retorn que el delegat.
        //cada element serà creat aprop de l'element que el crea
        newInf.transform.position = new Vector2(t.position.x+x, t.position.y+y);
        newInf.GetComponent<infectat2>().InfectarEvent += contagi;

        if (ActualitzarUIEvent != null)
        {
            ActualitzarUIEvent.Invoke();
        }
    }

    void spawnCasa()
    {
        GameObject newCasa = Instantiate(casa);
        newCasa.transform.position = new Vector2(-7f, -1f);
        //puedes suscribir a un evento una funcion que ni siquiera es tuya.
        newCasa.GetComponent<pacasa>().RestarInfectadoEvent += textUI.GetComponent<UI>().restarInfectado;
    }
}
