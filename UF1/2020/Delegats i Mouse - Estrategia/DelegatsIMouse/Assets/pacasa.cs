﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pacasa : MonoBehaviour
{

    //un delegat es un punter a una funció. És una variable que està representant una funció
    public delegate int RestarInfectado();
    //l'event fa referència a una funció, indicada per el delegat.
    public event RestarInfectado RestarInfectadoEvent;


    private bool mouseup = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDrag()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.transform.position = new Vector2(pos.x,pos.y);
    }

    private void OnMouseUp()
    {
        mouseup = true;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (mouseup)
        {
            print("POPOM PA TU CASA ");
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            if (RestarInfectadoEvent != null)
            {
                int a = RestarInfectadoEvent.Invoke();
                print("queden " + a + " infectats");
            }
        }
    }
}
