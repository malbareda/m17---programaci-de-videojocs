using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfectatCovid : MonoBehaviour
{
    bool parar = false;
    public TipusInfectat tipusInfectat;
    private int hp;
    private bool invFr=false;
    // Start is called before the first frame update
    void Start()
    {
        this.hp = tipusInfectat.hp;
        this.transform.localScale *= tipusInfectat.scale;
        //this.GetComponent<SpriteRenderer>().color = tipusInfectat.color;
        StartCoroutine("Disparar");
        if(tipusInfectat.velAngular!=0)StartCoroutine("Girar");
        if (tipusInfectat.velVertical != 0) StartCoroutine("Vertical");
        StartCoroutine("Blau");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator Vertical()
    {
        while (true)
        {
            this.GetComponent<Rigidbody2D>().velocity=new Vector2(0, -tipusInfectat.velVertical);
            yield return new WaitForSeconds(3f);
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, tipusInfectat.velVertical);
            yield return new WaitForSeconds(3f);

        }
    }
    IEnumerator Blau()
    {
        while (true)
        {
            for (int i = 0; i < 20; i++)
            {
                Color c = this.GetComponent<SpriteRenderer>().color;
                c.g -= 0.05f;
                this.GetComponent<SpriteRenderer>().color = c;
                yield return new WaitForSeconds(0.1f);
            }
            for (int i = 0; i < 20; i++)
            {
                Color c = this.GetComponent<SpriteRenderer>().color;
                c.g += 0.05f;
                this.GetComponent<SpriteRenderer>().color = c;
                yield return new WaitForSeconds(0.1f);
            }
        }
    }


    IEnumerator Disparar()
    {
        while (!parar)
        {
            GameObject newCovid = Instantiate(tipusInfectat.covid);
            newCovid.transform.position = this.transform.position;
            newCovid.GetComponent<Rigidbody2D>().velocity = -this.transform.right * 2;
            yield return new WaitForSeconds(this.tipusInfectat.fireRate);
        }
        
    }

    IEnumerator Girar()
    {
        while (!parar)
        {
            this.transform.Rotate(new Vector3(0, 0, 0.5f*tipusInfectat.velAngular));
            yield return new WaitForSeconds(.01f);

        }
    }

    private void OnMouseDown()
    {
        if (!invFr)
        {
            hp--;
            invFr = true;
            StartCoroutine("Invulnerability");
            if (hp == 0)
            {
                parar = true;
            }
        }
        
    }

    IEnumerator Invulnerability()
    {
        yield return new WaitForSeconds(1f);
        invFr = false;
    }
}
