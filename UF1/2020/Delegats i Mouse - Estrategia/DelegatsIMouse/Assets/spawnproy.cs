using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnproy : MonoBehaviour
{
    public Vector3 spd;
    public GameObject proy;
    // Start is called before the first frame update
    void Start()
    {

        StartCoroutine(a());
    }

    // Update is called once per frame
    void Update()
    {
    }
    IEnumerator a()
    {
        while (true)
        {
            GameObject newProy = Instantiate(proy);
            newProy.transform.position = this.transform.position;
            newProy.GetComponent<Rigidbody2D>().velocity = spd;
            yield return new WaitForSeconds(0.5f);
        }
    }
}
