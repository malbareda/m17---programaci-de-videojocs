using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComoRotariamosUnCuadrado : MonoBehaviour
{
    public int spd;
    // Start is called before the first frame update
    void Start()
    {
        //elegantes
        this.transform.eulerAngles = new Vector3(0,0,45);
        this.transform.Rotate(0, 0, 20);


        ///componentes direccionales
        //this.transform.up; VERDE
        //this.transform.right;  ROJO
        //en 2d no sirve this.transform.forward;  AZUL
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(0, 0, 1);
        this.GetComponent<Rigidbody2D>().velocity = -this.transform.up * spd;

    }
}
