﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemigo : MonoBehaviour
{

    public GameObject disparo;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("salta", 0f, 2f);
        InvokeRepeating("dispara", 0f, 1.5f);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "pj")
        {
            Destroy(collision.gameObject);
        }
    }

    private void salta()
    {
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300));
    }

    private void dispara()
    {
        GameObject newDisparo = Instantiate(disparo);
        newDisparo.transform.position = this.transform.position;
        newDisparo.GetComponent<Rigidbody2D>().velocity = new Vector2(-4, newDisparo.GetComponent<Rigidbody2D>().velocity.y);

    }
}
