﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento : MonoBehaviour
{
    public int speed = 2;
    private bool salto = true;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("vacuna", 10f);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, this.GetComponent<Rigidbody2D>().velocity.y);
        }else if (Input.GetKey("d"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        if (Input.GetKeyDown("w") && salto)
        {
            salto = false;
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));

        }
    }

    //salta automaticament al colisionar. et passa un objecte Collision2D que detalla la colisio
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "suelo")
        {
            salto = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "sitemirotemato")
        {
            Destroy(this.gameObject);
        }
    }

    private void vacuna()
    {
        print("La vacuna destruye el virus");
        print("Viva");
        //no feu servir massa sovint aquesta funcio que menja molt.
        Destroy(GameObject.Find("Covid"));
    }


}
