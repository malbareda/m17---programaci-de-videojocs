﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorDePlataformes : MonoBehaviour
{
    public GameObject plataforma;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("generar", 0f, 2f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void generar()
    {
        GameObject newPlat = Instantiate(plataforma);
        float random = Random.Range(-3.5f, 3.5f);
        newPlat.transform.position = new Vector2(13, random);
        if (Random.Range(0f, 1f) < 0.5f)
        {
            newPlat.GetComponent<SpriteRenderer>().color = Color.green;
            newPlat.GetComponent<PlataformaMovil>().mala = true;
        }
    }
}
