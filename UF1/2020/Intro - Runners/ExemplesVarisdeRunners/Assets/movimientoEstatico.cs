﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class movimientoEstatico : MonoBehaviour
{
    public int speed = 2;
    private bool salto = true;
    private int p = 0;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("score", 1f, 1f);
       

    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x < -10)
        {
            Destroy(this.gameObject);
        }
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        if (Input.GetKeyDown("w") && salto)
        {
            salto = false;
            this.GetComponent<Rigidbody2D>().gravityScale *= -1;

        }
    }

    //salta automaticament al colisionar. et passa un objecte Collision2D que detalla la colisio
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "suelo")
        {
            salto = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "sitemirotemato")
        {
            Destroy(this.gameObject);
        }
    }

    private void score()
    {
        p++;
        GameObject score = GameObject.Find("Score");
        score.GetComponent<TMPro.TextMeshProUGUI>().text = "Score: " + p;
        GameObject imatge = GameObject.Find("Image");
        float vermell = p * 0.01f;
        imatge.GetComponent<Image>().color = new Color(vermell, 0f, 0f);
    }



}
