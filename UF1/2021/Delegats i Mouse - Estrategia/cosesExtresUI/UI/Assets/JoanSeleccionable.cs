using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoanSeleccionable : MonoBehaviour
{
    private SpriteRenderer sprRenderer;

    private bool joanSelected;

    public static bool dragJoans, mouseoverJoan;

    private Vector2 mousePos;

    private float dragOffsetX, dragOffsetY;

    // Start is called before the first frame update
    void Start()
    {
        sprRenderer = GetComponent<SpriteRenderer>();
        joanSelected = false;
        dragJoans = false;
        mouseoverJoan = false;
    }

    // When BoxSelections collider meets a soldier, soldier changes its color tint to Red
    // and soldier is marked as selected now

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Seleccionador>())
        {
            sprRenderer.color = new Color(1f, 0f, 0f, 1f);
            joanSelected = true;
        }
    }

    // When BoxSelection collider stops touching a soldier while left mouse button is still
    // being held down then soldeir gets its normal color tint and marked as not selected

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Seleccionador>() && Input.GetMouseButton(0))
        {
            sprRenderer.color = new Color(1f, 1f, 1f, 1f);
            joanSelected = false;
        }
    }

    private void Update()
    {
        // When left mouse button is clicked I need to get an offset between mouse position
        // and a soldier. This offset will help me to drag soldier/soldiers from
        // its/their initial positions depending on mouse position whitout any "jumping" issues

        if (Input.GetMouseButtonDown(0))
        {
            dragOffsetX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
            dragOffsetY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;
        }

        // And ofcourse I need to get mouse position

        if (Input.GetMouseButton(0))
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        //Pretty obvious piece of code I guess :-) If it's not just let me know.

        if (joanSelected && dragJoans)
        {
            transform.position = new Vector2(mousePos.x - dragOffsetX, mousePos.y - dragOffsetY);
        }

        // If right mouse button is pressed then selection is reset

        if (Input.GetMouseButtonDown(1))
        {
            joanSelected = false;
            dragJoans = false;
            sprRenderer.color = new Color(1f, 1f, 1f, 1f);
        }
    }

    // No need to explain I hope

    private void OnMouseDown()
    {
        mouseoverJoan = true;
    }

    // Same here

    private void OnMouseUp()
    {
        mouseoverJoan = false;
        dragJoans = false;
    }

    // Hope it's clear as well

    private void OnMouseDrag()
    {
        dragJoans = true;

        if (!joanSelected)
        {
            dragJoans = false;
        }

        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector2(mousePos.x - dragOffsetX, mousePos.y - dragOffsetY);
    }
}
