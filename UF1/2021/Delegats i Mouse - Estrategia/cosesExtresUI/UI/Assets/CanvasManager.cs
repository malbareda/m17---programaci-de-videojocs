﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ButtonPressed()
    {
        DadesEstatiques.boto++;
        print("He apretat el boto "+DadesEstatiques.boto+" vegades");
    }

    public void SliderSlided(float value)
    {
        print(value);
    }

    public void TextTexted(string text)
    {
        print("He escrit un text " + text);
        PlayerPrefs.SetString("TextEscena1", text);
        PlayerPrefs.Save();
    }

    public void OnDropdownDropdowned(int a)
    {
        print(a);
        if (a == 0)
        {
            print("cane canoso");
        }else if (a == 1)
        {
            print("Ruben urbano me la agarras con la mano");
        }
        else
        {
            print("ruby y pau debajo de un arbol");
        }
    }

    public void CanviEscena()
    {
        SceneManager.LoadScene("Altra");
    }


}
