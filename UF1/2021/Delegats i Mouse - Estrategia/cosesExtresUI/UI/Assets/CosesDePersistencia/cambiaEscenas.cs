using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class cambiaEscenas : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        print("he carregat " + SceneManager.GetActiveScene().name);   
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            if(SceneManager.GetActiveScene().name== "PersistenciaEscenesA")
            SceneManager.LoadScene("AltraEscena");
            else SceneManager.LoadScene("PersistenciaEscenesA");
        }
    }
}
