using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="data",menuName ="ScriptableObjects/persistente")]
public class ScritableObjectPersistente : ScriptableObject
{
    public int variableA;
    public string variableB;
}
