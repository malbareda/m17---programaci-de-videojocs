﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class infectao : MonoBehaviour
{

    public delegate void Infectar();
    public event Infectar InfectarEvent;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("inf", 3f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void inf()
    {
        if(InfectarEvent != null)
        {
            InfectarEvent.Invoke();
        }
    }
}
