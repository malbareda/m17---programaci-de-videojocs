﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameController : MonoBehaviour
{
    public GameObject infectao;
    // Start is called before the first frame update
    void Start()
    {
        GameObject newInf = Instantiate(infectao);
        newInf.GetComponent<infectao>().InfectarEvent += contagi;
    }

    private void contagi()
    {
        GameObject newInf = Instantiate(infectao);
        float x = UnityEngine.Random.Range(-10f, 10f);
        float y = UnityEngine.Random.Range(-5f, 5f);
        newInf.transform.position = new Vector2(x, y);
        newInf.GetComponent<infectao>().InfectarEvent += contagi;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
