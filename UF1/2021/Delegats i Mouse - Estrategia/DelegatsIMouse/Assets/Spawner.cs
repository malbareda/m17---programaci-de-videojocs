using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject infectat;
    public List<TipusInfectat> llista;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Spawn");

    }

    // Update is called once per frame
    void Update()
    {

    }


    IEnumerator Spawn()
    {
        while (true)
        {
            TipusInfectat ti = llista[Random.Range(0, llista.Count)];
            GameObject newCovid = Instantiate(infectat);
            newCovid.GetComponent<InfectatCovid>().tipusInfectat = ti;
            newCovid.transform.position = new Vector2(Random.Range(-10f,10f),Random.Range(-5f,5f));
            yield return new WaitForSeconds(3f);
        }

    }
}
