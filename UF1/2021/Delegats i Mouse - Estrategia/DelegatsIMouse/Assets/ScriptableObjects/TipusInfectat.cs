using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Data", menuName ="ScriptableObjects/TipusInfectat",order =1)]
public class TipusInfectat : ScriptableObject
{
    public float scale;
    public Color color;
    public float fireRate;
    public int hp;
    public GameObject covid;
    public float velAngular;
    public float velVertical;


}
