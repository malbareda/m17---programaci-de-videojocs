using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveleftmountain : MonoBehaviour
{
    public float spd;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(-spd, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(-spd, 0, 0);
        if (this.transform.position.x < -13)
        {
            this.transform.position = new Vector3(13, 0, 0);
        }
    }
}
