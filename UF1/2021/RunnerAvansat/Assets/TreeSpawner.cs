using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Spawn", 0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Spawn()
    {
        float randL = Random.Range(0f, 2f);
        GameObject tree = ObjectPool.SharedInstance.GetPooledObject();
        if (tree != null)
        {
            tree.transform.position = this.transform.position;
            tree.transform.rotation = this.transform.rotation;
            tree.SetActive(true);
        }
        Invoke("Spawn", randL);
    }
}
