using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cuadrat : MonoBehaviour
{
    //comencem creant el delegat
    public delegate void FerAlgo();
    public event FerAlgo OnFerAlgo;

    //els delegats poden tenir arguments
    public delegate void FerArguments(int a);
    public event FerArguments OnFerArguments;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("a"))
        {
            if (OnFerAlgo != null)
            {
                OnFerAlgo.Invoke();
            }
        }
        if (Input.GetKeyDown("s"))
        {
            if (OnFerArguments != null)
            {
                OnFerArguments.Invoke(4);
            }
        }
    }
}
