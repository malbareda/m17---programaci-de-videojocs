using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Capsula : MonoBehaviour
{
    public Cuadrat cuadrat;
    void Start()
    {
        //subscriures. Ens subscribim al delegat amb una funcio
        cuadrat.OnFerAlgo += ActivarCapsula;
    }

    private void ActivarCapsula()
    {
        print("Capsula s'ha activat");
    }

    // Update is called once per frame
    void Update()
    {

    }

    //aquesta funcio salta automaticament a l'esborrar l'objecte o desactivarlo
    private void OnDisable()
    {
        //ens dessuscribim
        cuadrat.OnFerAlgo -= ActivarCapsula;
    }
}
