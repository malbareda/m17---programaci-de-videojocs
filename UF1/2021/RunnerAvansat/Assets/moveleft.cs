using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveleft : MonoBehaviour
{
    public float spd;
    public List<GameObject> pool;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(-spd, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(-spd, 0, 0);
        if (this.transform.position.x < -13)
        {
            this.gameObject.SetActive(false);
        }
    }
}
