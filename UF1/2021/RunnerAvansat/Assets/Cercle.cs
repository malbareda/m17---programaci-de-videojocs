using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cercle : MonoBehaviour
{
    public Cuadrat cuadrat;
    void Start()
    {
        //subscriures. Ens subscribim al delegat amb una funcio
        cuadrat.OnFerAlgo += Activar;
        cuadrat.OnFerArguments += ActivarArguments;
    }

    private void Activar()
    {
        print("Cercle s'ha activat");
    }

    private void ActivarArguments(int a)
    {
        print("m'han passat un " + a);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //aquesta funcio salta automaticament a l'esborrar l'objecte o desactivarlo
    private void OnDisable()
    {
        //ens dessuscribim
        cuadrat.OnFerAlgo -= Activar;
    }
}
