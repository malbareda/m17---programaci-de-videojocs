using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnerpincho : MonoBehaviour
{
    public GameObject[] pinchos;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("spawn", 0f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void spawn()
    {
        
        Instantiate(pinchos[Random.Range(0, 3)]);
    }
}
