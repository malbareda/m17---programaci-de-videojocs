using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTerreny : MonoBehaviour
{

    public GameObject[] terrenys;
    // Start is called before the first frame update
    void Start()
    {

        int index = Random.Range(0, terrenys.Length);
        Instantiate(terrenys[index]);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
