using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElHorror : MonoBehaviour
{
    public GameObject grego;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("moreGregos", 1f, 1f);
    }

    void moreGregos()
    {
        Instantiate(grego);
        grego.transform.position = new Vector3(0, -10, 0);
    }

}
