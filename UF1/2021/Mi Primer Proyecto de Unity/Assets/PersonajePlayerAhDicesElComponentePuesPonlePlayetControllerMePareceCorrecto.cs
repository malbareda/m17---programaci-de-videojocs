using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonajePlayerAhDicesElComponentePuesPonlePlayetControllerMePareceCorrecto : MonoBehaviour
{
    // Start is called before the first frame update
    public int spd;
    private bool terra = false;
    public int temps = 0;
    void Start()
    {
        Debug.Log(Input.GetKey("d"));
        InvokeRepeating("pujarSegon", 1f, 1f);
    }

    void pujarSegon()
    {
        temps++;
    }

    // Update is called once per frame
    void Update()
    {
        /*Debug.Log(Input.GetKey("d"));
        if (Input.GetKey("d"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(spd, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        }
        else if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(-spd, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        }
        */
        if (Input.GetKeyDown("space"))
        {
            if (terra)
            {
                this.GetComponent<Rigidbody2D>().gravityScale *= -1;
                this.transform.localScale = new Vector2(1, this.transform.localScale.y * -1);
                terra = false;
            }
        }
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        terra = true;
        if (collision.transform.tag == "grego")
        {
            Debug.Log("Gregorio te da la brasa");
            Destroy(this.gameObject);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "grego")
        {
            Debug.Log("Gregorio te da la brasa");
            Destroy(this.gameObject);
        }
    }
}
