using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobertoArrastrable : MonoBehaviour
{
    public GameEvent roberto;
    private bool mouseup = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseDrag()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.transform.position = new Vector2(pos.x, pos.y);
    }

    private void OnMouseDown()
    {
        mouseup = false;
    }


    private void OnMouseUp()
    {
        mouseup = true;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (mouseup && collision.gameObject.tag=="pi�a")
        {
            print("mua");
            Destroy(this.gameObject);
            roberto.Raise();
            
        }
    }

}
