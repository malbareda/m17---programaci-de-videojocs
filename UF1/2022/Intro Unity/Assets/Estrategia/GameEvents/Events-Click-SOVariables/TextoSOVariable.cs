using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextoSOVariable : MonoBehaviour
{

    float score = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void actualizar()
    {
        score++;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "RobertoPuntos: " + score;
    }

    public void actualizarConFloat(float f, float f2)
    {
        score= score+f+f2;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "RobertoPuntos: " + score;
    }
}
