using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpanwerSOVariable : MonoBehaviour
{
    public GameObject rob;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnroberto());       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator spawnroberto()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            GameObject newRob = Instantiate(rob);
            newRob.transform.localScale = new Vector2(Random.Range(0.1f, 0.25f), Random.Range(0.1f, 0.25f));
        }
    }
}
