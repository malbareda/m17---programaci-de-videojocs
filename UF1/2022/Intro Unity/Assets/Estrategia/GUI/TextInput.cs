using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextInput : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void canviText(string nouText)
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = nouText;
    }

    public void seleccionarText()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().color = Color.green;
    }

    public void desseleccionarText()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().color = Color.white;
    }
}
