using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toggle()
    {
        if (this.GetComponentInChildren<TMPro.TextMeshProUGUI>().text == "Menu")
        {
            this.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Menun't";
        }
        else
        {
            this.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Menu";
        }
    }
}
