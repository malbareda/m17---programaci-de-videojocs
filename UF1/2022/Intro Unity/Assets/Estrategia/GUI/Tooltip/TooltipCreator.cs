using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipCreator : MonoBehaviour
{
    GameObject tooltip;

    // Start is called before the first frame update
    void Start()
    {
        tooltip = this.transform.GetChild(0).gameObject;
        
        
        //sprite = this.transform.GetChild(0).GetComponentInChildren<Image>();

        //text = this.transform.GetChild(0).GetComponentInChildren<TMPro.TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void crearTooltip(Sprite s, string s2, Vector3 v)
    {
        tooltip.SetActive(true);
        tooltip.transform.position = v+Vector3.up*2;
        tooltip.transform.GetChild(0).GetComponent<Image>().sprite = s;
        tooltip.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = s2;
        StartCoroutine(disableTooltip());

    }

    IEnumerator disableTooltip()
    {
        yield return new WaitForSeconds(3);
        tooltip.SetActive(false);
    }
}
