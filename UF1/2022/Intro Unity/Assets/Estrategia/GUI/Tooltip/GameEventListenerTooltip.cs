// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Events;

public class GameEventListenerTooltip : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEventTooltip Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<Sprite, string, Vector3> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(Sprite f, string f2, Vector3 v)
    {

        Response.Invoke(f, f2,v);
    }
}

