using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipCoses : MonoBehaviour
{
    public string descripcio;
    bool enter = false;
    [SerializeField]
    private GameEventTooltip GameEventTooltip;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseEnter()
    {
        enter = true;
        StartCoroutine(tooltipWait());
    }

    private void OnMouseExit()
    {
        enter = false;
    }

    IEnumerator tooltipWait()
    {
        yield return new WaitForSeconds(1);
        if (enter)
        {
            GameEventTooltip.Raise(this.GetComponent<SpriteRenderer>().sprite, descripcio,transform.position);
        }

    }
}
