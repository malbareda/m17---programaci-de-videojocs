using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameEventTooltip : ScriptableObject
{

    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<GameEventListenerTooltip> eventListeners =
        new List<GameEventListenerTooltip>();

    public void Raise(Sprite s, string s2, Vector3 v)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(s,s2,v);
    }

    public void RegisterListener(GameEventListenerTooltip listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerTooltip listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
