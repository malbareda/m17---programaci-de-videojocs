using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjecteMenu : MonoBehaviour
{
    public Sprite r;
    public Sprite j;
    [SerializeField]
    int angSpd=5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toggleRotate(bool t)
    {
        if (t)
        {


            this.GetComponent<Rigidbody2D>().angularVelocity = angSpd;
        }
        else this.GetComponent<Rigidbody2D>().angularVelocity = 0f;
    }

    public void redden(float f)
    {
        this.GetComponent<SpriteRenderer>().color = new Color(1, 1 - f, 1 - f);
    }

    public void changeSprite(int num)
    {
        if (num == 0)
        {
            this.GetComponent<SpriteRenderer>().sprite = j;
        }else if (num == 1)
        {
            this.GetComponent<SpriteRenderer>().sprite = r;
        }
    }

    public void botons(int opcio)
    {
        switch (opcio)
        {
            case 1:
                angSpd++;
                break;
            case 2:
                if (angSpd > 1) angSpd--;
                break;
            case 3:
                angSpd *= -1;
                break;
        }
    }
}
