using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioA : MonoBehaviour
{
    Carlos carlos;
    // Start is called before the first frame update
    void Start()
    {
         carlos = this.transform.parent.GetComponent<Carlos>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("RadioA");
        carlos.transforms.Add(collision.transform);
        carlos.avisar();
        
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        carlos.transforms.Remove(collision.transform);
        carlos.avisar();
    }


}
