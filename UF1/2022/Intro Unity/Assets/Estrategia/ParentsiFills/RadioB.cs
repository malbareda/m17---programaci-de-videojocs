using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioB : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("RadioB");
        if (collision.tag == "veg")
        {
            this.transform.parent.GetComponent<Carlos>().cambioColor(Color.green);
        }
        else if (collision.tag == "carne")
        {
            this.transform.parent.GetComponent<Carlos>().cambioColor(Color.red);
        }
    }
}
