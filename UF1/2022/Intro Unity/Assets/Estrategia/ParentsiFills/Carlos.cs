using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carlos : MonoBehaviour
{
    public List<Transform> transforms = new List<Transform>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("CarlosT");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print("CarlosCol");
    }

    public void cambioColor(Color c)
    {
        this.GetComponent<SpriteRenderer>().color = c;
    }

    public void avisar()
    {
        int veggies = 0;
        int meaties = 0;
        foreach(Transform t in transforms)
        {
            if (t.tag == "veg") veggies++;
            else if (t.tag == "carne") meaties++;
        }

        if (veggies > meaties) this.GetComponent<SpriteRenderer>().color = Color.green;
        else if (veggies < meaties) this.GetComponent<SpriteRenderer>().color = Color.red;
        else this.GetComponent<SpriteRenderer>().color = Color.white;
    }
}
