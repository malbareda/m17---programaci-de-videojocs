using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolSalta : MonoBehaviour
{
    GameObject joan;
    // Start is called before the first frame update
    void Start()
    {
        joan = GameObject.Find("joan_quadrada");
        joan.GetComponent<JoanSalta>().joansaltaEvent += polSalta;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void polSalta()
    {
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 200));
    }
}
