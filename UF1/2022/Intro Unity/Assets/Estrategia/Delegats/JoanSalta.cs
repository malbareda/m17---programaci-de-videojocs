using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoanSalta : MonoBehaviour
{
    //un delegat es un punter a una funcio. Es una variable pero que dintre de la variable hi ha una funcio
    public delegate void JoanSaltaEventHandler();
    //un event es una cosa que es pot invocar.
    public event JoanSaltaEventHandler joansaltaEvent;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 200));            joansaltaEvent.Invoke();
        }
    }
}
