using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class infectat1 : MonoBehaviour
{
    //un delegat es un punter a una funci�. �s una variable que est� representant una funci�
    public delegate void Infectar();
    //l'event fa refer�ncia a una funci�, indicada per el delegat.
    public event Infectar InfectarEvent;

    /*
     * Events.
     * 
     * Els events funcionen com un sistema de notificacions.
     * Tu tens subscripcions a l'event. Et subscrius al seu "sistema de notificacions intern"
     * Quan l'event salta mitjan�ant Invoke, aleshores tot all� subscrit a l'event rep una notificaci�
     * 
     * */

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("inf", 3f, 3f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void inf()
    {
        //voldriem que saltes un event que notifiques al gameController que ha de crear un nou infectat.

        //si l'event no es buit
        if (InfectarEvent != null)
        {
            //invocar l'event. Per tant estem invocant la funci� delegada infectar.
            InfectarEvent.Invoke();
        }

    }
}

