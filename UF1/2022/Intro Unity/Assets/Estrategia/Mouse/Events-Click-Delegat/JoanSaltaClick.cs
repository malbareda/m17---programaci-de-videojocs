using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoanSaltaClick : MonoBehaviour
{
    private bool selected = false;
    ClickManager clickManager;
    public int numero;
    // Start is called before the first frame update
    void Start()
    {
        clickManager = GameObject.Find("Manager").GetComponent<ClickManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)&&selected)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 200));
            
        }
    }

    private void OnMouseDown()
    {
        clickManager.clicked(numero);
        clickManager.clickEvent += unclick;
        selected = true;
        this.GetComponent<SpriteRenderer>().color = Color.red;
    }

    public void unclick(int i)
    {
        print("he desseleccionat perque acabo de clickar a roberto " + i);
        selected = false;
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }
}
