using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ClickManager : MonoBehaviour
{
    public delegate void click(int i);
    public event click clickEvent;

    public void clicked(int i)
    {
        
        clickEvent?.Invoke(i);
        clickEvent=null;
    }

}
