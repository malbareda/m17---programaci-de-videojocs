using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoParallaxKawaii : MonoBehaviour
{
    public float spd;
    public Transform spawn;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-spd, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x<-15)
        {
            //print(transform.name + "a");
            this.transform.position = spawn.position;
        }
        
    }
}
