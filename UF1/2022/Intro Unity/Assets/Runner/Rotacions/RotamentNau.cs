using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotamentNau : MonoBehaviour
{
    public GameObject gitlab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            this.transform.Rotate(0, 0, -1);
            this.GetComponent<Rigidbody2D>().velocity = this.GetComponent<Rigidbody2D>().velocity.magnitude * this.transform.up;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            this.GetComponent<Rigidbody2D>().velocity = -this.transform.up * 1.5f;

        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.transform.Rotate(0, 0, +1);
            this.GetComponent<Rigidbody2D>().velocity = this.GetComponent<Rigidbody2D>().velocity.magnitude * this.transform.up;
        }
        else if (Input.GetKey(KeyCode.W))
        {
            this.GetComponent<Rigidbody2D>().velocity = this.transform.up * 3;
        }
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject newGitlab = Instantiate(gitlab);
            newGitlab.transform.position = this.transform.position;
            newGitlab.GetComponent<Rigidbody2D>().AddForce(this.transform.up * 7,ForceMode2D.Impulse);
        }
    }
}
