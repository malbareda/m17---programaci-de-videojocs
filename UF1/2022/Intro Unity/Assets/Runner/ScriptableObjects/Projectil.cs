using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Projectil : ScriptableObject
{
    public float spd;
    public float cd;
    public float scale;
    public Color color;
}
