using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cano : MonoBehaviour
{
    [SerializeField]
    private Projectil[] projectils;
    public GameObject cercle;
    int tipus = 0;
    // Start is called before the first frame update
    void Start()
    {
        GameObject newCercle = Instantiate(cercle);
        StartCoroutine(shoot());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Alpha1)) tipus = 0;
        if (Input.GetKey(KeyCode.Alpha2)) tipus = 1;
        if (Input.GetKey(KeyCode.Alpha3)) tipus = 2;
    }

    IEnumerator shoot()
    {
        while (true)
        {
            Projectil projectilActual = projectils[tipus];
            GameObject newCercle = Instantiate(cercle);
            newCercle.GetComponent<Rigidbody2D>().velocity = new Vector2(projectilActual.spd, 0);
            newCercle.GetComponent<SpriteRenderer>().color = projectilActual.color;
            newCercle.transform.localScale *= projectilActual.scale;

            yield return new WaitForSeconds(projectilActual.cd);


        }
    }

}
