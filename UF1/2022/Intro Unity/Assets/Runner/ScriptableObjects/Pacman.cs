using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pacman : MonoBehaviour
{
    public DatosComida datosPi�a;
    // Start is called before the first frame update
    Rigidbody2D rb;
    void Start()
    {
        datosPi�a.comibilidad = true;
        rb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            rb.velocity = new Vector2(-3, 0);
        }else if (Input.GetKey(KeyCode.S))
        {
            rb.velocity = new Vector2(0, -3);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector2(3, 0);
        }
        else if (Input.GetKey(KeyCode.W))
        {
            rb.velocity = new Vector2(0,3);
        }else if (Input.GetKeyDown(KeyCode.Space))
        {
            //toggle comibilidad.
            
            
            datosPi�a.comibilidad = !datosPi�a.comibilidad;
            if(!datosPi�a.comibilidad)this.GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);
            else this.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
        }

        datosPi�a.posicion = this.transform.position;
        
    }
}
