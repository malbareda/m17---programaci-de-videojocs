using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roberto : MonoBehaviour
{
    // Start is called before the first frame update

    public DatosComida datosPi�a;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (datosPi�a.comibilidad)
        {
            if (datosPi�a.posicion.x < this.transform.position.x)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(3, this.GetComponent<Rigidbody2D>().velocity.y);
            }

            if (datosPi�a.posicion.y < this.transform.position.y)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x,-3);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 3);
            }
        }
        else
        {
            if (datosPi�a.posicion.x < this.transform.position.x)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(3, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, this.GetComponent<Rigidbody2D>().velocity.y);
            }

            if (datosPi�a.posicion.y < this.transform.position.y)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 3);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -3);
            }
        }
    }
}
