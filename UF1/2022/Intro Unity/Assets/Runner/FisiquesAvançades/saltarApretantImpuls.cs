using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class saltarApretantImpuls : MonoBehaviour
{
    private bool jumping;
    private bool stopJumping;
    // Start is called before the first frame update
    void Start()
    {
        jumping = false;
        stopJumping = false;
        StartCoroutine(comunisme());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !jumping){
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 5),ForceMode2D.Impulse);
            jumping = true;
            stopJumping = false;
            StartCoroutine(saltando());
        }else if (Input.GetKey(KeyCode.Space) && !stopJumping)
        {
            //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0.5f));
        }
        else if(jumping)
        {
            stopJumping = true;
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.name == "suelo") jumping = false;
    }

    private IEnumerator saltando()
    {
        yield return new WaitForSeconds(0.5f);
        stopJumping = true;
    }

    private IEnumerator comunisme()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            Color col = this.GetComponent<SpriteRenderer>().color;
            Color newCol = new Color(col.r + 0.01f, col.g-0.01f, col.b - 0.01f);
            this.GetComponent<SpriteRenderer>().color = newCol;
            print(this.GetComponent<SpriteRenderer>().color);
            if (this.GetComponent<SpriteRenderer>().color.b <= 0)
            {
                break;
            }

        }
        print("canvi");
        StartCoroutine(antiComunisme());
        
    }

    private IEnumerator antiComunisme()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            Color col = this.GetComponent<SpriteRenderer>().color;
            Color newCol = new Color(col.r + 0.01f, col.g + 0.01f, col.b + 0.01f);
            this.GetComponent<SpriteRenderer>().color = newCol;
            if (this.GetComponent<SpriteRenderer>().color.b >= 1)
            {
                break;
            }

        }
        StartCoroutine(comunisme());

    }
}
