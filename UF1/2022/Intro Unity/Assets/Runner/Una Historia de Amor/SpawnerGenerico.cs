using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerGenerico : MonoBehaviour
{

    public enum Tipo {Cerveza,Pi�a};

    public Tipo tipo;
    public GameObject spawneado;
    public float cooldown;
    public int cantidad;    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnear());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //corrutina, se ejecuta asincronamente
    IEnumerator spawnear()
    {
        while (true)
        {
            yield return new WaitForSeconds(cooldown);
            if (tipo == Tipo.Cerveza)
            {
                for (int i = 0; i < cantidad; i++)
                {
                    GameObject cervezaInstanciada = Instantiate(spawneado);
                    float posicionH = Random.Range(-5.41f, 5.77f);
                    cervezaInstanciada.transform.position = new Vector2(posicionH, 7);
                }
            }else if(tipo == Tipo.Pi�a)
            {
                GameObject newPi�a = Instantiate(spawneado);
                newPi�a.GetComponent<Rigidbody2D>().velocity = new Vector2(-6, 0);
           }
        }
        
    }
}
