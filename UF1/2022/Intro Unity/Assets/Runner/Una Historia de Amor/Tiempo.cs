using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.SceneTemplate;
using UnityEngine;

public class Tiempo : MonoBehaviour
{
    private float temps = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //TMPro.TextMeshProUGUI componentText = this.GetComponent<TextMeshProUGUI>();
        

        //Time.deltaTime el temps que ha passat entre l'ultim frame i l'actual
        temps += Time.deltaTime;

        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Temps: " + temps; ;   

    }


}
