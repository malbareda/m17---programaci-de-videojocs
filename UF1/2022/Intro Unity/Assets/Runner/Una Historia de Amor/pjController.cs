using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pjController : MonoBehaviour
{
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        //Input.getKeyDown()
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector2(3, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rb.velocity = new Vector2(-3, rb.velocity.y);
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(new Vector2(0, 500));
        }

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "objetoDelAmorDeR")
        {
            //sabemos que chocamos contra B
            print("El Amor ha triunfado");
        }
        if (collision.transform.tag == "pi�a")
        {
            print("No, es que esta pi�a es de Carlos");
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "cervesafriaporfavor")
        {
            print("Game Over");
            Destroy(this.gameObject);
        }
    }


}
