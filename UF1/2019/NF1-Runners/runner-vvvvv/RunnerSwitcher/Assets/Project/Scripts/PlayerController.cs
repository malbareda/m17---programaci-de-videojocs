﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	#region Delegates
	public delegate void OnHitSpikeAction();
	public delegate void OnHitGoombaAction();
	public delegate void OnHitOrbAction();
	#endregion Delegates

	#region Events
	public OnHitGoombaAction OnHitGoomba;
	public OnHitSpikeAction OnHitSpike;
	public OnHitOrbAction OnHitOrb;
	#endregion Events

	#region Constants
	private float SPEED = 550;
	private float JUMP_SPEED = 50000;
	#endregion Constants

	#region Fields
	private Vector3 leftBound;
	private Vector3 rightBound;
	private bool canJump;
	#endregion Fields

	#region Methods
	public void Update ()
	{
		ProcessInput();

		this.GetComponent<Rigidbody2D>().velocity = new Vector2
		(
			SPEED * Time.deltaTime,
			this.GetComponent<Rigidbody2D>().velocity.y
		);
	}

	private void ProcessInput ()
	{
		if (Input.GetKeyDown("up") || Input.GetKeyDown("w") || Input.GetKeyDown("space")) { InvertGravity(); }
	}

	private void InvertGravity ()
	{
		Physics2D.gravity *= -1;
	}

	private void Jump (bool force = false)
	{
		if (canJump || force)
		{
			canJump = false;

			this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * JUMP_SPEED * Time.deltaTime);
		}
	}

	public void OnCollisionEnter2D (Collision2D collision)
	{
		if (collision.gameObject.tag == "Bound")
		{
			canJump = true;
		}

		if (collision.gameObject.GetComponent<SpikeController>() != null)
		{
			if (OnHitSpike != null)
			{
				OnHitSpike();
			}
		}
		else if (collision.gameObject.GetComponent<EnemyController>() != null)
		{
			EnemyController enemy = collision.gameObject.GetComponent<EnemyController>();
			
			if (this.transform.position.y > enemy.transform.position.y + enemy.GetComponent<BoxCollider2D>().size.y / 2)
			{
				GameObject.Destroy(collision.gameObject);

				Jump(true);

				if (OnHitGoomba != null)
				{
					OnHitGoomba();
				}
			}
			else
			{
				if (OnHitSpike != null) { OnHitSpike(); }
			}
		}
		else if (collision.gameObject.GetComponent<OrbController>() != null)
		{
			if (OnHitOrb != null)
			{
				OnHitOrb();
			}
		}
	}
	#endregion Methods
}