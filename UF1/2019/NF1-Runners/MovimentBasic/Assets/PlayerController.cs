﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public int vel;
    public int fSalto;
    private bool puedeSaltar = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("d"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(vel, 0));
            //this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey("a"))
        {
            ///this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-vel, 0));
            
        }
        /*else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }*/
        ///Debug.Log("la velocidad es" + this.GetComponent<Rigidbody2D>().velocity);


        if (Input.GetKey("w"))
        {
            if (puedeSaltar)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, fSalto));
                puedeSaltar = false;
            }
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {

        Debug.Log("colisionado con "+collision.gameObject.name);

        if(collision.gameObject.CompareTag("Floor"))
        {
            puedeSaltar = true;
        }
        
    }
}
