﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameSceneManager : MonoBehaviour
{
	#region Constants
	private float FARTHEST_BLOCK_DISTANCE = 50f;
	#endregion Constants

	#region Unity Fields
	public Camera mainCamera;
	public Text scoreText;
	public Text gameOverText;
	public PlayerController player;
	public Transform blockContainer;
	#endregion Unity Fields

	#region Fields
	private int score;
	private float gameTimer;
	private bool gameOver;

	private float pointerScenarioBlocks = -10f;
	private float pointerHazardBlocks = 30f;
	private GameObject[] scenarioBlockPrefabs;
	private GameObject[] hazardBlockPrefabs;
	private List<BlockController> blocks;
	private float previousX;
	private float stuckTimer;
	#endregion Fields

	#region Methods
	public void Start ()
	{
		Time.timeScale = 1;

		player.OnHitGoomba += OnHitGoomba;
		player.OnHitSpike += OnGameOver;

		blocks = new List<BlockController>(20);

		scenarioBlockPrefabs = Resources.LoadAll<GameObject>("Prefabs/Blocks/Scenario");
		hazardBlockPrefabs = Resources.LoadAll<GameObject>("Prefabs/Blocks/Hazards");
	}

	public void Update ()
	{
		if (gameOver)
		{
			if (Input.GetKeyDown("r"))
			{
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			}

			return;
		}

		// Stuck logic
		if (Mathf.Abs(player.transform.position.x - previousX) < 0.01)
		{
			stuckTimer += Time.deltaTime * 1.5f;
		}
		else
		{
			stuckTimer = Mathf.Max(stuckTimer - Time.deltaTime, 0);
		}
		previousX = player.transform.position.x;

		if (player.transform.position.y < -10) OnGameOver();
		if (Mathf.Abs(mainCamera.transform.position.x - player.transform.position.x) > 15) OnGameOver();

		// Stick camera to player
		mainCamera.transform.position = new Vector3
		(
			Mathf.Lerp(mainCamera.transform.position.x + stuckTimer, player.transform.position.x, Time.deltaTime * 10),
			-1,
			mainCamera.transform.position.z
		);

		scoreText.text = "Score: " + Mathf.Floor(player.transform.position.x);

		// Spawn blocks
		while (pointerScenarioBlocks < player.transform.position.x + FARTHEST_BLOCK_DISTANCE)
		{
			GameObject blockObject = GameObject.Instantiate<GameObject>(scenarioBlockPrefabs[Random.Range(0, scenarioBlockPrefabs.Length)]);

			BlockController block = blockObject.GetComponent<BlockController>();
			block.transform.SetParent(blockContainer);
			block.transform.localPosition = new Vector3(pointerScenarioBlocks, 0, 0);

			pointerScenarioBlocks += block.size;

			blocks.Add(block);
		}

		while (pointerHazardBlocks < player.transform.position.x + FARTHEST_BLOCK_DISTANCE)
		{
			GameObject blockObject = GameObject.Instantiate<GameObject>(hazardBlockPrefabs[Random.Range(0, hazardBlockPrefabs.Length)]);

			BlockController block = blockObject.GetComponent<BlockController>();
			block.transform.SetParent(blockContainer);
			block.transform.localPosition = new Vector3(pointerHazardBlocks, 0, 0);

			pointerHazardBlocks += block.size;

			blocks.Add(block);
		}

		// Clears blocks
		List<BlockController> blocksToDestroy = new List<BlockController>();
		foreach (BlockController block in blocks)
		{
			if (block.transform.position.x < player.transform.position.x - FARTHEST_BLOCK_DISTANCE)
			{
				blocksToDestroy.Add(block);
			}
		}

		foreach (BlockController blockToDestroy in blocksToDestroy)
		{
			blocks.Remove(blockToDestroy);
			GameObject.Destroy(blockToDestroy.gameObject);
		}
	}

	private void OnHitGoomba ()
	{
		this.score += 100;
	}

	private void OnGameOver ()
	{
		gameOver = true;

		scoreText.enabled = false;
		gameOverText.enabled = true;

		gameOverText.text = "Game over!\nScore: " + Mathf.Floor(player.transform.position.x) + "\nPress R to restart";

		Time.timeScale = 0;
	}
	#endregion Methods
}