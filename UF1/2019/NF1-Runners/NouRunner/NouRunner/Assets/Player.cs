﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int jumpForce;
    public int horizontalVelocity;
    private bool canJump;
    // Start is called before the first frame update


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space") && canJump)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
        }
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(horizontalVelocity, this.GetComponent<Rigidbody2D>().velocity.y);


        
        
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name == "Floor")
        {
            canJump = true;
        }
    }

    //esto se llamara automaticamente cada vez que choques con un collider que tenga el trigger activado
    void OnTriggerEnter2D(Collider2D coll)
    {
        Debug.Log(coll.tag);
        if (coll.tag == "Obstacle")
        {
            GameObject.Destroy(this.gameObject);
        }
    }
}
