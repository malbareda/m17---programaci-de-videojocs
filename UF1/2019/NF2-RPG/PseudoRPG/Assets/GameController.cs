﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject santi;
    // Start is called before the first frame update
    public GameObject heli;

    void Start()
    {
        InvokeRepeating("VOX", 0, 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void VOX()
    {
        Debug.Log("Nuevo votante de VOX");
        GameObject newFranco = Instantiate(santi, this.transform);
        santi.transform.position = new Vector2(Random.Range(-4.0f, 4.0f), Random.Range(-4.0f, 4.0f));
        //suscripcion al evento. Ahora cuando el evento salte, llamara a la funcion Alarma.
        newFranco.GetComponent<FrancoMuerto>().vivaEspanyaEvent += Alarma;
        heli.GetComponent<HelicopterController>().nuevoFranco(newFranco);
    }


    void Alarma(string nom, Vector2 pos)
    {
        Debug.Log("Mierda, "+nom+" ha explotado");
    }
}
