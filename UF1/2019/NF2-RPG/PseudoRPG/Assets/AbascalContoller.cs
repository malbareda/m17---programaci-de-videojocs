﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AbascalContoller : MonoBehaviour
{


    public string esp;
    public int vel;

    private int votantes;
    public int Votantes
    {
        get
        {
            Debug.Log("putos rojos");
            return votantes;
        }
        set
        {
            if (value < 10)
            {
                votantes = value;
            }

        }
    }

    //instancia del singleton
    static GameObject abascal = null;


    // Awake is called on object instantiation
    void Awake()
    {
        //Patró singleton
        if(abascal == null)
        {
            //crea el objecte en el primer moment
            abascal = this.gameObject;
            //per defecte els objectes es destrueixen al carregar una altra escena. 
            //D'aquesta manera no es destrueix al carretgarse
            DontDestroyOnLoad(abascal);
        }
        else
        {
            //si no es el primer objecte creat (perque tornes a l'escena a on esc crea, es destrueix automàticament, d'aquesta forma no téns múltiples instàncies del mateix objecte (singleton)
            Destroy(this.gameObject);
        }
        
    }


    // Start is called before the first frame update
    void Start()
    {
        //Invoke crida uan funcio al cap de n segons
        Invoke("cosasNazis", 2);
        //Invoke repeating la crida periòdicament
       // InvokeRepeating("cosasNazis", 5, 2);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Input.inputString);
        if(Input.GetKey("w")){
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
            ///Guarda un objecte a PlayerPrefs. Aquest objecte queda guardat encara que tanquis el joc.
            //PlayerPrefs.SetString("esp","callarse catalufos que no escucho");
        }else if (Input.GetKey("s"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
        }
        if (Input.GetKey("d"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }

        if (Input.GetKey("o"))
        {
            this.saveGame();
        }
        if (Input.GetKey("p"))
        {
            this.LoadGame();
        }







    }

    void cosasNazis()
    {
        Debug.Log("arriba españa");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log(collision.gameObject.name);
        if (collision.gameObject.name == "Alcantarilla")
        {
            SceneManager.LoadScene("Bertin");
        }
        else if (collision.gameObject.name == "seta")
        {
        }
    }

    private Save createSave()
    {
        Save save = new Save();
        save.abascalX = this.transform.position.x;
        save.abascalY = this.transform.position.y;

        return save;

    }

    private void saveGame()
    {
        //crees el objecte save
        Save save = this.createSave();
        //crees un BinaryFormatter que es com un OOS
        BinaryFormatter bf = new BinaryFormatter();
        //Crees el File
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        //serialitzes
        bf.Serialize(file, save);
        file.Close();

        Debug.Log("Game Saved");

    }

    public void LoadGame()
    {
        // 1
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            

            // 2
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save save = (Save)bf.Deserialize(file);
            file.Close();

            // 3
            this.transform.position = new Vector2(save.abascalX, save.abascalY);

            Debug.Log("Game Loaded");

        }
        else
        {
            Debug.Log("No game saved!");
        }
    }
}
