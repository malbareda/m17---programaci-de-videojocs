﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject abascal;
    void Start()
    {
        Invoke("volver", 5);
        //Find troba un GameObject ja existent per nom
        abascal = GameObject.Find("Abascal");
        //Recordem que els scripts son un component i per tant es poden accedir mitjançant GetComponent
        Debug.Log(abascal.GetComponent<AbascalContoller>().vel);
        abascal.transform.position = new Vector2(GameObject.Find("Abascal").transform.position.x, GameObject.Find("Abascal").transform.position.y - 0.5f);
        //SetActive. Un objecte desactivat no crida l'update, ni el troba el Find
        abascal.SetActive(false);
        //Accedeix al PlayerPrefs. Trobara la clau encara que s'hagi apagat el joc entre desat i carregat
        Debug.Log(PlayerPrefs.GetString("esp"));
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void volver()
    {
        //tornem a activar l'objecte
        abascal.SetActive(true);

        SceneManager.LoadScene("SampleScene");
    }
}
