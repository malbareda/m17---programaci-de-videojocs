﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class triangle : MonoBehaviour
{

    public int vel = 2;
    

    //instancia del singleton
    static GameObject santi = null;

    //es el metode que es crida quan s'instancia l'objecte
    //es el mes similar a un constructor pur
    private void Awake()
    {
        if (santi == null)
        {
            //crea l'objecte
            santi = this.gameObject;
            //especifica que l'objecte no es destrueixi al canviar d'escena
            DontDestroyOnLoad(santi);
        }
        else
        {
            Destroy(this.gameObject);
        }
        
    }

    // Start es crida just abans de fer el primer frame.
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.localScale *= 1.001f;

        if (Input.GetKey("w"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
        }
        else if (Input.GetKey("s"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
        }
        if (Input.GetKey("d"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "Alcantarilla")
        {
            //cambiar de escena
            SceneManager.LoadScene("New TileMap");
        }else if(collision.gameObject.name == "seta"){
            SceneManager.LoadScene("SampleScene");
        }
    }
}
