﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrancoMuerto : MonoBehaviour
{


    //un delegat es agafar una funcio i convertirla en una variable
    public delegate void vivaEspanya(string nom, Vector2 pos);
    //un event es un delegat que pot invocar funcions alienes.
    public event vivaEspanya vivaEspanyaEvent;

    string nombre;
    // Start is called before the first frame update
    void Start()
    {
        nombre = "Franco " + Random.Range(0, 1000000);
        InvokeRepeating("Explosion", 0, 2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Explosion()
    {
        int i = Random.Range(1, 10);
        if (i == 9)
        {
            //si hay alguien suscrito en el evento
            if (vivaEspanyaEvent != null)
            {
                vivaEspanyaEvent(this.nombre, this.transform.position);
            }
            Destroy(this.gameObject);

        }
        


    }
}
