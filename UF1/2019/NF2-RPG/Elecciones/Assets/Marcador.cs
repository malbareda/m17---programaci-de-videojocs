﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Marcador : MonoBehaviour
{
    int vox = 0;
    public GameObject votante;

    // Start is called before the first frame update
    void Start()
    {
        votante.GetComponent<VotanteController>().eventVotar += votos; 
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void votos()
    {
        vox++;
        this.GetComponent<Text>().text = "Votos de VOX " + vox; 
    }
}
