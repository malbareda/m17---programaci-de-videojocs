﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Marcador2 : MonoBehaviour
{
    public GameObject votante;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("newVotante",0,2);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void newVotante()
    {
        GameObject newVotante = Instantiate(votante);
        newVotante.GetComponent<VotanteController2>().eventVotar += voto;
    }

    void voto(string s)
    {
        Debug.Log("a");
        this.GetComponent<Text>().text = "NUEVO VOTO PARA " + s;
    }
}
