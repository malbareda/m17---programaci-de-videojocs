﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VotanteController2 : MonoBehaviour
{
    public delegate void votar(string partido);
    public event votar eventVotar;

    public string[] partidos = { "PSOE", "PP", "VOX", "DrogasDuras", "El partido de mierda del muñon", "ERC", "PuigdemontFanClub" };
    // Start is called before the first frame update
    void Start()
    {
        int rand = Random.Range(0, partidos.Length);
        eventVotar(partidos[rand]);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
