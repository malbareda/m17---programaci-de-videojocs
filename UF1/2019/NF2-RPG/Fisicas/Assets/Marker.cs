﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marker : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.transform.Rotate(new Vector3(0, 0, 60));
        this.transform.Translate(new Vector2(Random.Range(5, 30), 0));
        this.transform.Rotate(new Vector3(0, 0, -60));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
