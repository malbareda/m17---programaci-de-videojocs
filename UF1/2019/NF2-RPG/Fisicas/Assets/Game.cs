﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public Camera camera;
    public GameObject curling;
    public GameObject marker;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        camera.transform.position = new Vector3(curling.transform.position.x, camera.transform.position.y, camera.transform.position.z);
        Debug.Log(curling.GetComponent<Rigidbody2D>().velocity.x);
        if (curling.GetComponent<Rigidbody2D>().velocity.x < 0.00000001)
        {
            Debug.Log("Final "+ Mathf.Abs(curling.transform.position.x - marker.transform.position.x));
            Destroy(this);
        }
    }
}
