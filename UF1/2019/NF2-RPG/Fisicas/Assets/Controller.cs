﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<BoxCollider2D>().sharedMaterial.friction = 0.0005f;
        this.GetComponent<BoxCollider2D>().enabled = false;
        this.GetComponent<BoxCollider2D>().enabled = true;
        InvokeRepeating("fregar", 0,0.2f);
    }

    // Update is called once per frame
    void Update()
    {

        //Debug.Log(this.GetComponent<BoxCollider2D>().sharedMaterial.friction);
        
    }

    void fregar()
    {
        if (Input.GetKey("space"))
        {
            Debug.Log("esto baja");
            this.GetComponent<BoxCollider2D>().sharedMaterial.friction -= 0.0001f;
            this.GetComponent<BoxCollider2D>().enabled = false;
            this.GetComponent<BoxCollider2D>().enabled = true;
        }
       /* else
        {
            this.GetComponent<BoxCollider2D>().sharedMaterial.friction += 0.00005f;
            this.GetComponent<BoxCollider2D>().enabled = false;
            this.GetComponent<BoxCollider2D>().enabled = true;
        }*/
    }
}
