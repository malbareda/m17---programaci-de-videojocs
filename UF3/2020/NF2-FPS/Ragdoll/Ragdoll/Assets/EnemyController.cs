﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
 
public class EnemyController : MonoBehaviour
{
    public bool isDead;
    public Transform player;
    public GameObject playerModel;
    public float moveSpeed = 1;

    public GameObject playerRagdoll;

    private void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isDead = true;
            playerModel.SetActive(false);
            playerRagdoll.transform.position = playerModel.transform.position;
            playerRagdoll.SetActive(true);
        }
    }
}