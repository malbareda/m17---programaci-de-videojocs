﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatMovil : MonoBehaviour
{
    public int spd = 10;
    private int dir = 1;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(moute());
    }

    private IEnumerator moute()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            dir *= -1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody>().velocity = this.transform.right * spd * dir;
    }
}
