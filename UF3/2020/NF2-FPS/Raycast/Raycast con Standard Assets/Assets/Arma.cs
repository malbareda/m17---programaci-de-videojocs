﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : MonoBehaviour
{
    public Camera FPCamera;
    public GameObject explosion;
    float rango = 50f;
    float momento = 2500f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        RaycastHit hit;

        

        if(Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, rango))
        
        {

            print(hit.transform.name);
            Debug.DrawLine(FPCamera.transform.position, hit.point, Color.red, 1f);
            GameObject newExplosion = Instantiate(explosion);
            newExplosion.transform.position = hit.point;
            Destroy(newExplosion, 1f);
            if(hit.transform.tag == "ElsQueCantes")
            {
                hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal*momento, hit.point);
            }
            if (hit.transform.tag == "Ragdoll")
            {
                hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momento, hit.point);
                hit.transform.gameObject.SetActive(false);
                hit.transform.parent.GetChild(0).gameObject.SetActive(true);
            }
        }
       
    }
}
