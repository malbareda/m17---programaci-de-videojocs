﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MeshDeformer : MonoBehaviour
{
    public int force = 10;
    public float forceOffset = 0.1f;
    Mesh deformingMesh;
    Vector3[] originalVertices, displacedVertices;
    Vector3[] vertexVelocities;
    // Start is called before the first frame update
    void Start()
    {
        deformingMesh = GetComponent<MeshFilter>().mesh;
        originalVertices = deformingMesh.vertices;
        displacedVertices = new Vector3[originalVertices.Length];
        for (int i = 0; i < originalVertices.Length; i++)
        {
            displacedVertices[i] = originalVertices[i];
        }
        vertexVelocities = new Vector3[originalVertices.Length];
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;


            if (Physics.Raycast(inputRay, out hit))
            {
                print(hit.point);
                MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();
                if (deformer)
                {
                 
                    Vector3 point = hit.point;
                    point += hit.normal * forceOffset;
                    deformer.AddDeformingForce(point, force);
                }
            }
        }


        
    }
    public void AddDeformingForce(Vector3 point, float force)
    {
        
        for (int i = 0; i < displacedVertices.Length; i++)
        {
            AddForceToVertex(i, point, force);
        }
        
        deformingMesh.vertices = displacedVertices;
        deformingMesh.RecalculateNormals();
    }

    void AddForceToVertex(int i, Vector3 point, float force)
    {
       //calculem distancia al punt que hem aplicat la força
        Vector3 pointToVertex = displacedVertices[i] - point;
        //calculem la força depenent de la distancia (la distancia es diu magnitud)
        float attenuatedForce = force / (1f + pointToVertex.sqrMagnitude);
        //calculem la velocitat a partir de la força v = acc * t;
        //float velocity = attenuatedForce * Time.deltaTime;
        //vertexVelocities[i] += pointToVertex.normalized * velocity;
        //calculem la distancia a partir de la velcoitat = d = v*t;
        displacedVertices[i] += attenuatedForce * Time.deltaTime * Time.deltaTime;
        //resetejem les velocitats. aixi només funcionara mentre apretem
        vertexVelocities[i] = new Vector3(0, 0, 0);
        
    }
}
