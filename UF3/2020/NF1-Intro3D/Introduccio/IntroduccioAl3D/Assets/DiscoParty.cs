﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscoParty : MonoBehaviour
{
    // Start is called before the first frame update
    Light llum; 
    void Start()
    {
        llum = this.GetComponent<Light>();


        StartCoroutine(red());
        StartCoroutine(blue());
        StartCoroutine(green());
    }

    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator red()
    {
        while (true)
        {
            float r = Random.Range(0, 255);
            llum.color = new Color(r, llum.color.g, llum.color.b);
            yield return new WaitForSeconds(2);
        }
    }

    IEnumerator blue()
    {

        while (true)
        {
            float b = Random.Range(0, 255);
            llum.color = new Color(llum.color.r, llum.color.g, b);
            yield return new WaitForSeconds(2.5f);
        }
    }

    IEnumerator green()
    {
        while (true)
        {
            float g = Random.Range(0, 255);
            llum.color = new Color(llum.color.r, g, llum.color.b);
            yield return new WaitForSeconds(1.5f);
        }
    }
}
