using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycastea : MonoBehaviour
{
    //distancia maxima del raycast
    public float rang;
    public int moment;
    public LayerMask mask;
    public GameObject explosion;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R)){
            FesmeUnRaycast();
        }
    }

    private void FesmeUnRaycast()
    {
        //guarda la informaci� sobre la col�lisio amb la que ha xocat el raycast
        RaycastHit hit;

        //Physics.RaycastAll
        Debug.DrawRay(this.transform.position, this.transform.right*10, Color.magenta,2f);
        if (Physics.Raycast(this.transform.position,this.transform.right,out hit, rang, mask))
        {
            GameObject newExplosion = Instantiate(explosion);
            newExplosion.transform.position = hit.point;
            Destroy(newExplosion, 1f);

            Debug.DrawLine(this.transform.position, hit.point, Color.cyan, 2f);
            print(hit.transform.name);
            hit.rigidbody.AddForceAtPosition(-hit.normal * moment, hit.point);
            
        }
        else
        {
            print("potat");
        }
    }
}
