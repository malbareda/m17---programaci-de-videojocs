﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Camera mainCamera;
    public GameObject joan;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //movem la camera

        
        mainCamera.transform.position = new Vector3(
            //en la coordenada X, a on esta joan pero una mica mes enrera (forward negat)
            //lerp - interpolacio lineal. li passes posicio inicial, posicio final, temps
            //posicio inicial, posicio de la camara. posicio final a on volem anar, un temps (anar provant amb Time.deltaTime*algo)
            Mathf.Lerp(mainCamera.transform.position.x,joan.transform.position.x - joan.transform.forward.x * 3, Time.deltaTime*2 ),
            //en la coordenada Y, a on esta joan, pero una mica mes amunt
            Mathf.Lerp(mainCamera.transform.position.y,joan.transform.position.y + 3, Time.deltaTime*2 ),
            //en la coordenada Z, igual que X
            Mathf.Lerp(mainCamera.transform.position.z, joan.transform.position.z - joan.transform.forward.z * 3, Time.deltaTime * 2)
            );
            
        //mainCamera.transform.position = (joan.transform.position - joan.transform.forward * 3)+Vector3.up*3;
        //rota la camera per a que el seu vector forward apunti cap al punt donat
        Vector3 joanAmunt = new Vector3(joan.transform.position.x, joan.transform.position.y+2, joan.transform.position.z);
        mainCamera.transform.LookAt(joanAmunt);
    }
}
