﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveplat : MonoBehaviour
{
    public Vector3 ini;
    public Vector3 end;
    public int spd;
    public Vector3 platformMove;

 

    // Update is called once per frame
    void Update()
    {
        float step = spd * Time.deltaTime;
        Vector3 oldposition = this.transform.position;
        this.transform.position = Vector3.MoveTowards(this.transform.position, end, step);
        platformMove = this.transform.position - oldposition;
        if(this.transform.position == end)
        {
            Vector3 tmp;
            tmp = ini;
            ini = end;
            end = tmp;
        }
    }
}
