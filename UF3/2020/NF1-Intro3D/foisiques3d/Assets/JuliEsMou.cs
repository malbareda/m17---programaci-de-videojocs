﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JuliEsMou : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.localScale = new Vector3(1, 2, 1);
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(transform.localScale.x + 0.01f, 2, 1);

        if(Input.GetKey("w")){
            Moute("w");
        }else if (Input.GetKey("s"))
        {
            Moute("s");
        }
            
    }

    private void Moute(string s)
    {
        if (s == "w")
        {
            transform.Translate(this.transform.forward);
        }else if (s == "s")
        {
            transform.Translate(-this.transform.forward);
        }
    }
}
