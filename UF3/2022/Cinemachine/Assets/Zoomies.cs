using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoomies : MonoBehaviour
{
    public Transform c;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(Mathf.Abs((this.transform.position-c.transform.position).magnitude) > 1  && this.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView>10f)
        {
            this.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView-=0.05f;
        }
        else
        {
            this.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView+=0.05f;
        }
    }
}
