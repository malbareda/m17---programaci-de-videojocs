using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camaraEspejo : MonoBehaviour
{
    // Start is called before the first frame update
    public Camera playerCam;
    private Vector3 myNormal;
    void Awake()
    {
        myNormal = this.transform.forward;
    }

    // Update is called once per frame
    void Update()
    {
        
        
        this.transform.forward = Vector3.Reflect(playerCam.transform.forward, myNormal);
    }
}
