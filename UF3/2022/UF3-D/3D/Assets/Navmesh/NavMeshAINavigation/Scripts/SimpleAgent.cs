using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SimpleAgent : MonoBehaviour
{
    public Transform goal;

    void Start()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(goal.position);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            NavMeshAgent agent = GetComponent<NavMeshAgent>();
            agent.SetDestination(goal.position);

        }
    }
}