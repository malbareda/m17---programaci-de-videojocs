using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour
{
    [SerializeField]
    private Vector3 m_RotationAxis;

    [SerializeField]
    private float m_RotationSpeed;

    void Update()
    {
        transform.Rotate(m_RotationAxis * m_RotationSpeed * Time.deltaTime);
    }
}
