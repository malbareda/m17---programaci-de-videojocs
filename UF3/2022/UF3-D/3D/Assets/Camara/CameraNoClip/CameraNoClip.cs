using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraNoClip : MonoBehaviour
{
    public Transform target;
    private Vector3 targetPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        targetPos = target.position;
        
        RaycastHit hit;
        if(Physics.Linecast(target.parent.position, target.position, out hit))
        {
           targetPos = hit.point;
            print(hit.distance);
            if(hit.distance < 1.5f)
            {
                //target.parent.GetComponent<MeshRenderer>().enabled = false;
                target.parent.GetComponent<MeshRenderer>().sharedMaterial.color = new Color(1, 1, 1, 0.4f);
            }
            else
            {
                //target.parent.GetComponent<MeshRenderer>().enabled = true;
                target.parent.GetComponent<MeshRenderer>().sharedMaterial.color = new Color(1, 1, 1, 1);
            }

        }
        else
        {
            //target.parent.GetComponent<MeshRenderer>().enabled = true;
            target.parent.GetComponent<MeshRenderer>().sharedMaterial.color = new Color(1, 1, 1, 1);
        }
        CameraLerp(targetPos);
    }

    private void CameraLerp(Vector3 targetPos)
    {
        this.transform.position=Vector3.Lerp(this.transform.position, targetPos, Time.deltaTime);
        this.transform.LookAt(target.parent);

    }
}
