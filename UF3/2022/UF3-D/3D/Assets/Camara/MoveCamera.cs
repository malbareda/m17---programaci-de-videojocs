﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveCamera : MonoBehaviour
{
    public float sensitivityX = 2F;
    public float sensitivityY = 2F;
    public float minimumX = -360F;
    public float maximumX = 360F;
    public float minimumY = -90F;
    public float maximumY = 90F;
    float rotationY = -60F;
    public bool fpv = false;
    public Camera FPC;
    Vector3 init;

    // For camera movement
    float CameraPanningSpeed = 10.0f;


    private void Start()
    {
        init = this.transform.position;
    }
    void Update()
    {
        MouseInput();
        KBInput();

    }

    private void KBInput()
    {
        if (Input.GetKeyDown("f"))
        {
            if (fpv)
            {
                fpv = false;
                FPC.rect = new Rect(0.66f, 0.2f, 1, 0.2f);
            }
            else
            {
                print("a");
                fpv = true;
                FPC.rect = new Rect(0, 0, 1, 1);

            }
        }
        else if (Input.GetKeyDown("r"))
        {
            this.transform.position = init;
            this.transform.LookAt(this.transform.parent);
        }
    }

    void MouseInput()
    {


        if (Input.GetMouseButton(0))
        {
        }
        else if (Input.GetMouseButton(1))
        {
            MouseRightClick();
        }
        else if (Input.GetMouseButton(2))
        {
            MouseMiddleButtonClicked();
        }
        else if (Input.GetMouseButtonUp(1))
        {
            ShowAndUnlockCursor();
        }
        else if (Input.GetMouseButtonUp(2))
        {
            ShowAndUnlockCursor();
        }
        else
        {
            MouseWheeling();
        }
    }

    void ShowAndUnlockCursor()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    void HideAndLockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void MouseMiddleButtonClicked()
    {
        HideAndLockCursor();
        Vector3 NewPosition = new Vector3(Input.GetAxis("Mouse X"), 0, Input.GetAxis("Mouse Y"));
        Vector3 pos = transform.position;
        if (NewPosition.x > 0.0f)
        {
            pos += transform.right;
        }
        else if (NewPosition.x < 0.0f)
        {
            pos -= transform.right;
        }
        if (NewPosition.z > 0.0f)
        {
            pos += transform.forward;
        }
        if (NewPosition.z < 0.0f)
        {
            pos -= transform.forward;
        }
        pos.y = transform.position.y;
        transform.position = pos;
    }

    void MouseRightClick()
    {
        HideAndLockCursor();
            //agafo la rotacio actual i li sumo el moviment X del ratoli
            float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

            //agafo la rotacio actual i li sumo el moviment X del ratoli
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        
        
    }

    void MouseWheeling()
    {
        Vector3 pos = transform.position;
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            pos = pos - transform.forward;
            transform.position = pos;
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            pos = pos + transform.forward;
            transform.position = pos;
        }
    }
}
