﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Escopeta : MonoBehaviour
{
    public Camera FPCamera;
    public GameObject explosion;
    public GameObject ragdoll;
    float rango = 20f;
    float momento = 250f;
    private bool charged = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (charged)
            {
                Shoot();
                StartCoroutine(Recharge());
            }
        }
    }

    IEnumerator Recharge()
    {
        yield return new WaitForSeconds(3);
        charged = true;
    }

    private void Shoot()
    {

        charged = false;


        for(int i=0; i < 4; i++)
        {
            RaycastHit hit;
            Vector3 newForward = new Vector3(FPCamera.transform.forward.x + Random.Range(-0.05f, +0.05f), FPCamera.transform.forward.y + Random.Range(-0.05f, +0.05f), FPCamera.transform.forward.z + Random.Range(-0.05f, +0.05f));

            if (Physics.Raycast(FPCamera.transform.position, newForward, out hit, rango))

            {
                float percent =  rango/ hit.distance;

                print("disparo "+i+" a "+hit.point+" a "+hit.distance+" "+percent);
                Debug.DrawLine(FPCamera.transform.position, hit.point, Color.red, 1f);
                GameObject newExplosion = Instantiate(explosion);
                newExplosion.transform.position = hit.point;
                Destroy(newExplosion, 1f);
                if (hit.transform.tag == "ElsQueCantes")
                {
                    hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momento*percent, hit.point);
                }
                if(hit.transform.tag == "Matable")
                {
                    //desactivem el personatge
                    hit.transform.gameObject.SetActive(false);
                    //activem el ragdoll
                    hit.transform.parent.GetChild(0).gameObject.SetActive(true);
                    hit.transform.parent.GetChild(0).position = hit.transform.position;
                    hit.transform.parent.GetChild(0).GetChild(0).GetChild(1).gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * 2000, hit.point) ;
                }
                if(hit.transform.tag == "RagdollMuerto")
                {
                    hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * 2000, hit.point);
                }

            }
        }
        

    }
}
