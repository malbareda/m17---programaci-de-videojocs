using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnaPelotaQueBotaYBota : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody>().velocity = this.transform.forward * 2;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        print("rebote");
        this.transform.forward = Vector3.Reflect(this.transform.forward, collision.contacts[0].normal);
        this.GetComponent<Rigidbody>().velocity = this.transform.forward * 2;

    }
}
