﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : MonoBehaviour
{
    public Camera FPCamera;
    public GameObject explosion;
    public GameObject ragdoll;
    float rango = 50f;
    float momento = 2500f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        RaycastHit hit;

        

        if(Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, rango))
        
        {

            print(hit.transform.name);
            Debug.DrawLine(FPCamera.transform.position, hit.point, Color.red, 1f);
            GameObject newExplosion = Instantiate(explosion);
            newExplosion.transform.position = hit.point;
            Destroy(newExplosion, 1f);
            if(hit.transform.tag == "ElsQueCantes")
            {
                hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal*momento, hit.point);
            }

            if (hit.transform.tag == "peakComedia")
            {

                hit.transform.gameObject.SetActive(false);
                Instantiate(ragdoll, hit.transform.position, hit.transform.rotation);

            }
            if (hit.transform.tag == "CUBISMO")
            {

                hit.transform.GetComponent<CharacterJoint>().breakForce=0;
                hit.transform.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momento, hit.point);


            }
        }
       
    }
}
