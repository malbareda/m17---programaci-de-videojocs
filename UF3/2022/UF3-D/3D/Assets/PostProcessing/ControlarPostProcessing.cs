using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ControlarPostProcessing : MonoBehaviour
{
    private Volume volume;
    private bool desaturado = false;
    // Start is called before the first frame update
    void Start()
    {
        volume = this.GetComponent<Volume>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            
            StartCoroutine(memoriasDeRoberto());
        }
    }

    public IEnumerator memoriasDeRoberto()
    {

        ColorAdjustments ca;
        volume.profile.TryGet<ColorAdjustments>(out ca);
        print("a?");
        if (!desaturado)
        {
            while (ca.saturation.value > -100)
            {

                ca.saturation.value -= 1;
                yield return new WaitForSeconds(0.05f);

            }
            desaturado = true;
        }
        else
        {
            while (ca.saturation.value < 0)
            {
                ca.saturation.value += 1;
                yield return new WaitForSeconds(0.05f);
            }
            desaturado = false;
        }
    
    }
}
