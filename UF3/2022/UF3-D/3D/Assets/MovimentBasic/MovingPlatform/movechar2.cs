﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movechar2 : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private float playerSpeed = 2.0f;
    private float jumpHeight = 1.0f;
    private float gravityValue = -9.81f;
    public GameObject myPlatform = null;

    private void Start()
    {
        controller = gameObject.GetComponent<CharacterController>();
    }

    void Update()
    {
        groundedPlayer = controller.isGrounded;

        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector3 move = new Vector3(Input.GetAxis("Horizontal")*2, 0, Input.GetAxis("Vertical")*2);

        

        controller.Move(move * Time.deltaTime * playerSpeed);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }


        //print("A" + this.transform.position.z);
        if (myPlatform != null)
        {
            print("B" + myPlatform.GetComponent<moveplat>().platformMove.z);
            this.transform.position += myPlatform.GetComponent<moveplat>().platformMove;
        }


        //print("C" + this.transform.position.z);
        // Changes the height position of the player..
        if (Input.GetButtonDown("Jump") && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            controller.Move(playerVelocity * Time.deltaTime);
        }
        else if (!groundedPlayer)
        {
            playerVelocity.y += gravityValue * Time.deltaTime;
            controller.Move(playerVelocity * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        print("in");
        if (collision.transform.tag == "mplat")
        {
            myPlatform = collision.transform.parent.gameObject;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        print("out");
        if (collision.transform.tag == "mplat")
        {
            myPlatform = null;
        }
    }
}