﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public float currentHealth = 100;
    public float redHealth = 100;
    public float maxHealth = 100;

    public Transform vidaVerde;
    public Transform vidaRoja;
    public Text infoText;

    void Start()
    {
        //looseHealth(20);
        Character c = this.GetComponentInParent<Character>();
        if (c == null)
        {
            Debug.Log("No tiene vida este personaje. DESTRUYENDO OBJETO DE VIDA."); ;
            Destroy(this.gameObject);
        }
        currentHealth = c.vida;
        redHealth = c.vida;
        maxHealth = c.max_vida;
    }
    void Update()
    {
        Character c = this.GetComponentInParent<Character>();
        //Actualizar vida máxima
        if (maxHealth != c.vida)
            maxHealth = c.max_vida;
        //Actualizar vida actual 
        //detecta mas vida
        if (currentHealth > c.vida)
            looseHealth(currentHealth - c.vida);
        //detecta mas vida
        else if (currentHealth < c.vida)
        {
            currentHealth = c.vida;
            redHealth = c.vida;

        }
        vidaVerde.localScale = new Vector2(currentHealth / maxHealth, 1);
        vidaRoja.localScale = new Vector2(redHealth / maxHealth, 1);
    }

    public void looseHealth(float amount)
    {
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            currentHealth = 0;
        }
        StartCoroutine(newHealth(100));
    }
    private IEnumerator newHealth(int velocity)
    {
        while (redHealth != currentHealth)
        {
            yield return new WaitForSeconds(0.001f);
            float moveAmount = velocity * Time.deltaTime;
            moveAmount = Mathf.Clamp(moveAmount, 0f, redHealth - currentHealth);
            redHealth -= moveAmount;
        }
    }
    

}
