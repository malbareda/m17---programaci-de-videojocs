﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTower : MonoBehaviour
{
    private Ally torre;
    private bool selected = false;
    private SpriteRenderer contour = null;

    public GameObject torreMejorada;

    // Start is called before the first frame update
    void Start()
    {
        torre = this.GetComponentInParent<Ally>();
        contour = this.GetComponent<SpriteRenderer>();
        contour.enabled=false;
    }
    private void FixedUpdate()
    {

    }

    void Mejorar()
    {
        torre.max_vida *= 2;
        torre.vida = torre.max_vida;

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        MouseFollower mf = collision.gameObject.GetComponent<MouseFollower>();
        if (mf!=null && mf.selected==null)
        {
            selected = true;
            mf.selected = this.GetComponentInParent<Ally>();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        MouseFollower mf = collision.gameObject.GetComponent<MouseFollower>();
        if (mf != null)
        {
            selected = false;
            mf.selected = null;
        }
    }
    void LateUpdate()
    {
        if (contour != null)
        {
            if (selected)
            {
                Ally torre = GetComponentInParent<Ally>();
                //contour.SetActive(true);
                contour.enabled = true;
                //Interacturar si esta seleccionado
                if (Input.GetMouseButtonDown(0))
                {
                    //accion al vender
                    if (MenuController.accionv)
                    {
                        MenuController.accionv = false;
                        //MenuController.accionv = false;
                        MainController.dineroo += torre.venta;
                        Destroy(this.transform.parent.gameObject);
                    }
                    //accion al mejorar
                    else if (MenuController.accionm)
                    {
                        //MenuController.accionm = false;
                        if (MainController.dineroo >= torre.precioMejora)
                        {
                            //this.Mejorar();
                            MainController.dineroo -= torre.precioMejora;
                            GameObject objectMejorado = Instantiate(torreMejorada);
                            objectMejorado.transform.position = torre.gameObject.transform.position;
                            MenuController.accionm = false;
                            Destroy(torre.gameObject);
                        }
                        MenuController.accionm = false;
                    }
                }
            }
            else
            {
                //contour.SetActive(false);
                contour.enabled = false;
            }
        }
    }

    //funciona mejor con el trigger, así que hemos eliminado todas las funciones de OnMouseEnter, etc.
}
