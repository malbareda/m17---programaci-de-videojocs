﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class MouseManager : MonoBehaviour
{

    private Tilemap tilemap;

    public Transform target;

    void Start()
    {
        tilemap = GameObject.Find("Suelo").GetComponent<Tilemap>();
    }

    void Update()
    {

        if (Input.GetMouseButton(1))
        {
            Vector2 inputmouse = Input.mousePosition;
            //Debug.Log("posicio pixel " + inputmouse);
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(inputmouse);
           // Debug.Log("posicio camera " + mousePos);
            Vector3Int gridPos = tilemap.WorldToCell(mousePos);

            if (tilemap.HasTile(gridPos))
            {
            //    Debug.Log("Casella " + gridPos);
            }
            target.transform.position = mousePos;
        }
        

    }


}