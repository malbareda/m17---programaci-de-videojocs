﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//Este script lo único que hace es cambiar la canción de fondo cuando pulsamos a la N o la B
public class FondoMusicController : MonoBehaviour
{
    //La playlist es donde se guardan las canciones que hemos puesto para este juego
    public AudioClip[] playList;
    private int pos;
    private new AudioSource audio;
    public Text nom;


    public Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Fondo());
        pos = 0;

        audio = GetComponent<AudioSource>();
        ponerCancion();
        nom.text = "";
        StartCoroutine(nombreCancion());


    }
    //Muestra por pantalla el nombre de la canción actual
    private IEnumerator nombreCancion()
    {
        Debug.Log(playList[pos].name);
        nom.text = playList[pos].name;
        yield return new WaitForSecondsRealtime(3f);
        nom.text = "";
    }
    //Cambia el fondo de color aleatorio para que sea una discoparty
    private IEnumerator Fondo()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        cam.backgroundColor = new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);
        StartCoroutine(Fondo());
    }

    // Update is called once per frame

    private void Update()
    {
        if (!audio.isPlaying)
            nextSong();
        inputSong();
        //inputCancion();
    }
    //Si pulsas a la N, sale la siguiente canción de la lista, si le das a la b, la anterior
    private void inputSong()
    {
        if (Input.GetKeyDown("n"))
        {
            nextSong();
        }
        else if (Input.GetKeyDown("b"))
        {
            backSong();
        }
    }
    private void nextSong()
    {
        pos++;
        if (pos >= playList.Length)
            pos = 0;
        ponerCancion();
    }
    private void backSong()
    {
        pos--;
        if (pos < 0)
            pos = playList.Length  - 1;
        ponerCancion();
    }

    /// funciones de Albert
    private void inputCancion()
    {
        if (Input.GetKeyDown("n"))
        {
            if (pos + 1 < playList.Length)
            {

                pos++;
                ponerCancion();
            }
            else
            {

                pos = 0;
                ponerCancion();
            }
        }
        else if (Input.GetKeyDown("b"))
        {
            Debug.Log(pos - 1);
            if (pos > 0)
            {
                pos--;

                ponerCancion();
            }
            else
            {

                pos = playList.Length - 1;
                ponerCancion();
            }
        }
    }

    private void ponerCancion()
    {
        audio.clip = playList[pos];
        audio.Play();
        StopAllCoroutines();
        StartCoroutine(Fondo());
        StartCoroutine(nombreCancion());

    }
}
