﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Esta clase solo sirve para mover la camara, la cual sigue al jugador donde quiera que vaya
 */
public class CameraMovement : MonoBehaviour
{
    //La velocidad a la cual se mueve la cámara
    int vel = 5;
    //Boolean para saber si la tenemos centrada o no
    bool centrado = true;
    //El jugador al que seguirá
    GameObject player = null;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    //Cuando el jugador le da a la Y cambiamos el funcionamiento de la cámara, en vez de seguir al jugador, la mueves tú con WASD
    void Update()
    {
        if (Input.GetKeyDown("y"))
            centrado = !centrado;
        if (centrado)
        {
            //Si no está centrado, la puedes mover, si está centrada, seguirá al jugador siempre
            if (player == null)
                centrado = false;
            else
                this.gameObject.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);
        }
        else
            inputMove();
        inputScroll();
    }

    private void inputMove()
    {
        float moveX = 0;
        float moveY = 0;
        if (Input.GetKey("d")) moveX += vel;
        if (Input.GetKey("a")) moveX -= vel;
        if (Input.GetKey("w")) moveY += vel;
        if (Input.GetKey("s")) moveY -= vel;
        this.GetComponent<Rigidbody2D>().velocity = (new Vector2(moveX, moveY));
    }
    private void inputScroll()
    {
        Camera camera = this.GetComponent<Camera>();
        Vector2 scroll = Input.mouseScrollDelta;
        if (scroll.y == 1)//zoom in
        {
            if (camera.orthographicSize>3)
                camera.orthographicSize -= 0.5f;
        }
        else if (scroll.y == -1)//zoom out
        {
            if (camera.orthographicSize < 7)
                camera.orthographicSize += 0.5f;
        }
    }

}
