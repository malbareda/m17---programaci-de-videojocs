﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//EN este clase englobamos todos los enemigos que debemos derrotar
public class Enemy : Character
{
    //Recompensa hace que cuando muere nos sume esa cantidad de dinero
    public int recompensa = 10;
    //private bool curar = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    //Cuando un enemigo muere (muere cuando su vida llega a 0) aparte de destruir su gameobject, nos dará dinero que podremos gastar en comprar o mejorar torres.
    override public void die()
    {
        //Debug.Log("---------------------------------------------");
        //sumar zloty a la partida segun recompensa
        MainController.dineroo+=recompensa;
        Destroy(this.gameObject);
    }

    /*
    public void OnEnable()
    {
        HealControler.OnDamanged2 += HandleOnDamaged;
    }
    public void OnDisable()
    {
        HealControler.OnDamanged2 -= HandleOnDamaged;
    }
    public void HandleOnDamaged(int vid)
    {
       if (curar)
        {
            if (vida > 0)
            {
                if (vida + vid <= 150) { this.vida += vid; }
                else
                {
                    Debug.Log(this.gameObject.name + "CURAAAAAAAAAAAAAAAAA");
                    vida = 150;
                  StartCoroutine(Curar());
                }

            }
        }

    }

    private IEnumerator Curar()
    {

        curar = false;

        yield return new WaitForSecondsRealtime(1f);
        curar = true;
    }*/
}
