﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAttack : MonoBehaviour
{

    //List<GameObject> cola = new List<GameObject>();
    public List<Enemy> cola = new List<Enemy>();

    public Enemy objetivo = null;

    public Proyectil proyectilPrefab;
    //public Proyectil actualProyectil = null;
    bool can_shoot = true;
    public float damage = 1.0f;
    public float cool_down = 1.0f;


    public Transform shootTransform;

    public bool attacking = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (cola.Count > 0)
        {
            foreach (Enemy item in cola)
            {
                objetivo = item;
                break;
            }
        }
        else
        {
            objetivo = null;
        }
        if(objetivo!=null && can_shoot)
        {
            Proyectil actualProyectil = Instantiate(proyectilPrefab);
            actualProyectil.name = "proy";
            actualProyectil.transform.position = shootTransform.position;
            actualProyectil.daño = this.damage;
            actualProyectil.target = objetivo;
            StartCoroutine(coolDown());
            //attacking = true;
        }
        /*
        if (attacking && actualProyectil == null)
            attacking = false;*/
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy e = collision.gameObject.GetComponent<Enemy>();
        if (e != null)
        {
            //Debug.Log("Entra" + e.name);
            cola.Add(e);
        }

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Enemy e = collision.gameObject.GetComponent<Enemy>();
        if (e != null)
        {
            //Debug.Log("Sale" + e.name);
            cola.Remove(e);
        }
    }

    public IEnumerator coolDown()
    {
        can_shoot = false;
        yield return new WaitForSecondsRealtime(cool_down);
        can_shoot = true;

    }
}
