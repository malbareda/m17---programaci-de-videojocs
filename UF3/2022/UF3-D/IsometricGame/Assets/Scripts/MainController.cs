﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MainController : MonoBehaviour
{
    public GameObject playerPrefab;
    //dinero referencia al texto del canvas de arriba a la derecha que dice cuanto dinero tienes. El dinero sirve para comprar y mejorar torres, ganas dinero por tiempo o por matar enemigos.
    public UnityEngine.UI.Text dinero;
    //Es la variable en int del dinero que luego pondremos en el texto
    public static int dineroo = 20;
    // Este timer sirve para luego en la funcion de updateDinero sumar el dinero si llega a la cantidad de tiempo puesta en cdinero.
    private float timer = 0.0f;
    // Es la cantidad de tiempo necesaria para poder sumar un Zloty (dinero).
    private float cdinero = 1.0f;
    // El texto que sale en el hud indicándonos en que oleada estamos
    public UnityEngine.UI.Text oleada;
    // La variable en número de la oleada
    private int oleadaa;
    // La lista de enemigos que hay en el juego actualmente
    public GameObject[] ricardos;
    // Es el texto que sale por pantalla y nos muestra cuanto tiempo nos queda de oleada o descanso
    public UnityEngine.UI.Text crono;
    // El valor númerico por pantalla del tiempo
    private float cronoo;
    // Este boolean sirve para indicar si estamos en tiempo de oleada o de descanso si es true, es oleada, si es false es descanso
    private bool descontartiempo = true;

    // El awake nos crea el jugador del prefab que tenemos de él
    private void Awake()
    {
        //GameObject player = Instantiate(playerPrefab);
        //player.name = "FullPlayer";
    }
    // Start is called before the first frame update
    void Start()
    {
        // Ponemos los valores base, la oleada 1, 50 zloty y 15 segundos de oleada y generamos unos enemigos
        oleadaa = 1;
        dineroo = 50;
        cronoo = 15f;
        GameObject.Find("Collisions").GetComponent<Tilemap>().color = new Color(255, 0, 0, 0);
        GenerarRicardos();
    }

    // Update is called once per frame
    void Update()
    {
        // En el update únicamente tenemos que tener en cuenta de estar actualizando constantemente el dinero y el tiempo de oleada/descanso
        updateDinero();
        updateTiempo();

    }

    private void updateDinero()
    {
        // Cambiamos el texto a Zloty y el valor número del dinero
        dinero.text = ("Zloty: " + dineroo);
        // Si el timer, supera a cdinero (el tiempo está acordado a 1 segundo), suma dos al valor númerico del dinero
        if (timer > cdinero)
        {
            timer = 0.0f;
            dineroo+=2+(oleadaa/2);//ademas cada dos oleadas se añadira 1 más al valor sumado
        }
        // Si no lo hace, suma el tiempo que ha pasado desde la última vez que hizo update el juego.
        else
        {
            timer += Time.deltaTime;
        }

    }
    //Esta funcion hace que los textos por pantalla de oleada y tiempo se actualicen constantemente, además de que cuando empieza la oleada se generen enemigos
    private void updateTiempo()
    {
        System.Random r = new System.Random();
        //Si el booleano está en true significa que estamos en oleda y saldrá en el temporizador de la pantalla avisandonos a la vez de los segundos que quedan
        if (descontartiempo)
        {
            crono.text = ("Tiempo: " + cronoo.ToString("f2"));
        }
        //Si es false, saldrá que estamos en descanso y lo mismo, los segundos que quedan también.
        else
        {
            crono.text = ("Descanso: " + cronoo.ToString("f2"));
        }
        //El texto por pantalla de la oleada está actualizandose todo el rato
        oleada.text = ("Oleada: " + oleadaa);
        /*Debug.Log(timer);
          Debug.Log(cronoo);
            Debug.Log(oleadaa);*/
        //SI nos quedamos sin tiempo mirará como está el booleano descontartiempo
        if (cronoo <= 0)
        {
            //Si es true, lo pondrá en false, sumará una oleada al hud y pondrá 10 segundos de nuevo para descanso
           
            if (descontartiempo)
            {
                cronoo = 10f;
                oleadaa++;
                
                descontartiempo = false;
            }
            //Si es false, pondrá a 15 los segundos por pantalla y generará enemigos
            else
            {
                if (oleadaa % 5 == 0)
                {
                    cronoo = 30f;
                    GenerarRicardoBoss();
                    descontartiempo = true;

                }
                else
                {
                    cronoo = 15f;
                    GenerarRicardos();
                    descontartiempo = true;
                }
               
            }
            


        }
        else
        //Si sigue quedando tiempo, lo actualiza y ya.
        {
           
            cronoo -= Time.deltaTime;
            

        }
    }

    private void GenerarRicardoBoss()
    {
        System.Random r = new System.Random();
        GameObject nuevoRicardo = null;
        nuevoRicardo = Instantiate(ricardos[3]);
        nuevoRicardo.transform.SetParent(GameObject.Find("Enemies").transform);

        switch (r.Next(4) + 1)
        {
            case 1:
                nuevoRicardo.transform.position = new Vector2(11, 1);
                break;
            case 2:
                nuevoRicardo.transform.position = new Vector2(0, -6);
                break;
            case 3:
                nuevoRicardo.transform.position = new Vector2(-11, 1);
                break;
            case 4:
                nuevoRicardo.transform.position = new Vector2(0, 6);
                break;

        }
        
    }

    //Con esta función generamos los enemigos cada ronda
    private void GenerarRicardos()
    {   //Lo que hace es que recorre un blucle de la variable ricarditos que luego se multiplica por la oleada en la que estamos, así aumentamos la dificultad cada ronda
        System.Random r = new System.Random();
        int ricarditos = 3;
        GameObject nuevoRicardo = null;
        //En un bucle donde escoge una opción del switch aleatoria donde lo cogerá de una array donde tenemos nuestros diferentes enemigos
        for(int i=0; i<ricarditos* oleadaa; i++)
        {
            //nuevoRicardo = new GameObject();
            switch (r.Next(3)+1)
            { 
                case 1:
                    nuevoRicardo = Instantiate(ricardos[0]);
                    break;
                case 2:
                    nuevoRicardo = Instantiate(ricardos[1]);
                    break;
                case 3:
                    nuevoRicardo = Instantiate(ricardos[2]);
                    break;

            }
            //Luego, lo pondrá en una de nuestras 4 ubicaciones predefinidas
            nuevoRicardo.transform.SetParent(GameObject.Find("Enemies").transform);
            switch (r.Next(4) + 1)
            {
                case 1:
                    nuevoRicardo.transform.position = new Vector2(11, 1);
                    break;
                case 2:
                    nuevoRicardo.transform.position = new Vector2(0, -6);
                    break;
                case 3:
                    nuevoRicardo.transform.position = new Vector2(-11, 1);
                    break;
                case 4:
                    nuevoRicardo.transform.position = new Vector2(0, 6);
                    break;

            }
        }

        
    }
}
