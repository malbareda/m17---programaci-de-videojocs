﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsometricPlayerMovementController : MonoBehaviour
{
    //Define tanto el movimiento como la direccion a la que se mueve
    public float movementSpeed = 1f;
    IsometricCharacterRenderer isoRenderer;

    Rigidbody2D rbody;

    Vector2 previousPos;

    int frame = 0;

    private void Awake()
    {
        rbody = GetComponent<Rigidbody2D>();
        isoRenderer = GetComponentInChildren<IsometricCharacterRenderer>();
        previousPos = rbody.position;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        frame++;
        if (previousPos == rbody.position)
        {
            isoRenderer.SetDirection(Vector2.zero);
        }
        else if (frame >= 5)
        {
            Vector2 currentPos = rbody.position;
            Vector2 movement = (currentPos - previousPos) * movementSpeed;
            isoRenderer.SetDirection(movement);
            //Debug.Log(previousPos + " " + currentPos);
            previousPos = currentPos;
            frame = 0;
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
            movementSpeed *= 2;
        if (Input.GetKeyUp(KeyCode.LeftShift))
            movementSpeed /= 2;

    }

    void moveInput()//mover personaje por teclado
    {
        Vector2 currentPos = rbody.position;
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        Vector2 inputVector = new Vector2(horizontalInput, verticalInput);
        inputVector = Vector2.ClampMagnitude(inputVector, 1);
        Vector2 movement = inputVector * movementSpeed;
        Vector2 newPos = currentPos + movement * Time.fixedDeltaTime;
        isoRenderer.SetDirection(movement);
        rbody.MovePosition(newPos);
    }
}
