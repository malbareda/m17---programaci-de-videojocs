﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRight : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.AddComponent<Rigidbody2D>();
        this.gameObject.GetComponent<Rigidbody2D>().gravityScale=0;
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(2,0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
