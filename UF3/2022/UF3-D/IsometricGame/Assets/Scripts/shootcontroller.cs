﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootcontroller : MonoBehaviour
{
    public GameObject puntero;
    public GameObject enemigo;
    private float x;
    private float y;
    // Start is called before the first frame update
    void Start()
    {
        puntero = this.gameObject.transform.parent.gameObject;
        enemigo = GameObject.Find("Player");

        Debug.Log(enemigo.transform.position+" "+puntero.transform.position);
        
        this.GetComponent<Rigidbody2D>().velocity = new Vector2((puntero.transform.position.x - enemigo.transform.position.x) * 10, (puntero.transform.position.y - enemigo.transform.position.y) * 10);
        x = enemigo.transform.position.x - this.transform.position.x;
        y = enemigo.transform.position.y - this.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direccion = enemigo.transform.position - transform.position;
        float angulo = Mathf.Atan2(direccion.y, direccion.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angulo, Vector3.forward);
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(direccion.x*2,direccion.y*2);
        //Debug.Log(this.GetComponent<Rigidbody2D>().velocity);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.gameObject.name == "PlayerCollider")
        {
            Debug.Log("hola");
           // Destroy(collision.gameObject.transform.parent.gameObject);
            Destroy(this.gameObject);
        }
    }
}
