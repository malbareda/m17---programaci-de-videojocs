﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealControler : MonoBehaviour
{
    public delegate void _OnDamanged2(int Vida);
    public static event _OnDamanged2 OnDamanged2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Ricardo" || collision.gameObject.name == "Ricardo2")
        {
            OnDamanged2?.Invoke(40);
        }
    }
}
