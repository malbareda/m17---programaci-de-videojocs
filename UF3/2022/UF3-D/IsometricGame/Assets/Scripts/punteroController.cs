﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class punteroController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject puntero;
    public GameObject shot;
    public GameObject disparos;
    private bool dispara;
    void Start()
    {
        dispara = true;
        disparos = Instantiate(new GameObject());

    }

    // Update is called once per frame
    void Update()
    {
        if (dispara && disparos!=null) { 
            GameObject shoot=Instantiate(shot);

            shoot.transform.SetParent(disparos.transform);
            shoot.transform.position = new Vector3(this.transform.position.x, this.transform.position.y , this.transform.position.z);
            StartCoroutine(cooldown());
        }

    }

    private IEnumerator cooldown()
    {
        dispara = false;
        yield return new WaitForSecondsRealtime(3f);
        dispara = true;
    }
}
