﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo2Controller : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject player;
    public float mover;
    public float tiempo = 0.0f;
    public GameObject puntero;
    Pathfinding.AILerp lerp;
    void Start()
    {
        player = GameObject.Find("Player");
        mover = 0.005f;
        this.GetComponent<Pathfinding.AIDestinationSetter>().target = player.transform;
        lerp = this.GetComponent<Pathfinding.AILerp>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            lerp.canSearch = false;
        }

    }
    void siguePersonaje()
    {
        /*
        tiempo += Time.deltaTime;

        if (player.transform.position.x > this.transform.position.x)
        {
            this.transform.position = new Vector3(this.transform.position.x + mover, this.transform.position.y, this.transform.position.z);
        }
        else
        {
            this.transform.position = new Vector3(this.transform.position.x - mover, this.transform.position.y, this.transform.position.z);
        }

        if (player.transform.position.y > this.transform.position.y)
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + mover, this.transform.position.z);
        }
        else
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - mover, this.transform.position.z);
        }
        */

        if (player.transform.position.x > this.transform.position.x)
        {
            this.transform.position = new Vector3(this.transform.position.x + mover, this.transform.position.y, this.transform.position.z);
        }
        else
        {
            this.transform.position = new Vector3(this.transform.position.x - mover, this.transform.position.y, this.transform.position.z);
        }

        if (player.transform.position.y > this.transform.position.y)
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + mover, this.transform.position.z);
        }
        else
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - mover, this.transform.position.z);
        }


        //  this.GetComponent<Rigidbody2D>().velocity = new Vector2(puntero.transform.position.x-this.transform.position.x, puntero.transform.position.y - this.transform.position.y);
        //Debug.Log(puntero.transform.position.x - this.transform.position.x);
    }
}
