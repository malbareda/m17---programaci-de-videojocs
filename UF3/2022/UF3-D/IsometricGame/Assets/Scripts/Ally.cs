﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Esta clase desciende de character, la clase ally define a las torres que tenemos además de nuetro propio persoanje 
public class Ally : Character
{
    //EL valor de precio y de venta sirven para cuando se mejora la torre se aumenta su precio haciendo así que cuando más la mejores más caro sea, también cambia el de venta que nos darán el dinero cuando la queramos vender
    public int precio = 50;
    public int precioMejora = 100;
    public int venta = 25;
    //Es el contorno de la torre que podemos ver ingame que será verde para ver que es aliada
    public GameObject contour;
    public bool selected = false;
    // Start is called before the first frame update
    void Start()
    {
        // Si el ally es el jugador, podrá recuperar vida con el tiempo, las torres no podrán.
        if (this.gameObject.name.Equals("Player"))
        {
            StartCoroutine(regenerarVida());
        }
        //if(contour!=null)
            //contour.SetActive(false);
    }
    //Cada medio segundo el personaje se cura 10 de vida
    private IEnumerator regenerarVida()
    {
        float heal = 10;
        while (true)
        {
            yield return new WaitForSecondsRealtime(0.5f);
            if (vida != max_vida)
            {
                if ((vida + heal) > max_vida)
                {
                    vida = max_vida;
                }
                else
                {
                    vida += heal;
                }
            }
        }
    }
    // Cuando se mejora una torre se duplica su vida
    public void Mejorar()
    {
        max_vida *= 2;
        vida = max_vida;

    }
    /*
    private void OnMouseEnter()
    {
        if (!this.gameObject.name.Equals("Player"))
        {
            Debug.Log("preparado------------------------------");
            contour.enabled = true;
        }
    }
    
    private void OnMouseExit()
    {
        if (!this.gameObject.name.Equals("Player"))
        {
            Debug.Log("SALE------------------------------");
            contour.enabled = false;
        }
    }*/
    /*
    private void FixedUpdate()
    {
        selected = false;
    }*/
    /*
    void OnMouseEnter()
    {
        selected = true;
        //Debug.Log("Sobre torre");
    }*/
    /*
    void OnMouseExit()
    {
        Debug.Log("SALE------------------------------");
        selected = false;
    }*/

    /*
        
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Mouse"))
            selected = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Mouse"))
            selected = false;
    }
    void LateUpdate()
    {
        if (contour != null)
        {
            if (selected)
            {
                contour.SetActive(true);
                //Interacturar si esta seleccionado
                if (Input.GetMouseButtonDown(0))
                {
                    //accion al vender
                    if (MenuController.accionv)
                    {
                        //MenuController.accionv = false;
                        MainController.dineroo += this.venta;
                        Destroy(this.gameObject);
                    }
                    //accion al mejorar
                    else if (MenuController.accionm)
                    {
                        //MenuController.accionm = false;
                        if (MainController.dineroo >= (this.precio * 2))
                        {
                            this.Mejorar();
                            MainController.dineroo -= (this.precio * 2);
                            this.precio *= 2;
                            this.venta = this.precio / 2;
                        }
                    }
                    MenuController.cancelarAccion();
                }
            }
            else
                contour.SetActive(false);
        }
    }
    */

}
