﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
//Este script de vida lo tienen todos los personajes y muestra la barra de vida de cada uno que su vida máxima y restante en valor número y con un representación en forma de barra de vida
public class HealthGuay : MonoBehaviour
{
    //Estos valores es para calcular luego que parte tiene que estar rellena de la barra y que parte es roja porque ha recibido daño en ese momento.
    public float currentHealth = 100;
    public float redHealth = 100;
    public float maxHealth = 100;

    public Image vidaVerde;
    public Image vidaRoja;
    public Text infoText;

    void Start()
    {
        //Buscamos el personaje que el cual la barra de vida está referenciada
        //looseHealth(20);
        Character c = this.GetComponentInParent<Character>();
        //Si se ha borrado (por muerte) se destruirá también su barra de vida
        if (c == null)
        {
            Debug.Log("No tiene vida este personaje. DESTRUYENDO OBJETO DE VIDA."); ;
            Destroy(this.gameObject);
        }
        //Los valores de vida que antes hemos visto, se cambian a los que tenga el personaje referenciada
        currentHealth = c.vida;
        redHealth = c.vida;
        maxHealth = c.max_vida;
    }
    void Update()
    {
        Character c = this.GetComponentInParent<Character>();
        //Actualizar vida máxima
        if (maxHealth != c.vida)
            maxHealth = c.max_vida;
        //Actualizar vida actual 
        //detecta mas vida
        if (currentHealth > c.vida)
            looseHealth(currentHealth - c.vida);
        //detecta mas vida
        else if (currentHealth < c.vida)
            looseHealth(c.vida - currentHealth);
        {
            currentHealth = c.vida;
            //redHealth = c.vida;

        }
        //en caso de estar la barra roja por debajo moverla instantaniamente hasta la verde
        if (redHealth < currentHealth)
            redHealth = currentHealth;
        //cambiar tamaño de las barras
        vidaVerde.fillAmount = currentHealth / maxHealth;
        vidaRoja.fillAmount = redHealth / maxHealth;
        //cambiar texto de informacion
        infoText.text = "" + currentHealth +"/"+ maxHealth;
    }
    //Cuando gana vida, aumenta la vida actual
    public void gainHealth(float amount)
    {
        currentHealth += amount;

    }
    //Cuando pierde vida, aparte de actualizarse la vida, iniciamos una coroutina que hará que la vida perdida se convierta en roja y baje lentamente dando una sensión muy satisfactoria
    public void looseHealth(float amount)
    {
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            currentHealth = 0;
        }
        StopCoroutine("newHealth");
        StartCoroutine(newRedHealth(100));
    }
    //A la coroutina le pasamos el valor de cuanta velocidad tiene que bajar la vida
    private IEnumerator newRedHealth(int velocity)
    {
        //Solo lo hacemos mientras redhealth sea diferente (normalmente mayor) para que disminuya hsta donde está la vida actual
        while (redHealth != currentHealth)
        {
            yield return new WaitForSeconds(0.001f);
            float moveAmount = velocity * Time.deltaTime;
            moveAmount = Mathf.Clamp(moveAmount, 0f, redHealth - currentHealth);
            redHealth -= moveAmount;
        }
    }


}
