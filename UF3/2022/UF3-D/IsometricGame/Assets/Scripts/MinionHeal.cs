﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Este script lo utilizamos en el prefab del enemigo healer (cura a los demás)
public class MinionHeal : MonoBehaviour
{
    //List<GameObject> cola = new List<GameObject>();
    //La lista de enemgigos que tiene alrededor los cuales podrá curar
    public List<Enemy> cola = new List<Enemy>();

    public Proyectil proyectilPrefab;
    //Booleano para ssaber si puede curar o no
    bool can_heal = true;
    //El tiempo entre cura y cura
    public float cool_down = 2.5f;
    //El propio enemigo healer
    private Enemy healer = null;

    // Start is called before the first frame update
    void Start()
    {
        //En el start lo buscamos
        healer = this.GetComponentInParent<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {
        //Lo primero que hacemos es añadir al propio healer en la cola donde se curan los enemigos si está por debajo de su máximo de vida
        if (healer.vida < healer.max_vida && !cola.Contains(healer))
            cola.Add(healer);
        //Remueve de la lista antes de curar a todos los enemigos que tengan la vida máxima o que hayan muerto
        cola.RemoveAll(item => item.vida >= item.max_vida);
        cola.RemoveAll(item => item == null);
        //Si hay alguien en la cola y el healer puede curar, recorrerá la lista y curará a todos
        if (cola.Count > 0 && can_heal)
        {
            //Debug.Log("curarEnemigosEnElArea");
            foreach (Enemy item in cola)
            {
                item.curarVida((item.max_vida / 100) * 20); //cura un 20% de los que esten en su area
                if(item.Equals(healer))
                    item.curarVida((item.max_vida / 100) * 20); //cura otra vez (el doble) al propio healer
            }
            //Una vez curados se deberá esperar un tiempo para volver a curar
            can_heal = false;
            StartCoroutine(coolDown());
        }
    }
    //Cuando un enemigo se mantiene en el area trigger del healer mira si tiene la vida máxima si no la tiene lo mete a la cola para que cuando cure la próxima vez lo haga con el enemigo que esté dentro
    private void OnTriggerStay2D(Collider2D collision)
    {
        Enemy e = collision.gameObject.GetComponent<Enemy>();
        if (e != null)
        {
            if(e.vida<e.max_vida && !cola.Contains(e))
                cola.Add(e);
        }

    }
    //Cuando entra en el trigger lo añade directamente a la lista de enemigos
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy e = collision.gameObject.GetComponent<Enemy>();
        if (e != null)
        {
            cola.Add(e);
        }

    }
    //Cuando sale del area, se quita de la lista
    private void OnTriggerExit2D(Collider2D collision)
    {
        Enemy e = collision.gameObject.GetComponent<Enemy>();
        if (e != null)
        {
            cola.Remove(e);
        }
    }
    //Esta función sirve para poner el booleano de can heal en true o false para poder curar o esperar el tiempo que se necesite
    public IEnumerator coolDown()
    {
        can_heal = false;

        Color tmp = this.GetComponent<SpriteRenderer>().color;

        this.GetComponent<SpriteRenderer>().enabled = false; // pierde color verde mientras se recupera
        
        yield return new WaitForSecondsRealtime(cool_down);

        this.GetComponent<SpriteRenderer>().enabled = true; // recuperamos el sprite
        this.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 0.15f); //vuelve al color verde

        can_heal = true;

    }
}
