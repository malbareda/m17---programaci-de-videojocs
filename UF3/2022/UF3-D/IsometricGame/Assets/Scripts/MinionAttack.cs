﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Esta funcion la usan los enemigos para poder disparar
public class MinionAttack : MonoBehaviour
{

    //List<GameObject> cola = new List<GameObject>();
    //La lista de los objetivos a rango del enemigo (ya sean torres o el personaje)
    public List<Ally> cola = new List<Ally>();
    //El objetivo el cual el enemigo le está haciendo focus en ese momento
    public Ally objetivo = null;
    //El proyectil se coje de un prefab
    public Proyectil proyectilPrefab;
    //public Proyectil actualProyectil = null;
    //Para saber si puede dispara o no
    bool can_shoot = true;
    //Un tiempo de descanso entre disparo y disparo
    public float cool_down = 1.0f;

    public bool attacking = false;

    Pathfinding.AILerp lerp;

    // Start is called before the first frame update
    void Start()
    {
        lerp = this.GetComponentInParent<Pathfinding.AILerp>();
    }

    // Update is called once per frame
    //En el update mira si hay algún objetivo en la lista y lo convierte en el target, luego si puede disparar lo hará generando así un proyectil que irá a él
    void Update()
    {
        objetivo = null;
        if (cola.Count > 0)
        {
            foreach (Ally item in cola)
            {
                objetivo = item;
                break;
            }
        }
        //Si hay un objetivo y le puede disparar, generará el disparo y perseguirá al objetivo hasta darle
        if (objetivo != null && can_shoot)
        {
            Proyectil actualProyectil = Instantiate(proyectilPrefab);
            actualProyectil.name = "proyEvil";
            actualProyectil.transform.position = this.transform.position;
            actualProyectil.daño = 100;
            actualProyectil.target = objetivo;
            StartCoroutine(coolDown());
            //attacking = true;
        }

        if (objetivo != null && lerp.canMove)
        {
            lerp.canMove = false;
        }

        if (objetivo == null && !lerp.canMove)
        {
            lerp.canMove = true;
        }
    }
    //Cuando entra dentro del rango, entra en la lista de posibles objetivos
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Ally a = collision.gameObject.GetComponent<Ally>();
        if (a != null)
        {
            //Debug.Log("Entra" + a.name);
            cola.Add(a);
        }

    }
    //Cuando sale del rango del enemigo, sale de la lista de posibles objetivos
    private void OnTriggerExit2D(Collider2D collision)
    {
        Ally a = collision.gameObject.GetComponent<Ally>();
        if (a != null)
        {
            //Debug.Log("Sale" + a.name);
            cola.Remove(a);
        }
    }
    //El tiempo que no puede disparar
    public IEnumerator coolDown()
    {
        can_shoot = false;
        yield return new WaitForSecondsRealtime(cool_down);
        can_shoot = true;

    }
}
