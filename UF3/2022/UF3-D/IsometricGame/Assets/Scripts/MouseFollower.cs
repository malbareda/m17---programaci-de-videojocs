﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Esta clase se utiliza para un unico objeto ya que cambiara su posicion en todo momento
 * Siempre estara justo donde el ratón este
 * Ademas servira para sustituir OnMouseEnter y otras funciones en el caso de objetos que no son UI, como las Torres
 * Las torres detectaran la pequeña collision de este objeto 
 * */
public class MouseFollower : MonoBehaviour
{
    //Las torres son Ally en caso de no ser null esta variable las otras torres no se iluminaran
    public Ally selected = null;

    public Text info;

    private void Start()
    {
        info.transform.parent.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 inputmouse = Input.mousePosition;

        Vector2 mouse = Camera.main.ScreenToWorldPoint(inputmouse);

        this.transform.position = new Vector2(mouse.x, mouse.y);

        Physics.queriesHitTriggers = true;

        //aparte usaremos esta funcion
        changeInfo();
    }

    private void LateUpdate()
    {
        //Eliminar cualquier torre asignada a Mouse si no hay ninguna compra activa
        if(!MenuController.acciont1 && !MenuController.acciont2 && !MenuController.acciont3)
        {
            foreach (Transform child in transform)
            {
                if(child.GetComponent<Ally>()!=null)
                    Destroy(child.gameObject);
            }
        }
    }

    //Si hay una torre seleccionada se mostrara un texto para informar al usuario
    private void changeInfo()
    {
        if (selected != null)
        {
            info.transform.parent.gameObject.SetActive(true);
            if (MenuController.accionm)
                info.text = "" + (selected.precioMejora);
            else if (MenuController.accionv)
                info.text = "" + (selected.venta);
            else
                info.text = selected.name;
        }
        else
        {
            info.transform.parent.gameObject.SetActive(false);
        }
    }

    //el apartado para seleccionar una torre por defecto se hace en el prefab de la torre en el hijo llamado "Contour"
   
}
