﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Este script lo tiene el prefab del proyectil
public class Proyectil : MonoBehaviour
{
    //Tienen un objetivo el cual tienen que perseguir, el cual se comprueba en el personaje
    public Character target;
    //Un daño, que quitarán la vida al objetivo
    public float daño = 200;
    //Una velocidad de movimiento que definirán lo rápido que van
    public float vel = 3;

    private float beforDif = 0.0f;

    // Update is called once per frame
    void Update()
    {
        //Cada vez el proyectil se va haciendo más rápido
        vel += 0.01f;
        //moverse en direccion a el target enemigo
        Vector2 proyectilPosition = this.transform.position;
        Vector2 targetPosition = transform.position;
        if (target != null)
            targetPosition = target.transform.position;
        Vector3 direccion = (targetPosition - proyectilPosition).normalized;//convertir a (1,0,0) o (0,1,0)
        float moveAmount = vel * Time.deltaTime;
        moveAmount = Mathf.Clamp(moveAmount, 0f, Vector3.Distance(proyectilPosition, targetPosition));

        //Cantidad de desplazamiento aplicada en un frame
        this.transform.position += direccion * moveAmount;

        float diferenciaActual = Vector3.Distance(proyectilPosition, targetPosition);
        if (diferenciaActual <= 0.2f)
        {
            if (target != null)
                target.recibirDaño(daño);
            Destroy(this.gameObject);
        }
        else if (diferenciaActual == beforDif)
        {
            if (target != null)
                target.recibirDaño(daño);
            Destroy(this.gameObject);
        }
        else
        {
            beforDif = diferenciaActual;
        }
        //Debug.Log(Vector3.Distance(proyectilPosition, targetPosition));
    }
    //Cuando impactan con el objetivo le hacen daño y se destruyen los proyectiles
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Enemy>() != null)
        {
            collision.GetComponent<Enemy>().recibirDaño(daño);
            Destroy(this.gameObject);
        }
    }
}
