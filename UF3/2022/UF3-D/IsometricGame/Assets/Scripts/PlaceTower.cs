﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Este script permite poner la torre cuando le damos a comprar, además nos sale la torre antes de que la pongamos para ver donde se pondría
public class PlaceTower : MonoBehaviour
{
    public Point GridPosition {get;private set;}
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //Se va moviendo la torre por el mapa mientras movemos el raton
    public void Setup(Point gridPos, Vector3 worldPos, Transform parent){
        this.GridPosition = gridPos;
        transform.position = worldPos;
        transform.SetParent(parent);
        
    }
    //Cuando le damos al click izquierdo del ratón se pone la torre que teníamos seleccionada
    private void OnMouseOver(){
        if (Input.GetMouseButtonDown(0)){
            PlacingTower();
        }
    }
    private void PlacingTower(){
        Debug.Log("placing a tower");
    }
}
