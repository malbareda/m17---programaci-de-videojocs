﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//En la clase character tenemos todos los enemigos y aliados (torres y personaje)
public class Character : MonoBehaviour
{
    //Como variables tenemos la vida actual y la vida máxima, que como es lógico la vida actual no puede sobrepasar la máxima y si llega a 0 de vida actual muere
    public float vida = 150;
    public float max_vida = 0;
    // Start is called before the first frame update
    void Awake()
    {
        max_vida = vida;
    }
    //Cuando un personaje es golpeado, recibe daño, este daño lo resta a la vida actual, si llega a 0 muere
    public void recibirDaño(float daño)
    {
        vida -= daño;
        if (vida < 0) vida = 0;
        if (vida == 0) die();
    }
    //Cuando un personaje se cura su vida actual aumenta, pero nunca podrá sobrepasar la máxima
    public void curarVida(float heal)
    {
        if (vida != max_vida)
        {
            if ((vida + heal) > max_vida)
                vida = max_vida;
            else
                vida += heal;
        }
    }
    //Cuando la vida actual de un personaje llega a 0 muere y su objeto se destruye
    public virtual void die()
    {
        Destroy(this.gameObject);
    }
}
