﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;//nos servira por si queremos saber que boton esta siendo seleccionado en el menu

public class MenuController : MonoBehaviour
{
    //Estos boolean sirven para saber como deberían de cambiarse los menús dependiendo de su estado, si están true significa que cuando se active su menú pasarán a false y desaparecerán
    private bool menuop = true;
    private bool statop = true;
    private bool compraop = false;
    //Estos boolean es para saber si se pueden poner las torres 1, 2 y 3, las cuales solo tenemos implementada la primera, si está true significa que cuando clickemos en el mapa si tenemos el dinero se pondrá una torre.
    public static bool acciont1 = false;
    public static bool acciont2 = false;
    public static bool acciont3 = false;
    // Lo mismo que lo de las torres, pero con la venta y la mejora de torres
    public static bool accionv = false;
    public static bool accionm = false;
    //Son los diferentes menús los cuales van variando dependiendo de lo que busquemos, entre el menú entero, el de las compras y el de los stats.
    public GameObject menua;

    public GameObject stata;

    public GameObject compraa;
    // El profab base de la primera torre que crearemos cuando la compremos
    public GameObject tower1;
    public GameObject tower2;
    public GameObject tower3;

    public Button cancelar;

    public static GameObject torre;

    //El texto en pantalla que te dice si puedes poner torre, si puedes vender o mejorar
    public Text recordatorio = null;

    //La posición del ratón
    private Vector2 inputmouse;

    private Vector2 mousePos;

    // Start is called before the first frame update
    void Start()
    {
        //Haré que el juego empiece sin ningún mensaje en pantalla ni ningún menú
        recordatorio.text = "";
        menu();
        cancelar.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(accionv);

        //En el update estamos siempre actualizando la posición del ratón

        inputmouse = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(inputmouse);
        //Si pulsamos E sale el menú de compra, venta y mejora
        if (Input.GetKeyDown("e"))
        {
            menu();
        }
        //Si pulsamos Q salen los stats del personaje
        if (Input.GetKeyDown("q"))
        {
            stats();
        }
        //Si los booleanes de compra de torre1, torre 2 o torre 3 están activos cambiarán el texto que sale por pantalla
        if (acciont1 || acciont2 || acciont3)
        {
            cancelar.gameObject.SetActive(true);
            recordatorio.text = "Escoge donde quieres poner la torre";
        }
        //Lo mismo que lo de arriba con el de venta y con el de mejora
        else if (accionv)
        {
            cancelar.gameObject.SetActive(true);
            recordatorio.text = "Escoge la torre que quieres vender";
        }
        else if (accionm)
        {
            cancelar.gameObject.SetActive(true);
            recordatorio.text = "Escoge la torre que quieres mejorar, cuesta el doble del precio original de la torre";
        }
        //Si no hay ningún booleano de acción activado quitará el texto
        else
        {
            cancelar.gameObject.SetActive(false);
            recordatorio.text = "";
        }
        //Las acciones siempre están activas para que en cualquier momento que tengamos activado alguno de los booleanos compre, venda o mejore
        compraClick();
        
        //vender();
        //mejorar();
    }
    public void cancelarAccion()
    {
        acciont1 = false;
        acciont2 = false;
        acciont3 = false;
        accionv = false;
        accionm = false;
        if (torre != null)
        {
            Destroy(torre.gameObject);
            torre = null;
        }
    }
    
    //Únicamente vuelve a poner en false el booleano de acción para no estar todo el rato vendiendo
    public void vender()
    {
        if (Input.GetMouseButton(0) && accionv)
        {

            accionv = false;

        }
    }
    //Lo mismo que lo de arriba pero con la mejora
    public void mejorar()
    {
        if (Input.GetMouseButton(0) && accionm)
        {
            
            accionm = false;

        }
    }
    
    //Estas 3 funciones hacen lo mismo, de base desactivan los otros 2 menús pero además miran si estaba activado o no el otro que intentan activad/desactivar, para que si estaba desactivado, lo active y viceversa.
    private void menu()
    {
        if (menuop)
        {
            menua.SetActive(false);
            menuop = false;
            statop = false;
            compraop = false;
            compraa.SetActive(false);
            stata.SetActive(false);

        }
        else if (!menuop)
        {
            menua.SetActive(true);
            menuop = true;
            statop = false;
            compraop = false;
            compraa.SetActive(false);
            stata.SetActive(false);

        }

    }
    private void stats()
    {
        if (statop)
        {
            stata.SetActive(false);
            menuop = false;
            statop = false;
            compraop = false;
            compraa.SetActive(false);
            menua.SetActive(false);

        }
        else if (!statop)
        {
            stata.SetActive(true);
            menuop = false;
            statop = true;
            compraop = false;
            compraa.SetActive(false);
            menua.SetActive(false);

        }
    }
    public void compras()
    {
        if (compraop)
        {
            compraa.SetActive(false);
            menuop = false;
            statop = false;
            compraop = false;
            menua.SetActive(false);
            stata.SetActive(false);
        }
        else if (!compraop)
        {
            compraa.SetActive(true);
            menuop = false;
            statop = false;
            compraop = true;
            menua.SetActive(false);
            stata.SetActive(false);

        }
    }

    //Aquí es donde se crea el objeto de la torre cuando lo compramos
    void Tower1()
    {
        
        GameObject newtower1 = Instantiate(tower1);
        newtower1.transform.position = mousePos;
    }
    //Cuando se hace click en los botones del HUD ponen en true los booleanos de acciones
    public void cacciont1()
    {
        acciont1 = true;
        //añadido
        // La torre se instancia tal cual poniendola como hijo de del Objeto Mouse el cual cambia su posicion a donde este el raton en todo momento
        torre = Instantiate(tower1);
        torre.name = "Tower1";
        torre.transform.position = GameObject.Find("Mouse").transform.position;
        torre.transform.parent = GameObject.Find("Mouse").transform;
        //sobretodo hay que desactivar su ataque o si no tendremos una torreta floatante
        torre.GetComponentInChildren<TowerAttack>().enabled = false;
        //tambien desactivamos el collider
        torre.GetComponent<PolygonCollider2D>().enabled = false;
    }
    //Cuando se hace click en los botones del HUD ponen en true los booleanos de acciones
    public void cacciont2()
    {
        acciont1 = true;
        //añadido
        // La torre se instancia tal cual poniendola como hijo de del Objeto Mouse el cual cambia su posicion a donde este el raton en todo momento
        torre = Instantiate(tower2);
        torre.name = "Tower2";
        torre.transform.position = GameObject.Find("Mouse").transform.position;
        torre.transform.parent = GameObject.Find("Mouse").transform;
        //sobretodo hay que desactivar su ataque o si no tendremos una torreta floatante
        torre.GetComponentInChildren<TowerAttack>().enabled = false;
        //tambien desactivamos el collider
        torre.GetComponent<PolygonCollider2D>().enabled = false;
    }
    //Cuando se hace click en los botones del HUD ponen en true los booleanos de acciones
    public void cacciont3()
    {
        acciont3 = true;
        //añadido
        // La torre se instancia tal cual poniendola como hijo de del Objeto Mouse el cual cambia su posicion a donde este el raton en todo momento
        torre = Instantiate(tower3);
        torre.name = "Tower3";
        torre.transform.position = GameObject.Find("Mouse").transform.position;
        torre.transform.parent = GameObject.Find("Mouse").transform;
        //sobretodo hay que desactivar su ataque o si no tendremos una torreta floatante
        torre.GetComponentInChildren<TowerAttack>().enabled = false;
        //tambien desactivamos el collider
        torre.GetComponent<PolygonCollider2D>().enabled = false;
    }
    //Cuando la acción está activa comprueba si tiene dinero para comprar una torre y colocarla donde clique, si no, no hace nada
    public void compraClick()
    {
        if (acciont1 || acciont2 || acciont3)
        {
            //Primero cancelar en caso de intento de fraude
            if (MainController.dineroo < torre.GetComponent<Ally>().precio)
            {
                acciont1 = false;
                acciont2 = false;
                acciont3 = false;
            }
            else
            {
                //En caso contrario esperar a que el usuario haga click
                if (Input.GetMouseButton(0))
                {
                    //Siempre y cuando no haga click encima de boton de cancelar se colocara la torre
                    if (EventSystem.current.currentSelectedGameObject != cancelar.gameObject)
                    {
                        MainController.dineroo -= torre.GetComponent<Ally>().precio;

                        //Invoke("Tower1", 0);
                        //activamos el collider
                        torre.GetComponent<PolygonCollider2D>().enabled = true;
                        //acticamos su ataque y lo sacamos de mouse para que deje de moverse
                        torre.GetComponentInChildren<TowerAttack>().enabled = true;
                        torre.transform.parent = null;
                        torre = null;
                        //Recalculamos A* (Astar)
                        //AstarPath.active.Scan();
                        var graphToScan = AstarPath.active.data.gridGraph;
                        AstarPath.active.Scan(graphToScan);
                    }
                    acciont1 = false;
                    acciont2 = false;
                    acciont3 = false;

                }
            }
        }

    }
    
    public void caccionv()
    {
        accionv = true;

    }
    public void caccionm()
    {
        accionm = true;

    }
}
