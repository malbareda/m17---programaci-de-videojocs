﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoanWallRun : MonoBehaviour
{
    // Start is called before the first frame update
    private bool wallrun = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey("space")){
            print("Joan " + this.transform.position);
            this.GetComponent<Rigidbody>().velocity = this.transform.up * 5;
        }
        if (Input.GetKey("w"))
        {
            print("Joan " + this.transform.position);
            //transform.forward sempre apunta endavant.
            
            this.GetComponent<Rigidbody>().velocity = this.transform.forward*2;
        }
        if (Input.GetKey("s"))
        {
            print("Joan " + this.transform.position);
            //transform.forward sempre apunta endavant.

            this.GetComponent<Rigidbody>().velocity = -this.transform.forward * 2;
        }
        if (Input.GetKey("a"))
        {
            this.transform.Rotate(new Vector3(0, -1, 0)*1);
        }
        if (Input.GetKey("d"))
        {
            this.transform.Rotate(new Vector3(0, 1, 0) * 1);
        }
        if (Input.GetKey("q"))
        {
            this.GetComponent<Rigidbody>().velocity = -this.transform.right * 2;
        }
        if (Input.GetKey("e"))
        {
            this.GetComponent<Rigidbody>().velocity = this.transform.right * 2;
        }

        if (wallrun) this.GetComponent<Rigidbody>().velocity += -this.transform.up*0.5f;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "wallrun")
        {
            this.wallrun = true;
            this.transform.Rotate(new Vector3(-90, 0, 0));
            this.GetComponent<Rigidbody>().useGravity = false;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "wallrun")
        { 
        this.wallrun = false;
        this.transform.Rotate(new Vector3(90, 0, 0));
        this.GetComponent<Rigidbody>().useGravity = true;
        }   
    }
}
