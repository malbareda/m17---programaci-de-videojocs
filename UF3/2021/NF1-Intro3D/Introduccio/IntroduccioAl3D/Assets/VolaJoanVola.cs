﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolaJoanVola : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey("space")){
            print("Joan " + this.transform.position);
            this.GetComponent<Rigidbody>().velocity = this.transform.up * 5;
        }
        if (Input.GetKey("w"))
        {
            print("Joan " + this.transform.position);
            //transform.forward sempre apunta endavant.
            
            this.GetComponent<Rigidbody>().velocity = this.transform.forward*2;
        }
        if (Input.GetKey("s"))
        {
            print("Joan " + this.transform.position);
            //transform.forward sempre apunta endavant.

            this.GetComponent<Rigidbody>().velocity = -this.transform.forward * 2;
        }
        if (Input.GetKey("a"))
        {
            this.transform.Rotate(new Vector3(0, -1, 0)*1);
        }
        if (Input.GetKey("d"))
        {
            this.transform.Rotate(new Vector3(0, 1, 0) * 1);
        }
        if (Input.GetKey("q"))
        {
            this.GetComponent<Rigidbody>().velocity = -this.transform.right * 2;
        }
        if (Input.GetKey("e"))
        {
            this.GetComponent<Rigidbody>().velocity = this.transform.right * 2;
        }
    }
}
