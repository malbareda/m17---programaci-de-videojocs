﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class SimpleCarController : MonoBehaviour
{
    public List<AxleInfo> axleInfos;
    public float maxMotorTorque;
    public float maxSteeringAngle;

    // finds the corresponding visual wheel
    // correctly applies the transform
    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
        if (collider.transform.childCount == 0)
        {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);
        rotation = rotation * Quaternion.Euler(new Vector3(0, 0, 90));
        visualWheel.transform.rotation = rotation;
        visualWheel.transform.position = position;
        
    }

    public void FixedUpdate()
    {
        float motor = maxMotorTorque * Input.GetAxis("Vertical");
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

        foreach (AxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor)
            {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
            }
            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);
        }
        if (Input.GetKey("z"))
        {
            
            foreach (AxleInfo axleInfo in axleInfos)
            {
                
                    WheelFrictionCurve wfc = new WheelFrictionCurve();
                    wfc.extremumSlip = 0f;
                    wfc.asymptoteSlip = 0f;
                    wfc.asymptoteValue = 0f;
                    wfc.extremumValue = 0f;
                    wfc.stiffness = 1;
                    axleInfo.rightWheel.forwardFriction = wfc;
                    axleInfo.leftWheel.forwardFriction = wfc;
                    WheelFrictionCurve wfc2 = new WheelFrictionCurve();
                    wfc2.extremumSlip = 1000f;
                    wfc2.asymptoteSlip = 1000f;
                    wfc2.asymptoteValue = 1000f;
                    wfc2.extremumValue = 1000f;
                    wfc2.stiffness = 1;
                    axleInfo.rightWheel.sidewaysFriction = wfc2;
                    axleInfo.leftWheel.sidewaysFriction = wfc2;
                
               
            }
        }
    }
}