using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorretaGotele : MonoBehaviour
{
    private bool sentido = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!sentido)
        {
            this.transform.Rotate(new Vector3(0, -1, 0));
            if (this.transform.rotation.eulerAngles.y < 270 && this.transform.rotation.eulerAngles.y > 250)
            {
                sentido = !sentido;
            }


        }
        else
        {
            this.transform.Rotate(new Vector3(0, 1, 0));
            if (this.transform.rotation.eulerAngles.y > 90 && this.transform.rotation.eulerAngles.y < 250)
            {

                sentido = !sentido;
            }
        }

        RaycastHit hitInfo;
        Debug.DrawLine(this.transform.position, this.transform.forward*50,Color.magenta);
        if(Physics.Raycast(this.transform.position, this.transform.forward, out hitInfo, 50f )){
            print("RAYCAST HIT "+hitInfo.distance+" "+hitInfo.transform.name);

        }

      //  Physics.SphereCastAll

    }
}
