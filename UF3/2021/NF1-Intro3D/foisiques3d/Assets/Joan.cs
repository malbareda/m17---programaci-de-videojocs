﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joan : MonoBehaviour
{
    CharacterController cc;
    // Start is called before the first frame update
    void Start()
    {
       cc  = this.GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {

        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");

        Vector3 movimiento = transform.forward * ver + transform.right * hor;
        print(cc);
        print(5 * Time.deltaTime * movimiento);
        cc.SimpleMove(80 * Time.deltaTime * movimiento);

    }
}
