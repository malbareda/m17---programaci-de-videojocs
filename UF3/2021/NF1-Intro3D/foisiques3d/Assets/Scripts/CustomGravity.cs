﻿using UnityEngine;

public static class CustomGravity {

	public static Vector3 GetGravity(Vector3 position, Vector3 positionPlaneta) { 
		return (position-positionPlaneta).normalized * Physics.gravity.y;
	}


	public static Vector3 GetUp (Vector3 position) {
		Vector3 up = position.normalized;
        if (Physics.gravity.y < 0f)
        {
			return up;
        }
        else
        {
			return -up;
        }
	}
}