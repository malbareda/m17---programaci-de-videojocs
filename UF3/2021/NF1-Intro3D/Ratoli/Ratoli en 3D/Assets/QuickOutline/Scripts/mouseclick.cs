﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseclick : MonoBehaviour
{
    public bool selected = false;
    public bool mouseover = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseOver()
    {
        mouseover = true;
        if (!selected)
        {
            this.GetComponent<Outline>().enabled = true;
            this.GetComponent<Outline>().OutlineMode = Outline.Mode.OutlineVisible;
        }
    }

    private void OnMouseExit()
    {
        mouseover = false;
        if (!selected)
        {
            this.GetComponent<Outline>().enabled = false;
        }
    }

    private void OnMouseDown()
    {
        if (selected)
        {
            if (!mouseover)
            {
                this.GetComponent<Outline>().enabled = false;
            }
            selected = false;
        }
        else
        {
            this.GetComponent<Outline>().enabled = true;
            this.GetComponent<Outline>().OutlineMode = Outline.Mode.OutlineAndSilhouette;
            selected = true;
        }
    }
}
