﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class ResourcePool : MonoBehaviour
{
    [Header("Values")]
    public float moveRate;
    public float moveSpeed;
    public float baseScaleMultiplier; // how large the icon starts
    public float scaleMultiplierIncreasePerUnit; // how much the icon increases in size
    public int unitsPerIcon; // how many units are represented by one icon
    public float iconSpawnRate;
    public ResourceType type;

    [Header("Prefabs")]
    public GameObject resourceIconPrefab;
    public GameObject materialPoolEffectObjectPrefab;

    [Space]
    public string spawnedIconTag = "SpawnedIcon";

    public bool IsComplete
    {
        get
        {
            return active == false;
        }
    }

    private RectTransform resourceCounterIconTransform;
    private int startAmount;
    private int amount; // how much was spent
    private bool active;
    private bool shouldSpawnIcons;
    private GameObject poolIcon;
    private Transform poolTarget;
    private ResourceCounter resourceCounter;
    private Vector3 baseLocalScale;
    private float currentScaleFactor;
    private List<GameObject> spawnedIcons;
    private VisualEffect materialPoolEffect;
    private int particlesAliveAtStart;

    // Coroutines
    private IEnumerator moveIcons;
    private IEnumerator spawnIcons;

    public void OnTriggerEnter(Collider other)
    {
        if (active && other.tag == spawnedIconTag && poolIcon != null)
        {
            if (!poolIcon.activeInHierarchy)
            {
                currentScaleFactor = baseScaleMultiplier;
                poolIcon.transform.localScale = new Vector3(baseLocalScale.x * currentScaleFactor, baseLocalScale.y * currentScaleFactor, baseLocalScale.z * currentScaleFactor);
                poolIcon.SetActive(true);
            }
            else
            {
                currentScaleFactor += scaleMultiplierIncreasePerUnit;
            }
            spawnedIcons.Remove(other.gameObject);
            Destroy(other.gameObject);
            poolIcon.transform.localScale = (baseLocalScale * currentScaleFactor);
        }
    }

    private IEnumerator MoveIcons()
    {
        while (Application.isPlaying)
        {
            if (active && spawnedIcons.Count > 0)
            {
                foreach (GameObject icon in spawnedIcons)
                {

                    if (icon != null)
                    {
                        Vector3 heading = poolTarget.position - resourceCounterIconTransform.position;
                        float distance = heading.magnitude;
                        Vector3 direction = heading / distance;
                        icon.transform.Translate(direction * moveSpeed);
                    }
                }
            }
            if (amount == 0 && spawnedIcons.Count == 0)
            {
                ShowVisualEffect();
            }
            yield return new WaitForSeconds(moveRate);
        }
    }

    private void ShowVisualEffect()
    {
        shouldSpawnIcons = false;
        StopAllCoroutines();

        poolIcon.SetActive(false);

        Transform towerUpgradePosition =
            UpgradeCastle.GetSelectedTower().transform.Find("ParticlePoint");

        GameObject materialPoolEffectObject =
            Instantiate(materialPoolEffectObjectPrefab);

        materialPoolEffect = materialPoolEffectObject.GetComponent<VisualEffect>();
        materialPoolEffect.SetVector3("StartPosition", poolIcon.transform.position);
        materialPoolEffect.SetVector3("TowerPosition", towerUpgradePosition.position);

        materialPoolEffect.SendEvent("OnPlay");

        Invoke("DeactivatePool", 3.0F);
    }

    private IEnumerator SpawnIcons()
    {
        while (Application.isPlaying && shouldSpawnIcons)
        {
            yield return new WaitForSeconds(iconSpawnRate);
            if (amount > 0)
            {
                int amountToSubtract = unitsPerIcon;
                if (resourceCounter.value - amountToSubtract < 0)
                {
                    amountToSubtract = resourceCounter.value;
                }
                amount -= amountToSubtract;
                resourceCounter.SubtractAmount(amountToSubtract);

                GameObject resourceIcon = Instantiate(resourceIconPrefab, resourceCounterIconTransform, true);
                resourceIcon.transform.position = resourceCounterIconTransform.transform.position;
                resourceIcon.GetComponent<SphereCollider>().enabled = true;
                resourceIcon.transform.localScale *= 0.005F;
                resourceIcon.transform.Find(type.ToString()).gameObject.SetActive(true);
                resourceIcon.tag = spawnedIconTag;

                spawnedIcons.Add(resourceIcon);
            }
        }
    }

    #region Activate/Deactivate Methods
    public void ActivatePool(int value)
    {
        moveIcons = MoveIcons();
        spawnIcons = SpawnIcons();
        poolTarget = transform.Find("WorldPositionTarget");
        if (spawnedIcons == null)
        {
            spawnedIcons = new List<GameObject>();
        }
        else
        {
            spawnedIcons.Clear();
        }
        amount = value;
        startAmount = value;
        active = true;
        shouldSpawnIcons = true;
        StartCoroutine(spawnIcons);
        StartCoroutine(moveIcons);
    }

    private void DeactivatePool()
    {
        active = false;
        StopAllCoroutines();
    }
    #endregion

    #region Set Methods
    public void SetType(ResourceType newType)
    {
        type = newType;
        poolIcon = transform.Find("ResourceIcon").Find(type.ToString()).gameObject;
        baseLocalScale = poolIcon.transform.localScale;
    }
    public void SetResourceCounterIconTransform(RectTransform iconTransform)
    {
        resourceCounterIconTransform = iconTransform;
    }

    public void SetResourceCounterScript(ResourceCounter counter)
    {
        resourceCounter = counter;
    }
    #endregion
}
