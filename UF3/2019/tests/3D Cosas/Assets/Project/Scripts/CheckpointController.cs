﻿using UnityEngine;
using System.Collections;

public class CheckpointController : MonoBehaviour
{
	#region Delegates
	public delegate void OnHitPlayerAction (CheckpointController checkpoint);
	#endregion Delegates

	#region Events
	public OnHitPlayerAction OnHitPlayer;
	#endregion Events

	#region Unity Fields
	public int id;
	#endregion Unity Fields
	
	#region Methods
	public void OnTriggerEnter (Collider collider)
	{
		if (collider.GetComponent<PlayerController>() != null)
		{
			if (OnHitPlayer != null) OnHitPlayer(this);
		}
	}
	#endregion Methods
}