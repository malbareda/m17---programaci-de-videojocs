﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	#region Delegates
	public delegate void OnHitSpikeAction();
	public delegate void OnHitOrbAction();
	#endregion Delegates

	#region Events
	public OnHitSpikeAction OnHitSpike;
	public OnHitOrbAction OnHitOrb;
	#endregion Events

	#region Constants
	private float SPEED = 800f;
	private float ROTATE_SPEED = 200f;
	#endregion Constants

	#region Fields
	private Vector3 leftBound;
	private Vector3 rightBound;
	#endregion Fields

	#region Methods
	public void FixedUpdate ()
	{
		ProcessInput();
	}

	private void ProcessInput ()
	{
		if (Input.GetKey("left") || Input.GetKey("a"))
		{
            /*
             * RotateAround, gira en funció d'un punt (fent servir un punt de referencia, un vector de rotació, i un angle)
             * El punt de referencia seria el centre de l'objecte. si vols rotar sobre tu mateix seria la propia position (recordem que la position implica les coordenades centrals)
             * el vector pots fer servir un dels tres vectors base
             *  transform.up -> gira al voltant de l'eix Y (fletxa verda)
             *  transform.forward -> gira al voltant de l'eix Z (fletxa blava)
             *  transform.right -> gira al voltant de l'eix X (fletxa vermella)
             * 
             * 
             * */


            /*
             * Time.deltaTime torna el temps entre Frames. D'aquesta forma t'assegures que la velocitat sigui independent del framerate. (un framerate mes baix fara que es mogui mes per frame)
             **/
			transform.RotateAround(transform.position, transform.up, Time.deltaTime * -ROTATE_SPEED);
		}
		if (Input.GetKey("right") || Input.GetKey("d"))
		{
			transform.RotateAround(transform.position, transform.up, Time.deltaTime * ROTATE_SPEED);
		}

		if (Input.GetKey("up") || Input.GetKey("w")) { this.GetComponent<Rigidbody>().AddForce(this.transform.forward * SPEED * Time.deltaTime); }
		if (Input.GetKey("down") || Input.GetKey("s")) { this.GetComponent<Rigidbody>().AddForce(-this.transform.forward * SPEED * Time.deltaTime); }
	}
	#endregion Methods
}