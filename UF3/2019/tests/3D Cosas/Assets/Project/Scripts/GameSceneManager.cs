﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameSceneManager : MonoBehaviour
{
	#region Unity Fields
	public Camera mainCamera;
	public Text scoreText;
	public Text gameOverText;
	public PlayerController player;
	public Transform checkpointContainer;
    public Camera fpCamera;
	#endregion Unity Fields

	#region Fields
	private int score;
	private float gameTimer;
	private bool gameOver;
	private int nCheckpoints;
	private int currentCheckpoint;
	private float trackStartTime;
	private bool started;
	private float bestTime;
    private int nLaps;
    private float totTime;
    private float timeLimit = 40f;
	#endregion Fields

	#region Methods
	public void Start ()
	{
		Time.timeScale = 1;

		player.OnHitSpike += OnGameOver;
		player.OnHitOrb += OnGameWin;

        //ens suscribim a tots els checkpoints
        nCheckpoints = checkpointContainer.childCount;
		foreach (CheckpointController checkpoint in checkpointContainer.GetComponentsInChildren<CheckpointController>())
		{
			checkpoint.OnHitPlayer += OnReachCheckpoint;
		}

		currentCheckpoint = -1;
        nLaps = 0;
        totTime = 0;

		scoreText.text = "";
	}

	public void Update ()
	{
		// LERP fa una interpolació lineal. Aixo fara un moviment de camara molt més smooth que no si directament ho posem a la posició del jugador
		mainCamera.transform.position = new Vector3
		(
            //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
			Mathf.Lerp(mainCamera.transform.position.x, (player.transform.position.x - (player.transform.forward.x * 10)), Time.deltaTime * 10),
			Mathf.Lerp(mainCamera.transform.position.y, 6, Time.deltaTime * 10),
			Mathf.Lerp(mainCamera.transform.position.z, (player.transform.position.z - (player.transform.forward.z * 10)), Time.deltaTime * 10)
		);

        //fa una rotació automàtica per a que miri al jugador
		mainCamera.transform.LookAt(player.transform);

        // LERP fa una interpolació lineal. Aixo fara un moviment de camara molt més smooth que no si directament ho posem a la posició del jugador
        fpCamera.transform.position = new Vector3
        (
            //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
            Mathf.Lerp(fpCamera.transform.position.x, (player.transform.position.x + (player.transform.forward.x * 1)), Time.deltaTime * 10),
            Mathf.Lerp(fpCamera.transform.position.y, 0f, Time.deltaTime * 10),
            Mathf.Lerp(fpCamera.transform.position.z, (player.transform.position.z + (player.transform.forward.z * 1)), Time.deltaTime * 10)
        );

        GameObject forw = new GameObject();

        Vector3 pos = new Vector3(player.transform.position.x + (player.transform.forward.x * 2), player.transform.position.y, player.transform.position.z + (player.transform.forward.z * 2));
        forw.transform.position = pos;


        //fa una rotació automàtica per a que miri al jugador
        fpCamera.transform.LookAt(player.transform);

        if (gameOver)
		{
			if (Input.GetKeyDown("r"))
			{
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			}

			return;
		}

		if (started)
		{
			scoreText.text = "Temps: " + (Time.time - trackStartTime).ToString("F2");
			if (bestTime > 0)
			{
				scoreText.text += "\nMillor: " + (bestTime.ToString("F2"));
			}
            scoreText.text += "\nRestant: " + (timeLimit - Time.time).ToString("F2");

        }
	}

	public void OnGameOver ()
	{
		gameOver = true;

		scoreText.enabled = false;
		gameOverText.enabled = true;

		gameOverText.text = "MotoMongo\nR per a reiniciar";

		Time.timeScale = 0;
	}

	public void OnGameWin ()
	{
		gameOver = true;

		scoreText.enabled = false;
		gameOverText.enabled = true;

		gameOverText.text = "If you ar jier tunai, itis bicos iu ar e güinner\nR per a reiniciar";

		Time.timeScale = 0;
	}

	private void OnReachCheckpoint (CheckpointController checkpoint)
	{
		if (currentCheckpoint == -1 && checkpoint.id == 0)
		{
			started = true;
			trackStartTime = Time.time;
			currentCheckpoint = checkpoint.id;
		}
		else if (checkpoint.id == 0 && currentCheckpoint == nCheckpoints - 1)
		{
			OnFullTrack();
			currentCheckpoint = checkpoint.id;
		}
		else if (checkpoint.id == currentCheckpoint + 1)
		{
			currentCheckpoint = checkpoint.id;
		}
	}

	private void OnFullTrack ()
	{
		float currentTime = Time.time - trackStartTime;
		if (bestTime == 0 || currentTime < bestTime)
		{
			bestTime = currentTime;
		}
        totTime += currentTime;
		trackStartTime = Time.time;
        nLaps++;
        if (totTime > 40)
        {
            this.OnGameOver();
        }else if (nLaps == 3)
        {
            this.OnGameWin();
        }
	}
	#endregion Methods
}