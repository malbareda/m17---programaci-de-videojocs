﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New HP", menuName = "HP Data", order = 52)]
public class HPPersonaje : ScriptableObject
{
    [SerializeField]
    private int hp;
    public int Hp { get; set; }

}
