﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coses : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //tengo un campo serializado que es el scriptableobject
    [SerializeField]
    private Coso coso; // 1
    [SerializeField]
    private GameEvent onCosoClick;

    private void OnMouseDown()
    {
        onCosoClick.Raise(
            ); // 2
    }
}
