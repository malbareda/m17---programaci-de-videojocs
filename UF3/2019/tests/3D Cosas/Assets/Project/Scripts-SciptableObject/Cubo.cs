﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubo : MonoBehaviour
{
    public float vel;
    // Start is called before the first frame update
    void Start()
    {
        vel = 0;
        this.transform.Translate(new Vector3(10, 10, 10));
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(new Vector3(0,0,0), transform.up, Time.deltaTime * vel);
        transform.RotateAround(new Vector3(0, 0, 0), transform.forward, Time.deltaTime * vel);
        vel++;
        if (vel > 500)
        {
            vel = -500;
        }
    }
}
