﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpUI : MonoBehaviour
{

    public Text text1;
    public Image image1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateDisplayUI(Coso coso)
    {

        text1.text = coso.CosoName + " " + coso.Description;
        print(coso.Icon);
        image1.sprite = coso.Icon;
    }
}
