﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//esta anotacion simplemente sirve para poder crear nuevos scriptableobjects desde el menu
[CreateAssetMenu(fileName = "New Coso", menuName = "Coso Data", order = 51)]
public class Coso : ScriptableObject
{
    //el scriptable object sive para guardar datos

    //todos los datos deben ser serializados y privados
    [SerializeField]
    private string cosoName;
    [SerializeField]
    private string description;
    [SerializeField]
    private Sprite icon;
    [SerializeField]
    private int notaFinal;

    //todos los datos deben tener accesores
    public string CosoName
    {
        get
        {
            return cosoName;
        }
    }

    public string Description
    {
        get
        {
            return description;
        }
    }

    public Sprite Icon
    {
        get
        {
            return icon;
        }
    }

    //pueden tambien tener setters
    public int NotaFinal
    {
        get
        {
            return notaFinal;
        }
        set
        {
            this.notaFinal = value;
        }
    }

}


