﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gregorio : MonoBehaviour
{
    public GameEvent medina;
    public GameEvent carlos;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Nota", 1, 2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Nota()
    {
        int nota = Random.Range(0, 10);
        print(nota);
        if (nota == 5)
        {
            medina.Raise();
        }else if (nota == 4)
        {
            carlos.Raise();
        }
    }
}
