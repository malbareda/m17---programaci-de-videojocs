﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//anotacion que solo sirve para que te salga en el menu de create.  
[CreateAssetMenu(fileName = "New Alumne", menuName = "Alumne Data", order = 52)]
public class Alumno : ScriptableObject
{
    [SerializeField]
    private string nom;
    [SerializeField]
    private Sprite foto;
    [SerializeField]
    private int notaFinal;

}
