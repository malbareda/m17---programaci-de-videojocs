﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirusController : MonoBehaviour
{
    public int punts = 0;
    public GameEvent limfocitEvent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Celula>().data.name=="Hemati")
        {
            this.punts++;
            Destroy(collision.gameObject);
        }else if (collision.gameObject.GetComponent<Celula>().data.name== "Monocit")
        {
            Destroy(this.gameObject);
        }else if (collision.gameObject.GetComponent<Celula>().data.name == "THelper")
        {
            limfocitEvent.Raise();
        }
    }
}
