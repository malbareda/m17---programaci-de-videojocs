﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Celula : MonoBehaviour
{
    public GameObject bengala;
    public ElementForm data;
    // Start is called before the first frame update
    public void init(ElementForm data)
    {
        this.data = data;
        print(data.name + " " +data.Radi);
        transform.localScale = 
            new Vector3(data.Radi, data.Radi, data.Radi);
        transform.
            Rotate(new Vector3(Random.Range(0, 360), 
            Random.Range(0, 360), Random.Range(0, 360)));
        GetComponent<Rigidbody>().velocity = 
            this.transform.forward * data.Spd;
        GetComponent<MeshRenderer>().material = 
            data.Color;

        if (data.name == "NK")
        {
            Invoke("NKExplode", Random.Range(5, 10));
        }
        if (data.name == "THelper")
        {
            this.GetComponent<SphereCollider>().radius = data.Radi * 2;
        }
        if (data.name == "Anticos")
        {
            this.transform.LookAt(GameObject.Find("Virus").transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //RES, NO FA RES
    }


    void NKExplode()
    {
        GameObject newBengala = Instantiate(bengala);
        Destroy(newBengala, 1f);

        RaycastHit hit;
        if (Physics.Raycast(this.transform.position, this.transform.forward, out hit, 2))
        {
            if (hit.transform.name == "Virus")
            {
                Destroy(hit.transform.gameObject);
            }
        }
        if (Physics.Raycast(this.transform.position, -this.transform.forward, out hit, 2))
        {
            if (hit.transform.name == "Virus")
            {
                Destroy(hit.transform.gameObject);
            }
        }
        if (Physics.Raycast(this.transform.position, this.transform.up, out hit, 2))
        {
            if (hit.transform.name == "Virus")
            {
                Destroy(hit.transform.gameObject);
            }
        }
        if (Physics.Raycast(this.transform.position, -this.transform.up, out hit, 2))
        {
            if (hit.transform.name == "Virus")
            {
                Destroy(hit.transform.gameObject);
            }
        }
        if (Physics.Raycast(this.transform.position, this.transform.right, out hit, 2))
        {
            if (hit.transform.name == "Virus")
            {
                Destroy(hit.transform.gameObject);
            }
        }
        if (Physics.Raycast(this.transform.position, -this.transform.right, out hit, 2))
        {
            if (hit.transform.name == "Virus")
            {
                Destroy(hit.transform.gameObject);
            }
        }
    }


}
