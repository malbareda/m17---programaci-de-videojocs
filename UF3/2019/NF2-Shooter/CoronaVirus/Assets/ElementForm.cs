﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//anotacion que solo sirve para que te salga en el menu de create.  
[CreateAssetMenu(fileName = "New Element", menuName = "Celula Data", order = 52)]
public class ElementForm : ScriptableObject
{
    [SerializeField]
    private string nom;
    [SerializeField]
    private Material color;
    [SerializeField]
    private float radi;
    [SerializeField]
    private float spd;

    public string Nom {get;set;}
    public Material Color { get; set; }
    public float Radi { get; set; }
    public float Spd { get; set; }

}

