﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{

    public ElementForm[] dataArr;
    public GameObject celula;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("generate", 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void generate()
    {
        ElementForm data = dataArr[Random.Range(0, dataArr.Length)];
        GameObject newCelula = Instantiate(celula);
        print("aa" + data.Radi);
        newCelula.GetComponent<Celula>().init(data);

    }

    public void antiCos(ElementForm ac)
    {
        ElementForm data = ac;
        GameObject newCelula = Instantiate(celula);
        newCelula.GetComponent<Celula>().init(data);
    }
}
