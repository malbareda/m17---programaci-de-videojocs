﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public int speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("w"))
        {
            this.GetComponent<Rigidbody>().
                AddForce(this.transform.forward * speed);
        }
        if (Input.GetKey("up"))
        {
            this.GetComponent<Rigidbody>().velocity = 
                (Vector3.up * speed);
        }
        if (Input.GetKey("down"))
        {
            this.GetComponent<Rigidbody>().velocity =
                (Vector3.down * speed);
        }
        if (Input.GetKey("a"))
        {
            this.transform.Rotate(0, -5, 0);
        }else if (Input.GetKey("d"))
        {
            this.transform.Rotate(0, 5, 0);
        }
    }
}
