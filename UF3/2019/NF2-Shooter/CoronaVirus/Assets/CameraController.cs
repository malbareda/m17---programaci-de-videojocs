﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // LERP fa una interpolació lineal. Aixo fara un moviment de camara molt més smooth que no si directament ho posem a la posició del jugador
        this.transform.position = new Vector3
        (
            //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
            Mathf.Lerp(this.transform.position.x, (player.transform.position.x - (player.transform.forward.x * 10)), Time.deltaTime * 10),
            Mathf.Lerp(this.transform.position.y, 6, Time.deltaTime * 10),
            Mathf.Lerp(this.transform.position.z, (player.transform.position.z - (player.transform.forward.z * 10)), Time.deltaTime * 10)
        );

        //fa una rotació automàtica per a que miri al jugador
        this.transform.LookAt(player.transform);
    }
}
