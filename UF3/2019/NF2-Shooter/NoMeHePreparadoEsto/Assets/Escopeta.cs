﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escopeta : MonoBehaviour
{
    float damage = 10f;
    float range = 20f;
    float momentum = 200f;
    public Camera FPCamera;
    public GameObject bengala;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) )
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        for(int i = 0; i < 5;  i++)
        {
            Vector3 newForward = new Vector3(FPCamera.transform.forward.x + UnityEngine.Random.Range(-0.05f, 0.05f), FPCamera.transform.forward.y + UnityEngine.Random.Range(-0.05f, 0.05f), FPCamera.transform.forward.z + UnityEngine.Random.Range(-0.05f, 0.05f));
            RaycastHit hit;
            if (Physics.Raycast(FPCamera.transform.position, newForward, out hit, range))
            {
                Debug.DrawRay(FPCamera.transform.position, newForward, Color.red);
                print(hit.transform.name);
                if (hit.transform.tag == "Disparable")
                {
                    hit.transform.gameObject.GetComponent<Disparable>().daño(damage);
                    //Vector3 middle = new Vector3(hit.normal.x, )
                    hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momentum, hit.point);
                }
                GameObject newBengala = Instantiate(bengala);
                newBengala.transform.position = hit.point;
                Destroy(newBengala, 1f);

            }
        }
        

    }

}
