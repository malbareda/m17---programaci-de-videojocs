﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AK : MonoBehaviour
{
    float damage = 10f;
    float range = 50f;
    float momentum = 200f;
    public Camera FPCamera;
    public GameObject bengala;
    public bool cd = false;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && !cd)
        {
            cd = true;
            Shoot();
            Invoke("Cooldown", 0.2f);
        }
    }

    private void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, range))
        {
            print(hit.transform.name);
            if (hit.transform.tag == "Disparable")
            {
                hit.transform.gameObject.GetComponent<Disparable>().daño(damage);
                //Vector3 middle = new Vector3(hit.normal.x, )
                hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momentum, hit.point);
            }
            GameObject newBengala = Instantiate(bengala);
            newBengala.transform.position = hit.point;
            Destroy(newBengala, 1f);

        }

    }

    private void Cooldown()
    {
        //poses un boolea a fals;
        cd = false;
    }
}
